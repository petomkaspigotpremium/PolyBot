package me.petomka.ts3bot.antiproxy;

import lombok.Getter;
import lombok.experimental.UtilityClass;
import me.petomka.ts3bot.config.ConfigManager;

import java.util.List;

@UtilityClass
public class Geofence {

	private static boolean isEnabled = ConfigManager.getBoolean("proxycheck.geofence.enabled");

	@Getter
	private static List<String> fencedCountries = ConfigManager.getStringList("proxycheck.geofence.countries");

	@Getter
	private static boolean fenceIsWhitelist = ConfigManager.getBoolean("proxycheck.geofence.is-whitelist");

	public static boolean isFenced(ProxyQueryResult queryResult) {
		return isFenced(queryResult.getCountry());
	}

	public static boolean isFenced(String country) {
		if (!isEnabled) {
			return false;
		}
		if (country == null || country.isEmpty()) {
			return false;
		}
		if (fenceIsWhitelist) {
			return !fencedCountries.contains(country);
		}
		return fencedCountries.contains(country);
	}

}
