package me.petomka.ts3bot.antiproxy;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import lombok.experimental.UtilityClass;
import me.petomka.ts3bot.config.ConfigManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

@UtilityClass
public class ProxyChecker {

	private static final String contactMail = "spigotpolybot@gmail.com";

	public static boolean isProxyIP(ProxyQueryResult queryResult) {
		return isProxyIP(queryResult, ConfigManager.getDouble("proxycheck.threshold"));
	}

	public static boolean isProxyIP(ProxyQueryResult queryResult, double threshold) {
		Preconditions.checkArgument(threshold <= 1.0D && threshold >= 0,
				"Threshold must be between 0.0 and 1.0 (included)");
		return queryResult.getResult() >= threshold;
	}

	public static Future<Optional<ProxyQueryResult>> queryIPAddress(String ipToQuery) {
		try {
			FutureTask<Optional<ProxyQueryResult>> futureTask = new FutureTask<>(() -> {
				String json = getText("http://check.getipintel.net/check.php?format=json&ip=" + ipToQuery +
						"&contact=" + contactMail + "&oflags=c");
				Gson gson = new Gson();
				return Optional.of(gson.fromJson(json, ProxyQueryResult.class));
			});
			futureTask.run();
			return futureTask;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new FutureTask<>(Optional::empty);
	}

	private static String getText(String url) throws Exception {
		URL website = new URL(url);
		URLConnection connection = website.openConnection();
		BufferedReader in = new BufferedReader(
				new InputStreamReader(
						connection.getInputStream()));

		StringBuilder response = new StringBuilder();
		String inputLine;

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}

		in.close();

		return response.toString();
	}

}
