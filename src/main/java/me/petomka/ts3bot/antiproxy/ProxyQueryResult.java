package me.petomka.ts3bot.antiproxy;

import lombok.Data;

@Data
public class ProxyQueryResult {

	/*
	 * Example Result:
	 * {"status":"success",
	 * "result":"1",
	 * "queryIP":"77.232.155.102",
	 * "queryFlags":null,
	 * "queryOFlags":"c",
	 * "queryFormat":"json",
	 * "contact":"given@contact.mail",
	 * "Country":"RU"}
	 * */

	private String status;
	private double result;
	private String queryIP;
	private String Country;

}
