package me.petomka.ts3bot.commands;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.DatabaseClientInfo;
import com.google.common.collect.Lists;
import me.petomka.ts3bot.config.AccountLinker;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.config.FileStorageLinker;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.modules.FeaturesModule;
import me.petomka.ts3bot.modules.LinkModule;
import me.petomka.ts3bot.modules.ModuleHandler;
import me.petomka.ts3bot.modules.StatusModule;
import me.petomka.ts3bot.mysql.MySQL;
import me.petomka.ts3bot.scoreboard.ChannelScoreboard;
import me.petomka.ts3bot.tasks.servergroupupdater.ServergroupUpdater;
import me.petomka.ts3bot.ts3listeners.supportchannel.SupportChannelListener;
import me.petomka.ts3bot.util.Array;
import me.petomka.ts3bot.util.IconSetter;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.UtilChatColor;
import me.petomka.ts3bot.util.clientverifier.ClientVerifier;
import me.petomka.ts3bot.util.commandsender.UtilCommandSender;
import me.petomka.ts3bot.util.player.UtilPlayer;
import me.petomka.ts3bot.util.playerverifier.PlayerVerifier;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.io.FileUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class UniMethods {

	public static int getLinkServergroupID() {
		return ConfigManager.getInt("link.servergroup-id");
	}

	/**
	 * Links a player with a matching client (by IP)
	 * Can override default values (if passed as non null values)
	 *
	 * @param toLink         The player to be linked
	 * @param clientUID      Optional: When passed overrides the selection by IP;
	 *                       A client on the teamspeak corresponding to the player
	 * @param setDescription Optional: When passed overrides the default config value;
	 *                       Whether a description according to the config should be set or not
	 * @param setIcon        Optional: When passed overrides the default config value;
	 *                       Whether a clients Icon should be set
	 * @param performServerGroupUpdate Optional: When passed overrides the default config value;
	 *                                 Whether the servergroup udpate task should run immediately for this client
	 */
	public static void linkPlayer(@Nonnull UtilPlayer toLink, @Nullable String clientUID, @Nullable Boolean setDescription, @Nullable Boolean setIcon, @Nullable Boolean performServerGroupUpdate) {
		if (LinkModule.getInstance().getStatus() != StatusModule.Status.OK) {
			toLink.sendMessage(ConfigManager.getString("message.needs-attention"));
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Bad linking status!");
			return;
		}
		if (ConfigManager.getLinkLimit() >= 0 &&
				LinkerHandler.linker.getClientUIDbyPlayerUUID(toLink.getUUID()).size() >= ConfigManager.getLinkLimit()) {
			toLink.sendMessage(ConfigManager.getLinkLimitReachedMessage());
			return;
		}

		int assign_group_id = getLinkServergroupID();
		String playerIP = toLink.getIPAddress().getHostAddress();
		TS3Api ts3 = MainManager.main.getTS3Api();
		List<Client> clientInfos = ts3.getClients();

		if (clientInfos == null) {
			clientInfos = Collections.emptyList();
		}

		List<Client> possibleClients;
		if (clientUID == null) {
			possibleClients = clientInfos.stream()
					.filter(info -> info.getIp().equals(playerIP))
					.filter(info -> !Array.containsInt(info.getServerGroups(), assign_group_id))
					.filter(info -> !LinkerHandler.linker.isClientLinked(info.getUniqueIdentifier()))
					.filter(Client::isRegularClient)
					.collect(Collectors.toList());
		} else {
			ClientInfo clientInfo = MainManager.main.getTS3Api().getClientByUId(clientUID);
			if (clientInfo == null) {
				throw new NullPointerException("Passed UID to link to did not provide a valid client");
			}
			possibleClients = Collections.singletonList(clientInfo);
		}

		if (possibleClients.size() == 0) {
			toLink.sendMessage(ConfigManager.getString("link.not-connected"));
			return;
		}

		if (possibleClients.size() == 1) {
			ts3.addClientToServerGroup(assign_group_id, possibleClients.get(0).getDatabaseId());
			if (setDescription == null ? ConfigManager.getBoolean("link.description.set") : setDescription) {
				ts3.editClient(possibleClients.get(0).getId(), Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION,
						ConfigManager.getRawString("link.description.format").replace("<player>", toLink.getName())
								.replace("<uuid>", toLink.getUUID().toString())));
			}
			if (setIcon == null ? ConfigManager.getBoolean("link.set-commandsender-head-icon") : setIcon) {
				CompletableFuture.runAsync(() -> {
					try {
						IconSetter.setIcon(possibleClients.get(0).getUniqueIdentifier(), toLink.getUUID(), MainManager.main.getTS3Api());
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
			}
			if (performServerGroupUpdate == null ? ConfigManager.getBoolean("link.immediate-group-update") : performServerGroupUpdate) {
				ServergroupUpdater updater = ServergroupUpdater.getInstance();
				if (updater != null) { //Feature may be disabled
					DatabaseClientInfo dbInfo = MainManager.main.getTS3Api().getDatabaseClientByUId(possibleClients.get(0).getUniqueIdentifier());
					ServergroupUpdater.getPermissionToGroup().keySet().forEach(
							perm -> updater.performUpdate(toLink, perm, dbInfo)
					);
				}
			}
			toLink.sendMessage(ConfigManager.getString("link.success"));
			LinkerHandler.linker.linkPlayer(toLink.getUUID(), possibleClients.get(0).getUniqueIdentifier());
			dispatchLinkCommands(toLink);
			return;
		}
		for (Client client : possibleClients) {
			ts3.sendPrivateMessage(client.getId(), ConfigManager.getRawString("link.ts3-verify-info"));
		}
		if (ClientVerifier.isVerifying(toLink.getUUID())) {
			toLink.sendMessage(ConfigManager.getString("link.multi-account").replaceAll("<code>", ClientVerifier.getCode(toLink.getUUID())));
			return;
		}
		ClientVerifier verifier = MainManager.main.createVerifier(toLink.getUUID());
		toLink.sendMessage(TextComponent.fromLegacyText(ConfigManager.getString("link.multi-account")
				.replaceAll("<code>", verifier.getCode())));
	}

	/**
	 * @see UniMethods#linkPlayer(UtilPlayer, String, Boolean, Boolean, Boolean)
	 */
	@Deprecated
	public static void linkPlayer(@Nonnull UtilPlayer toLink, @Nullable String clientUID, @Nullable Boolean setDescription, @Nullable Boolean setIcon) {
		linkPlayer(toLink, clientUID, setDescription, setIcon, null);
	}

	public static void dispatchLinkCommands(@Nullable UtilPlayer player) {
		if (player == null) {
			return;
		}
		ConfigManager.getStringList("link.dispatched-commands").forEach(cmd -> {
			MainManager.main.dispatchCommand(MainManager.main.getConsoleCommandSender(),
					cmd.replace("<name>", player.getName())
							.replace("<uuid>", player.getUUID().toString()));
		});
	}

	public static void unlinkPlayer(@Nullable UtilPlayer toUnlink, @Nonnull UUID playerUUID, @Nonnull String clientUID,
									@Nullable Boolean removeLinkGroup, @Nullable Boolean removeDescription,
									@Nullable Boolean removeClientIcon, @Nullable Boolean removeSyncedGroups) {

		LinkerHandler.linker.unlinkPlayer(playerUUID, clientUID);

		DatabaseClientInfo clientInfo = MainManager.main.getTS3Api().getDatabaseClientByUId(clientUID);
		if (clientInfo == null) {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Passed UID to unlink to did not provide a valid client");
			return;
		}
		TS3Api ts3 = MainManager.main.getTS3Api();

		if (toUnlink != null) {
			Optional.ofNullable(ChannelScoreboard.getBoard(toUnlink)).ifPresent(ChannelScoreboard::dispose);
		}

		try {
			if (removeLinkGroup == null ? ConfigManager.getBoolean("unlink.revoke-link-group") : removeLinkGroup) {
				MainManager.main.getTS3Api().removeClientFromServerGroup(ConfigManager.getInt("link.servergroup-id"),
						clientInfo.getDatabaseId());
			}
		} catch (Exception e) {
			MainManager.main.getPluginLogger().log(Level.WARNING, "Error when removing link group: " + e.getMessage());
		}
		try {
			if (removeDescription == null ? ConfigManager.getBoolean("unlink.remove-description") : removeDescription) {
				ts3.editDatabaseClient(clientInfo.getDatabaseId(), Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, ""));
			}
		} catch (Exception e) {
			MainManager.main.getPluginLogger().log(Level.WARNING, "Error when removing description: " + e.getMessage());
		}
		try {
			if (removeClientIcon == null ? ConfigManager.getBoolean("unlink.remove-client-icon") : removeClientIcon) {
				IconSetter.unsetIcon(clientUID, ts3);
			}
		} catch (Exception e) {
			MainManager.main.getPluginLogger().log(Level.WARNING, "Error when removing client icon: " + e.getMessage());
		}
		if (removeSyncedGroups == null ? ConfigManager.getBoolean("unlink.remove-synced-groups") : removeSyncedGroups) {
			for (Integer value : ServergroupUpdater.getPermissionToGroup().values()) {
				try {
					MainManager.main.getTS3Api().removeClientFromServerGroup(value, clientInfo.getDatabaseId());
				} catch (Exception e) {
					MainManager.main.getPluginLogger().log(Level.WARNING, "Error when removing a synced group: " + e.getMessage());
				}
			}
		}
		ConfigManager.getStringList("unlink.dispatched-commands").forEach(cmd -> {
			MainManager.main.dispatchCommand(MainManager.main.getConsoleCommandSender(), cmd);
		});
		if (toUnlink != null) {
			toUnlink.sendMessage(ConfigManager.getString("unlink.success"));
		}
	}

	public static void verificationCodeEntered(@Nonnull UtilPlayer sender, @Nonnull String code) {
		PlayerVerifier verifier = PlayerVerifier.getVerifier(sender.getUUID());
		if (verifier == null) {
			sender.sendMessage(ConfigManager.getString("message.not-verifying"));
			return;
		}
		if (!code.equalsIgnoreCase(verifier.getCode())) {
			sender.sendMessage(ConfigManager.getString("message.illegal-code").replace("<code>", code));
			return;
		}
		verifier.finished();
	}

	public static void linkConvert(UtilCommandSender sender, String[] args) {
		if (args.length != 1) {
			return;
		}
		if (!MySQL.hasConnection()) {
			try {
				MySQL.initMySQL();
			} catch (Exception ignored) {
			}
			sender.sendMessage(ConfigManager.getString("message.mysql-required"));
			return;
		}
		if (args[0].equalsIgnoreCase("mysql")) {
			convertFromYamlToSQL();
		} else if (args[0].equalsIgnoreCase("yml")) {
			boolean connection = false;
			try {
				MySQL.initMySQL();
				connection = true;
			} catch (Exception ignored) {
			}
			if (MySQL.hasConnection() && connection) {
				convertFromSQLToYaml();
			} else {
				sender.sendMessage(ConfigManager.getString("message.mysql-required"));
			}
		}
		sender.sendMessage(ConfigManager.getString("message.check-console"));
	}

	private static void convertFromYamlToSQL() {
		AccountLinker ymlLinker = LinkerHandler.getYMLLinker();
		AccountLinker mySQLLinker = LinkerHandler.getMySQLAccountLinker();
		ymlLinker.getLinkedUids().forEach((uuid, strings) -> {
			strings.forEach(uid -> {
				mySQLLinker.linkPlayer(uuid, uid);
			});
		});
	}

	private static void convertFromSQLToYaml() {
		FileStorageLinker ymlLinker = LinkerHandler.getYMLLinker();
		AccountLinker mySQLLinker = LinkerHandler.getMySQLAccountLinker();
		mySQLLinker.getLinkedUids().forEach((uuid, strings) -> {
			strings.forEach(uid -> {
				ymlLinker.linkPlayer(uuid, uid, false);
			});
		});
		ymlLinker.saveLinksFile();
	}

	public static void moveSubcommand(UtilPlayer sender, UtilPlayer target, boolean targetToSender) {
		/*
        Check if sender has permission
        Check if sender is linked
        Check if sender is online
        Check if target is linked
        Check if target is online
        move target -> sender / sender -> target
         */
		if (!(targetToSender ? sender.hasPermission("ts3bot.movehere") : sender.hasPermission("ts3bot.join"))) {
			sender.sendMessage(ConfigManager.getString("message.no-permission"));
			return;
		}
		if (!LinkerHandler.linker.isPlayerLinked(sender.getUUID())) {
			sender.sendMessage(ConfigManager.getString("message.link-required"));
			return;
		}
		Set<ClientInfo> senderTSInfo = MainManager
				.getClientInfosFromMultiple(LinkerHandler.linker
						.getClientUIDbyPlayerUUID(sender.getUUID()));
		if (senderTSInfo.isEmpty()) {
			sender.sendMessage(ConfigManager.getString("message.not-connected"));
			return;
		}
		if (!LinkerHandler.linker.isPlayerLinked(target.getUUID())) {
			sender.sendMessage(ConfigManager.getString("message.target-link-required").replace("<player>", target.getName()));
			return;
		}
		Set<ClientInfo> targetTSInfo = MainManager.getClientInfosFromMultiple(LinkerHandler.linker.getClientUIDbyPlayerUUID(target.getUUID()));
		if (targetTSInfo == null) {
			sender.sendMessage(ConfigManager.getString("message.target-not-connected").replace("<player>", target.getName()));
			return;
		}
		if (targetToSender) {
			if (senderTSInfo.size() > 1) {
				sender.sendMessage(ConfigManager.getMultipleIdentitiesErrorMessage());
				return;
			}
			ClientInfo senderInfo = MainManager.getAny(senderTSInfo);
			int channelId = senderInfo.getChannelId();
			targetTSInfo.forEach(targetInfo -> MainManager.main.getTS3Api().moveClient(targetInfo.getId(), channelId));
		} else {
			if (targetTSInfo.size() > 1) {
				sender.sendMessage(ConfigManager.getMultipleIdentitiesErrorMessage());
				return;
			}
			ClientInfo targetInfo = MainManager.getAny(targetTSInfo);
			int channelId = targetInfo.getChannelId();
			targetTSInfo.forEach(senderInfo -> MainManager.main.getTS3Api().moveClient(senderInfo.getId(), channelId));
		}
		sender.sendMessage(ConfigManager.getString("message.command-success"));
	}

	public static boolean checkTSConnectionAndWarn(UtilCommandSender sender) {
		if (!MainManager.isSelectedTeamspeakServerRunning()) {
			sender.sendMessage(ConfigManager.getString("message.connection-failure"));
			return false;
		}
		return true;
	}

	public static void tsReconnectCommand(UtilCommandSender sender) {
		if (MainManager.isSelectedTeamspeakServerRunning()) {
			sender.sendMessage(ConfigManager.getString("message.already-connected"));
			return;
		}
		sender.sendMessage(ConfigManager.getString("message.attempting-connection"));
		if (!MainManager.main.getTS3Query().isConnected()) {
			MainManager.main.forceAttemptReconnectQuery();
		}
		MainManager.main.forceAttemptReconnectServer();
	}

	public static void ts3botSubCommand(UtilCommandSender sender, String args[]) {
		if (checkArg(args[0], "supportchannel", "schannel", "sc")) {
			//cmd: /ts supportchannel list|(open|close [<index>|all])
			if (!sender.hasPermission(ConfigManager.getRawString("tscommands.supportchannel.permission"))) {
				sender.sendMessage(ConfigManager.getString("message.no-permission"));
				return;
			}
			if (args.length == 2 && args[1].equalsIgnoreCase("list")) {
				listSupportChannelSubCommand(sender);
				return;
			}
			if (args.length != 3) {
				sender.sendMessage(ConfigManager.getString("supportchannel.command.syntax"));
				return;
			}
			if (args[1].equalsIgnoreCase("open")) {
				openCloseSubCommand(sender, true, args[2]);
				return;
			}
			if (args[1].equalsIgnoreCase("close")) {
				openCloseSubCommand(sender, false, args[2]);
				return;
			}
			return;
		}
		sender.sendMessage(ConfigManager.getString("message.unknown-command"));
	}

	private static void listSupportChannelSubCommand(UtilCommandSender sender) {
		List<Integer> channelIDs = SupportChannelListener.getInstance().getSupportChannelIDs();
		if (channelIDs.isEmpty()) {
			sender.sendMessage(ConfigManager.getString("supportchannel.command.no-supportchannels"));
			return;
		}
		sender.sendMessage(ConfigManager.getString("supportchannel.command.list.head"));
		int index = 0;
		for (Integer channelID : channelIDs) {
			sender.sendMessage(ConfigManager.getString("supportchannel.command.list.element")
					.replace("<id>", String.valueOf(channelID))
					.replace("<channelname>", MainManager.main.getTS3Api().getChannelInfo(channelID).getName())
					.replace("<index>", String.valueOf(index++)));
		}
	}

	private static void openCloseSubCommand(UtilCommandSender sender, boolean open, String arg2) {
		List<Integer> toToggle = Lists.newArrayList();
		String configKey = open ? "opened" : "closed";
		if (arg2.equalsIgnoreCase("all")) {
			toToggle.addAll(SupportChannelListener.getInstance().getSupportChannelIDs());
			sender.sendMessage(ConfigManager.getString("supportchannel.command." + configKey + "-all"));
		} else {
			Integer integer = getInt(arg2);
			if (integer == null) {
				sender.sendMessage(ConfigManager.getString("supportchannel.command.int-required"));
				return;
			}
			if (integer < 0 || integer >= SupportChannelListener.getInstance().getSupportChannelIDs().size()) {
				sender.sendMessage(ConfigManager.getString("supportchannel.command.out-of-bounds"));
				return;
			}
			sender.sendMessage(ConfigManager.getString("supportchannel.command." + configKey + "-single")
					.replace("<index>", integer.toString()));
			toToggle.add(SupportChannelListener.getInstance().getSupportChannelIDs().get(integer));
		}
		if (open) {
			toToggle.forEach(SupportChannelListener.getInstance()::openSupportChannel);
		} else {
			toToggle.forEach(SupportChannelListener.getInstance()::closeSupportChannel);
		}
	}

	public static void statusSubCommand(UtilCommandSender sender, String[] args) {
		if (args.length == 0) {
			Menu menu = new Menu(UtilChatColor.GOLD + "Status of TS3Bot [version " + MainManager.main.getVersion() + " - Server is " + MainManager.main.getCBVersion().name() + "]:");
			boolean serverRunning = MainManager.isInitialized() && MainManager.isSelectedTeamspeakServerRunning();
			ModuleHandler.getInstance().getStatusModules()
					.stream()
					.filter(m -> serverRunning || !m.requiresTeamspeakConnection())
					.filter(m -> !m.requiresDatabaseConnection() || MySQL.hasConnection())
					.forEach(module -> {
						menu.addSub(
								new Menu(UtilChatColor.BLUE + module.getName() + ": " + module.getStatus(),
										ClickEvent.Action.RUN_COMMAND,
										"/tsstatus " + module.getName())
						);
					});
			menu.send(sender);
			return;
		}
		StatusModule module = ModuleHandler.getInstance().byName(args[0]);
		if (module == null) {
			sender.sendMessage(UtilChatColor.RED + "The specified module was not found or not loaded.");
			return;
		}
		if (args.length >= 2 && module instanceof FeaturesModule) {
			StatusModule feature = FeaturesModule.getFeature(String.join(" ", args)
					.substring(args[0].length() + 1));
			if (feature == null) {
				sender.sendMessage(UtilChatColor.RED + "The specified feature was not found or not loaded.");
				return;
			}
			module = feature;
		}
		module.getDetailedInfo().send(sender);
	}

	public static void tsInitCommand(UtilCommandSender sender) {
		try {
			MainManager.tsInit();
			sender.sendMessage("TS init was successful.");
		} catch (Exception exc) {
			sender.sendMessage("TS init failed, see console.");
			MainManager.main.getPluginLogger().log(Level.SEVERE, "TS Init command failed", exc);
		}
	}

	@Nullable
	public static Integer getInt(String text) {
		try {
			return Integer.parseInt(text);
		} catch (Exception e) {
			return null;
		}
	}

	public static boolean checkArg(String arg, String... values) {
		for (String value : values) {
			if (arg.equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}

	public static void upgradeLinksFromLegacy() {
		LinkerHandler.initLegacyLinker();
		Map<UUID, String> oldLinks = LinkerHandler.getLegacyAccountLinker().getAllLinks();
		ConfigManager.setLegacyLinking(false);
		LinkerHandler.getLegacyAccountLinker().wipeStorage();
		LinkerHandler.initAccountLinker();
		oldLinks.forEach((uuid, s) -> LinkerHandler.linker.linkPlayer(uuid, s));
	}

	public static void copyLegacyFolder(File dataFolder, Logger logger) {
		if (dataFolder.exists()) {
			return;
		}
		File oldFolder = new File(dataFolder.getParentFile(), "TS3Bot");
		if (!oldFolder.exists()) {
			return;
		}
		try {
			FileUtils.copyDirectory(oldFolder, dataFolder);
			logger.info("Copied legacy folder to new directory.");
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error while trying to copy datafolder");
		}
	}

}
