package me.petomka.ts3bot.commands.bukkit;

import lombok.Getter;
import me.petomka.ts3bot.config.ConfigManager;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;

/**
 * Created by Benedikt on 06.08.2017.
 */
public abstract class Command {

	private final String name;
	private final String permission;
	private final String usage;

	@Getter
	private BaseComponent[] noPermissionMessage = TextComponent.fromLegacyText(ConfigManager.getString("message.no-permission"));

	public Command(String name, String permission, String usage) {
		this.name = name;
		this.permission = permission;
		this.usage = usage;
		CommandManager.registerCommand(this);
	}

	public Command(String name, String permission, String usage, String noPermissionMessage) {
		this(name, permission, usage);
		this.noPermissionMessage = TextComponent.fromLegacyText(noPermissionMessage);
	}

	public abstract boolean execute(CommandSender sender, String label, String[] args);

	public String getName() {
		return name;
	}

	public String getPermission() {
		return permission;
	}

	public String getUsage() {
		return usage;
	}

	public boolean requiresInitialization() {
		return true;
	}
}
