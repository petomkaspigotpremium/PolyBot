package me.petomka.ts3bot.commands.bukkit;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBukkit;
import me.petomka.ts3bot.util.player.UtilPlayerBukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Benedikt on 06.08.2017.
 */
public class CommandLink extends Command {

	public CommandLink() {
		super("link", "ts3bot.link", "/link", ConfigManager.getString("link.no-permission"));
	}

	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command can only be performed by players.");
			return true;
		}
		if (!UniMethods.checkTSConnectionAndWarn(new UtilCommandSenderBukkit(sender))) {
			return true;
		}
		if (args.length == 1) {
			UniMethods.verificationCodeEntered(new UtilPlayerBukkit((Player) sender), args[0]);
			return true;
		}
		if(args.length != 0) {
			return false;
		}
		UniMethods.linkPlayer(new UtilPlayerBukkit((Player) sender), null, null, null, null);
		return true;
	}


}
