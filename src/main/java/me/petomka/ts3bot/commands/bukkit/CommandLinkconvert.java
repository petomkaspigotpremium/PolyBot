package me.petomka.ts3bot.commands.bukkit;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBukkit;
import org.bukkit.command.CommandSender;

/**
 * Created by Benedikt on 05.09.2017.
 */
public class CommandLinkconvert extends Command {

	public CommandLinkconvert() {
		super("linkconvert", "ts3bot.linkconvert", "/linkconvert <mysql/yml>");
	}

	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		UniMethods.linkConvert(new UtilCommandSenderBukkit(sender), args);
		return true;
	}
}
