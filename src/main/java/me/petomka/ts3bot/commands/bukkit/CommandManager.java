package me.petomka.ts3bot.commands.bukkit;

import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.JSONComponent;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

/**
 * Created by Benedikt on 06.08.2017.
 */
public class CommandManager {

	private static ArrayList<Command> commands = new ArrayList<>();

	public static void registerCommand(Command cmd) {
		commands.add(cmd);
	}

	public static boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
		for (Command command : commands) {
			if (command.requiresInitialization() && !MainManager.isInitialized()) {
				sender.sendMessage(ConfigManager.getString("message.init-required"));
				return true;
			}
			if (command.getName().equalsIgnoreCase(cmd.getName())) {
				if (sender.hasPermission(command.getPermission()) || command.getPermission().equals("")) {
					if (!command.execute(sender, label, args)) {
						sender.sendMessage(ConfigManager.getString("message.usage").replaceAll("<usage>", command.getUsage()));
					}
				} else {
					JSONComponent.sendMessage(sender, command.getNoPermissionMessage());
				}
			}
		}
		return true;
	}

}
