package me.petomka.ts3bot.commands.bukkit;

import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.scoreboard.ChannelScoreboardBukkit;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBukkit;
import me.petomka.ts3bot.util.player.UtilPlayerBukkit;
import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Set;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class CommandTS3Bot extends Command {

	public CommandTS3Bot() {
		super("ts3bot", "ts3bot.use", "/ts3bot");
	}

	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		if (args.length == 0) {
			sendTsInvite(sender);
			return true;
		}
		if (!UniMethods.checkTSConnectionAndWarn(new UtilCommandSenderBukkit(sender))) {
			return true;
		}
		if (args[0].equalsIgnoreCase("scoreboard") || args[0].equalsIgnoreCase("sb")) {
			return scoreboardSubCommand(sender);
		} else if (args[0].equalsIgnoreCase("cmd")) {
			return cmdSubCommand(sender);
		} else if (UniMethods.checkArg(args[0], "movehere", "mh", "get")) {
			Player target = pFromArg(sender, args[1]);
			if (target != null) {
				UniMethods.moveSubcommand(new UtilPlayerBukkit((Player) sender), new UtilPlayerBukkit(target), true);
			}
		} else if (UniMethods.checkArg(args[0], "join", "j", "find")) {
			Player target = pFromArg(sender, args[1]);
			if (target != null) {
				UniMethods.moveSubcommand(new UtilPlayerBukkit((Player) sender), new UtilPlayerBukkit(target), false);
			}
		}
		UniMethods.ts3botSubCommand(new UtilCommandSenderBukkit(sender), args);
		return true;
	}

	private Player pFromArg(CommandSender sender, String arg) {
		Player player = Bukkit.getPlayer(arg);
		if (player == null) {
			sender.sendMessage(ConfigManager.getString("message.player-not-found").replace("<player>", arg));
			return null;
		}
		return player;
	}

	private void sendTsInvite(CommandSender sender) {
		Menu menu = new Menu(ConfigManager.getString("ts.invite.message"));
		if (ConfigManager.getBoolean("ts.invite.clickable")) {
			menu.setAction(ClickEvent.Action.OPEN_URL);
			menu.setActionString(ConfigManager.getRawString("ts.invite.click-link").replaceAll("<host>", ConfigManager.getRawString("bot.host"))
					.replaceAll("<port>", String.valueOf(ConfigManager.getInt("ts.invite.port")))
					.replaceAll("<password>", ConfigManager.getRawString("ts.invite.password")));
		}
		if (ConfigManager.getBoolean("ts.invite.tooltip-enable")) {
			menu.setDescription(ConfigManager.getString("ts.invite.tooltip"));
		}
		menu.send(new UtilCommandSenderBukkit(sender));
	}

	private boolean scoreboardSubCommand(CommandSender sender) {
		if (!sender.hasPermission("ts3bot.scoreboard")) {
			sender.sendMessage(ConfigManager.getString("message.no-permission"));
			return true;
		}
		if (!(sender instanceof Player)) {
			sender.sendMessage(ConfigManager.getRawString("message.from-console"));
			return true;
		}
		if (!LinkerHandler.linker.isPlayerLinked(((Player) sender).getUniqueId())) {
			sender.sendMessage(ConfigManager.getString("message.link-required"));
			return true;
		}
		Set<String> uids = MainManager.getOnlineLinkedUids(((Player) sender).getUniqueId());
		if (uids.isEmpty()) {
			sender.sendMessage(ConfigManager.getString("ts.scoreboard.not-connected"));
			return true;
		}
		if (uids.size() > 1) {
			sender.sendMessage(ConfigManager.getMultipleIdentitiesErrorMessage());
			return true;
		}
		if (ChannelScoreboardBukkit.getBoard(new UtilPlayerBukkit((Player) sender)) != null) {
			ChannelScoreboardBukkit.getBoard(new UtilPlayerBukkit((Player) sender)).dispose();
			return true;
		}
		MainManager.main.getTS3Api().addTS3Listeners(new ChannelScoreboardBukkit((Player) sender));
		return true;
	}

	private boolean cmdSubCommand(CommandSender sender) {
		if (!LinkerHandler.linker.isPlayerLinked(((Player) sender).getUniqueId())) {
			sender.sendMessage(ConfigManager.getString("message.link-required"));
			return true;
		}
		Set<String> uids = MainManager.getOnlineLinkedUids(((Player) sender).getUniqueId());
		if (uids.isEmpty()) {
			sender.sendMessage(ConfigManager.getString("ts.scoreboard.not-connected"));
			return true;
		}
		uids.stream()
				.map(MainManager.main.getTS3Api()::getClientByUId)
				.map(ClientInfo::getId)
				.forEach(id -> {
					MainManager.main.getTS3Api().sendPrivateMessage(id, " ");
				});
		return true;
	}
}
