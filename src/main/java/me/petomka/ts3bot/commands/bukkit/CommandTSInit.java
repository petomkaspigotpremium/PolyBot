package me.petomka.ts3bot.commands.bukkit;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBukkit;
import org.bukkit.command.CommandSender;

public class CommandTSInit extends Command {

	public CommandTSInit() {
		super("tsinit", "ts3bot.tsinit", "tsinit");
	}

	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		UniMethods.tsInitCommand(new UtilCommandSenderBukkit(sender));
		return true;
	}

	@Override
	public boolean requiresInitialization() {
		return false;
	}
}
