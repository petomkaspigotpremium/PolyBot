package me.petomka.ts3bot.commands.bukkit;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBukkit;
import org.bukkit.command.CommandSender;

public class CommandTSReconnect extends Command {

	public CommandTSReconnect() {
		super("tsreconnect", "ts3bot.tsreconnect", "/tsreconnect");
	}

	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		UniMethods.tsReconnectCommand(new UtilCommandSenderBukkit(sender));
		return true;
	}
}
