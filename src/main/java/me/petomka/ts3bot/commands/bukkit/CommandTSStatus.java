package me.petomka.ts3bot.commands.bukkit;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBukkit;
import org.bukkit.command.CommandSender;

public class CommandTSStatus extends Command {

	public CommandTSStatus() {
		super("tsstatus", "ts3bot.tsstatus", "/ts status");
	}

	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		UniMethods.statusSubCommand(new UtilCommandSenderBukkit(sender), args);
		return true;
	}

	@Override
	public boolean requiresInitialization() {
		return false;
	}
}
