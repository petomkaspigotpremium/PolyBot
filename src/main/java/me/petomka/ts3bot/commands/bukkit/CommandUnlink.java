package me.petomka.ts3bot.commands.bukkit;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBukkit;
import me.petomka.ts3bot.util.player.UtilPlayerBukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Benedikt on 10.08.2017.
 */
public class CommandUnlink extends Command {

	public CommandUnlink() {
		super("unlink", "ts3bot.unlink", "/unlink [commandsender]");
	}

	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ConfigManager.getRawString("message.from-console"));
			return true;
		}
		if (!UniMethods.checkTSConnectionAndWarn(new UtilCommandSenderBukkit(sender))) {
			return true;
		}
		if (!LinkerHandler.linker.isPlayerLinked(((Player) sender).getUniqueId())) {
			sender.sendMessage(ConfigManager.getString("message.link-required"));
			return true;
		}
		Player player = (Player) sender;
		LinkerHandler.linker.getClientUIDbyPlayerUUID(player.getUniqueId()).forEach(uid -> {
			UniMethods.unlinkPlayer(
					new UtilPlayerBukkit(player),
					player.getUniqueId(),
					uid,
					null,
					null,
					null,
					null);
		});
		return true;
	}
}
