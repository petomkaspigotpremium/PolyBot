package me.petomka.ts3bot.commands.bungee;

import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Plugin;

import javax.annotation.Nullable;

/**
 * Created by Benedikt on 07.08.2017.
 */
public abstract class Command extends net.md_5.bungee.api.plugin.Command {

	private final String name;
	private final String permission;
	private final String usage;

	private BaseComponent[] permissionMessage = TextComponent.fromLegacyText(ConfigManager.getString("message.no-permission"));

	public Command(String name, String permission, String usage, @Nullable String[] aliases) {
		super(name, "", aliases != null ? aliases : new String[0]);
		this.name = name;
		this.permission = permission;
		this.usage = usage;
		ProxyServer.getInstance().getPluginManager().registerCommand((Plugin) MainManager.main, this);
	}

	public Command(String name, String permission, String usage) {
		this(name, permission, usage, null);
	}

	public Command(String name, String permission, String usage, String customPermissionMessage, String[] aliases) {
		this(name, permission, usage, aliases);
		this.permissionMessage = TextComponent.fromLegacyText(customPermissionMessage);
	}

	@Override
	public final void execute(CommandSender sender, String[] args) {
		if (sender.hasPermission(permission)) {
			if (requiresInitialization() && !MainManager.isInitialized()) {
				sender.sendMessage(TextComponent.fromLegacyText(ConfigManager.getString("message.init-required")));
				return;
			}
			if (!onCommand(sender, args)) {
				sender.sendMessage(new ComponentBuilder(ConfigManager.getString("message.usage").replaceAll("<usage>", usage)).create());
			}
		} else {
			sender.sendMessage(permissionMessage);
		}
	}

	public abstract boolean onCommand(CommandSender sender, String[] args);

	public String getName() {
		return name;
	}

	public String getPermission() {
		return "";
	}

	public String getUsage() {
		return usage;
	}

	public boolean requiresInitialization() {
		return true;
	}

}
