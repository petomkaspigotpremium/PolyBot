package me.petomka.ts3bot.commands.bungee;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBungee;
import me.petomka.ts3bot.util.player.UtilPlayerBungee;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class CommandLink extends Command {

	public CommandLink() {
		super("link", "ts3bot.link", "/ts3bot",
				ConfigManager.getString("link.no-permission"), new String[0]);
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(TextComponent.fromLegacyText("This command can only be performed by players."));
			return true;
		}
		if (!UniMethods.checkTSConnectionAndWarn(new UtilCommandSenderBungee(sender))) {
			return true;
		}
		if (args.length == 1) {
			UniMethods.verificationCodeEntered(new UtilPlayerBungee((ProxiedPlayer) sender), args[0]);
			return true;
		}
		if (args.length != 0) {
			return false;
		}
		UniMethods.linkPlayer(new UtilPlayerBungee((ProxiedPlayer) sender), null, null, null, null);
		return true;
	}
}
