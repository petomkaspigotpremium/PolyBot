package me.petomka.ts3bot.commands.bungee;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBungee;
import net.md_5.bungee.api.CommandSender;

/**
 * Created by Benedikt on 05.09.2017.
 */
public class CommandLinkconvert extends Command {

	public CommandLinkconvert() {
		super("linkconvert", "ts3bot.linkconvert", "/linkconvert <mysql/yml>");
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		UniMethods.linkConvert(new UtilCommandSenderBungee(sender), args);
		return true;
	}
}
