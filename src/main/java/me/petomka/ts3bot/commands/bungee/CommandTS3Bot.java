package me.petomka.ts3bot.commands.bungee;

import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.scoreboard.ChannelScoreboard;
import me.petomka.ts3bot.scoreboard.ChannelScoreboardBungee;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBungee;
import me.petomka.ts3bot.util.player.UtilPlayerBungee;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Set;

/**
 * Created by Benedikt on 08.08.2017.
 */
@SuppressWarnings("deprecation")
public class CommandTS3Bot extends Command {

	public CommandTS3Bot() {
		super("ts3bot", "ts3bot.use", "/ts3bot [help]", new String[]{"ts", "ts3"});
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (args.length == 0) {
			/*No args -> send ts invite link*/
			sendTsInvite(sender);
			return true;
		}
		if (!UniMethods.checkTSConnectionAndWarn(new UtilCommandSenderBungee(sender))) {
			return true;
		}
		if (args[0].equalsIgnoreCase("scoreboard") || args[0].equals("sb")) {
			/*Scoreboard sub command*/
			return scoreboardSubCommand(sender);
		} else if (args[0].equalsIgnoreCase("cmd")) {
			/*cmd sub command*/
			return cmdSubCommand(sender);
		} else if (UniMethods.checkArg(args[0], "movehere", "mh", "get")) {
			ProxiedPlayer target = pFromArg(sender, args[1]);
			if (target != null) {
				UniMethods.moveSubcommand(new UtilPlayerBungee((ProxiedPlayer) sender), new UtilPlayerBungee(target), true);
			}
		} else if (UniMethods.checkArg(args[0], "join", "j", "find")) {
			ProxiedPlayer target = pFromArg(sender, args[1]);
			if (target != null) {
				UniMethods.moveSubcommand(new UtilPlayerBungee((ProxiedPlayer) sender), new UtilPlayerBungee(target), false);
			}
		}
		UniMethods.ts3botSubCommand(new UtilCommandSenderBungee(sender), args);
		return true;
	}

	private ProxiedPlayer pFromArg(CommandSender sender, String arg) {
		ProxiedPlayer player = ProxyServer.getInstance().getPlayer(arg);
		if (player == null) {
			sender.sendMessage(ConfigManager.getString("message.player-not-found").replace("<player>", arg));
			return null;
		}
		return player;
	}

	private void sendTsInvite(CommandSender sender) {
		ComponentBuilder cp = new ComponentBuilder(ConfigManager.getString("ts.invite.message"));
		if (ConfigManager.getBoolean("ts.invite.clickable")) {
			cp.event(new ClickEvent(ClickEvent.Action.OPEN_URL, ConfigManager.getRawString("ts.invite.click-link").replaceAll("<host>",
					ConfigManager.getRawString("bot.host")).replaceAll("<port>", String.valueOf(ConfigManager.getInt("ts.invite.port")))
					.replaceAll("<password>", ConfigManager.getRawString("ts.invite.password"))));
		}
		if (ConfigManager.getBoolean("ts.invite.tooltip-enable")) {
			cp.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ConfigManager.getString("ts.invite.tooltip")).create()));
		}
		sender.sendMessage(cp.create());
	}

	private boolean scoreboardSubCommand(CommandSender sender) {
		if (!sender.hasPermission("ts3bot.scoreboard")) {
			sender.sendMessage(ConfigManager.getString("message.no-permission"));
			return true;
		}
		if (!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ConfigManager.getRawString("message.from-console"));
			return true;
		}
		if (!LinkerHandler.linker.isPlayerLinked(((ProxiedPlayer) sender).getUniqueId())) {
			sender.sendMessage(ConfigManager.getString("message.link-required"));
			return true;
		}
		Set<String> uids = MainManager.getOnlineLinkedUids(((ProxiedPlayer) sender).getUniqueId());
		if (uids.isEmpty()) {
			sender.sendMessage(ConfigManager.getString("ts.scoreboard.not-connected"));
			return true;
		}
		if (uids.size() > 1) {
			sender.sendMessage(ConfigManager.getMultipleIdentitiesErrorMessage());
			return true;
		}
		if (ChannelScoreboard.getBoard(new UtilPlayerBungee((ProxiedPlayer) sender)) != null) {
			ChannelScoreboardBungee.getBoard(new UtilPlayerBungee((ProxiedPlayer) sender)).dispose();
			return true;
		}
		MainManager.main.getTS3Api().addTS3Listeners(new ChannelScoreboardBungee((ProxiedPlayer) sender));
		return true;
	}

	private boolean cmdSubCommand(CommandSender sender) {
		if (!LinkerHandler.linker.isPlayerLinked(((ProxiedPlayer) sender).getUniqueId())) {
			sender.sendMessage(ConfigManager.getString("message.link-required"));
			return true;
		}
		Set<String> uids = MainManager.getOnlineLinkedUids(((ProxiedPlayer) sender).getUniqueId());
		if (uids.isEmpty()) {
			sender.sendMessage(ConfigManager.getString("ts.scoreboard.not-connected"));
			return true;
		}
		uids.stream()
				.map(MainManager.main.getTS3Api()::getClientByUId)
				.map(ClientInfo::getId)
				.forEach(id -> {
					MainManager.main.getTS3Api().sendPrivateMessage(id, " ");
				});
		return true;
	}
}
