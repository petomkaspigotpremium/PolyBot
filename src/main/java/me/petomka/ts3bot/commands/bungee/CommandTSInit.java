package me.petomka.ts3bot.commands.bungee;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBungee;
import net.md_5.bungee.api.CommandSender;

public class CommandTSInit extends Command {

	public CommandTSInit() {
		super("tsinit", "ts3bot.tsinit", "/tsinit");
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		UniMethods.tsInitCommand(new UtilCommandSenderBungee(sender));
		return true;
	}

	@Override
	public boolean requiresInitialization() {
		return false;
	}

}
