package me.petomka.ts3bot.commands.bungee;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBungee;
import net.md_5.bungee.api.CommandSender;

public class CommandTSReconnect extends Command {

	public CommandTSReconnect() {
		super("tsreconnect", "ts3bot.tsreconnect", "/tsreconnect", new String[]{"tsrc"});
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		UniMethods.tsReconnectCommand(new UtilCommandSenderBungee(sender));
		return true;
	}

}
