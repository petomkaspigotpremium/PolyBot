package me.petomka.ts3bot.commands.bungee;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBungee;
import net.md_5.bungee.api.CommandSender;

public class CommandTSStatus extends Command {

	public CommandTSStatus() {
		super("tsstatus", "ts3bot.tsstatus", "/tsstatus", new String[]{"ts3status", "tss"});
	}

	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		UniMethods.statusSubCommand(new UtilCommandSenderBungee(sender), args);
		return true;
	}

	@Override
	public boolean requiresInitialization() {
		return false;
	}
}
