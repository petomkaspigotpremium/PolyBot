package me.petomka.ts3bot.commands.bungee;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBungee;
import me.petomka.ts3bot.util.player.UtilPlayerBungee;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created by Benedikt on 10.08.2017.
 */
public class CommandUnlink extends Command {

	public CommandUnlink() {
		super("unlink", "ts3bot.unlink", "/unlink");
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, String[] args) {
		if (!(sender instanceof ProxiedPlayer)) {

			return true;
		}
		if (!UniMethods.checkTSConnectionAndWarn(new UtilCommandSenderBungee(sender))) {
			return true;
		}
		if (!LinkerHandler.linker.isPlayerLinked(((ProxiedPlayer) sender).getUniqueId())) {
			sender.sendMessage(ConfigManager.getString("message.link-required"));
			return true;
		}
		ProxiedPlayer player = (ProxiedPlayer) sender;
		LinkerHandler.linker.getClientUIDbyPlayerUUID(player.getUniqueId()).forEach(uid -> {
			UniMethods.unlinkPlayer(
					new UtilPlayerBungee(player),
					player.getUniqueId(),
					uid,
					null,
					null,
					null,
					null);
		});
		return true;
	}
}
