package me.petomka.ts3bot.config;

import java.util.Set;
import java.util.UUID;

public abstract class AccountLinker extends LinkerHandler {

	public AccountLinker(LinkerType linkerType) {
		super(linkerType);
	}

	public abstract UUID getPlayerUUIDbyClientUID(String uid);

	public abstract Set<String> getClientUIDbyPlayerUUID(UUID uuid);

	public abstract void linkPlayer(UUID uuid, String uid);

	public abstract void unlinkPlayer(UUID uuid, String uid);

	public abstract void unlinkClient(String uid);

	public abstract boolean isPlayerLinked(UUID uuid);

	public abstract boolean isClientLinked(String uid);

}
