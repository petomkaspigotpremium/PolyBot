package me.petomka.ts3bot.config;

import me.petomka.ts3bot.util.UtilChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class BungeeMessageColorizer extends MessageColorizer {

	public String colorizeString(String entry) {
		return BaseComponent.toLegacyText(TextComponent.fromLegacyText(UtilChatColor.translateAlternateColorCodes('&', entry)));
	}

}
