package me.petomka.ts3bot.config;

import lombok.Getter;
import lombok.experimental.UtilityClass;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.UtilChatColor;

import java.util.List;

/**
 * Created by Benedikt on 07.08.2017.
 */
@UtilityClass
public class ConfigManager {

	private String DATA_KEY = "datadonotchangethis.";

	@Getter
	private boolean legacyLinking = getBoolean(DATA_KEY + "was-legacy-link");

	@Getter
	private int linkLimit = getInt("link.link-limit");

	@Getter
	private String linkLimitReachedMessage = getString("message.link-limit-reached");

	public static void setLegacyLinking(boolean value) {
		legacyLinking = value;
		MainManager.main.configSet(DATA_KEY + "was-legacy-link", value);
	}

	@Getter
	private String multipleIdentitiesErrorMessage = getString("message.multiple-identites-error");

	public String getString(String path) {
		if (MainManager.main.getMainType() == MainManager.MainType.BUKKIT) {
			return colorize(MainManager.main.getString(path))
					.replace("<prefix>", colorize(MainManager.main.getString("message.prefix")));
		} else {
			return ((BungeeMessageColorizer) MessageColorizer.messageColorizer).colorizeString(MainManager.main.getString(path))
					.replace("<prefix>", colorize(MainManager.main.getString("message.prefix")));
		}
	}

	public String getRawString(String path) {
		return MainManager.main.getString(path);
	}

	public int getInt(String path) {
		return MainManager.main.getInt(path);
	}

	public boolean getBoolean(String path) {
		return MainManager.main.getBoolean(path);
	}

	public double getDouble(String path) {
		return MainManager.main.getDouble(path);
	}

	public List<String> getStringList(String path) {
		return MainManager.main.getStringList(path);
	}

	public List<Integer> getIntegerList(String path) {
		return MainManager.main.getIntegerList(path);
	}

	public String colorize(String entry) {
		return UtilChatColor.translateAlternateColorCodes('&', entry);
	}

}
