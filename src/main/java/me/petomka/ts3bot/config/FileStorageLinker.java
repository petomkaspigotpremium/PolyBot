package me.petomka.ts3bot.config;

import java.util.UUID;

public abstract class FileStorageLinker extends AccountLinker {

	public FileStorageLinker(LinkerType linkerType) {
		super(linkerType);
	}

	public abstract void linkPlayer(UUID uuid, String uid, boolean saveFile);

	public abstract void saveLinksFile();

}
