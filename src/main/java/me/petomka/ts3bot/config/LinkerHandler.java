package me.petomka.ts3bot.config;

import com.google.common.collect.*;
import lombok.Getter;
import me.petomka.ts3bot.config.legacy.LegacyAccountLinker;
import me.petomka.ts3bot.config.legacy.LegacyMySQLAccountLinker;
import me.petomka.ts3bot.config.legacy.LegacyYamlAccountLinkerBukkit;
import me.petomka.ts3bot.config.legacy.LegacyYamlAccountLinkerBungee;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.mysql.MySQL;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.logging.Level;

/**
 * Created by Benedikt on 07.08.2017.
 */
public abstract class LinkerHandler {

	public static AccountLinker linker;

	@Getter
	private static LegacyAccountLinker legacyAccountLinker = null;

	private LinkerType linkerType;
	private Map<UUID, Set<String>> playerLinks = Maps.newHashMap();

	public LinkerHandler(LinkerType linkerType) {
		this.linkerType = linkerType;
	}

	public static void initAccountLinker() {
		if(ConfigManager.isLegacyLinking()) {
			return;
		}
		String storageType = ConfigManager.getRawString("storage-method");
		if (storageType.equalsIgnoreCase("yml") || storageType.equalsIgnoreCase("yaml")) {
			linker = getYMLLinker();
		} else if (storageType.equalsIgnoreCase("mysql")) {
			linker = getMySQLAccountLinker();
		} else {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Enter a valid storage method in your config.yml! This can either " +
					"be \"yml\" or \"yaml\" for .yml-files, or \"mysql\" to use your MySQL-Database!");
		}
	}

	public static @Nonnull FileStorageLinker getYMLLinker() {
		if(linker != null && linker.getLinkerType() == LinkerType.YML) {
			return (FileStorageLinker) linker;
		}
		switch (MainManager.mainType) {
			case BUNGEE:
				return new YMLAccountLinkerBungee();
			case BUKKIT:
				return new YMLAccountLinkerBukkit();
		}
		throw new RuntimeException("Could not find any YML-Linker");
	}

	public static @Nonnull AccountLinker getMySQLAccountLinker() {
		if(linker != null && linker.getLinkerType() == LinkerType.MYSQL) {
			return linker;
		}
		MySQL.initMySQL();
		return new MySQLAccountLinker();
	}

	public static void initLegacyLinker() {
		if (legacyAccountLinker != null) {
			return;
		}
		String storageType = ConfigManager.getRawString("storage-method");
		switch (storageType.toLowerCase()) {
			case "yml":
			case "yaml":
				switch (MainManager.mainType) {
					case BUKKIT:
						legacyAccountLinker = new LegacyYamlAccountLinkerBukkit();
						break;
					case BUNGEE:
						legacyAccountLinker = new LegacyYamlAccountLinkerBungee();
				}
				break;
			case "mysql":
				legacyAccountLinker = new LegacyMySQLAccountLinker();
				break;
		}
	}

	protected @Nullable
	PlayerLink getLoadedLink(String uid) {
		UUID uuid = playerLinks.entrySet()
				.stream()
				.filter(link -> link.getValue().contains(uid))
				.findAny()
				.map(Map.Entry::getKey)
				.orElse(null);
		if (uuid == null) {
			return null;
		}
		return new PlayerLink(uuid, playerLinks.get(uuid));
	}

	protected @Nonnull
	PlayerLink getLoadedLink(UUID uuid) {
		return new PlayerLink(uuid, playerLinks.getOrDefault(uuid, ImmutableSet.of()));
	}

	protected void addLoadedLink(UUID uuid, String uid) {
		Set<String> linkedUids = playerLinks.getOrDefault(uuid, Sets.newHashSet());
		linkedUids.add(uid);
		playerLinks.put(uuid, linkedUids);
	}

	protected void setLoadedLinks(UUID uuid, Collection<String> uids) {
		Set<String> linkedUids = Sets.newLinkedHashSet(uids);
		playerLinks.put(uuid, linkedUids);
	}

	protected void removeLoadedLink(UUID uuid, String uid) {
		Set<String> linkedUids = getLoadedLink(uuid).getUids();
		if(linkedUids.isEmpty()) {
			return;
		}
		linkedUids.remove(uid);
		playerLinks.put(uuid, linkedUids);
	}

	protected void removeLoadedLinks(UUID uuid) {
		playerLinks.remove(uuid);
	}

	public LinkerType getLinkerType() {
		return linkerType;
	}

	public enum LinkerType {
		YML,
		MYSQL
	}

	public @Nonnull Map<UUID, Set<String>> getLinkedUids() {
		return playerLinks;
	}

}
