package me.petomka.ts3bot.config;

/**
 * Created by Benedikt on 07.08.2017.
 */
public abstract class MessageColorizer {

	public static MessageColorizer messageColorizer = null;

	public static void initMessageColorizerBungee() {
		messageColorizer = new BungeeMessageColorizer();
	}

}
