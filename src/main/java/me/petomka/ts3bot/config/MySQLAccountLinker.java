package me.petomka.ts3bot.config;

import com.google.common.collect.Maps;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.mysql.MySQL;
import me.petomka.ts3bot.mysql.SQLUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

import static me.petomka.ts3bot.mysql.MySQL.TABLE_PREFIX;

public class MySQLAccountLinker extends AccountLinker {

	private Map<UUID, Integer> playerDatabaseIds = Maps.newHashMap();

	public MySQLAccountLinker() {
		super(LinkerType.MYSQL);
		createTables();
		loadAllLinks();
	}

	private void createTables() {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement(
					"CREATE TABLE IF NOT EXISTS " + TABLE_PREFIX + "link_ids (`uuid` VARCHAR (36) NOT NULL , " +
							"`id` INT NOT NULL AUTO_INCREMENT, PRIMARY KEY (`uuid`), KEY (`id`))")) {
				stmt.executeUpdate();
			}
			try (PreparedStatement stmt = connection.prepareStatement(
					"CREATE TABLE IF NOT EXISTS " + TABLE_PREFIX + "link_uids (`id` INT NOT NULL, " +
							"`uid` VARCHAR (40) NOT NULL, FOREIGN KEY (`id`) REFERENCES " + TABLE_PREFIX +
							"link_ids(`id`) ON DELETE CASCADE)"
			)) {
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Failed to create tables", e);
		}
	}

	private void loadAllLinks() {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM " + TABLE_PREFIX + "link_ids")) {
				try (ResultSet resultSet = stmt.executeQuery()) {
					while (resultSet.next()) {
						UUID uuid = SQLUtils.getUUID(resultSet, "uuid");
						int id = SQLUtils.getInt(resultSet, "id");
						playerDatabaseIds.put(uuid, id);
					}
				}
			}
		} catch (SQLException e) {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Failed to load links", e);
		}
		playerDatabaseIds.forEach(this::loadLinks);
	}

	private void loadLinks(UUID uuid, int id) {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT uid FROM " + TABLE_PREFIX + "link_uids " +
					"WHERE id = ?")) {
				SQLUtils.setInt(stmt, 1, id);

				try (ResultSet resultSet = stmt.executeQuery()) {
					while (resultSet.next()) {
						String uid = SQLUtils.getString(resultSet, "uid");
						addLoadedLink(uuid, uid);
					}
				}
			}
		} catch (SQLException e) {
			MainManager.main.getPluginLogger().log(Level.SEVERE,
					"Error loading linked UIDs for UUID " + uuid, e);
		}
	}

	private void addUserToDB(UUID uuid) {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("INSERT INTO " + TABLE_PREFIX + "link_ids " +
					"(`uuid`) VALUES (?)", PreparedStatement.RETURN_GENERATED_KEYS)) {
				SQLUtils.setUUID(stmt, 1, uuid);
				stmt.executeUpdate();
				try (ResultSet resultSet = stmt.getGeneratedKeys()) {
					resultSet.next();
					int playerId = resultSet.getInt(1);
					playerDatabaseIds.put(uuid, playerId);
				}
			}
		} catch (SQLException e) {
			MainManager.main.getPluginLogger().log(Level.SEVERE,
					"Error adding UUID " + uuid + " to database", e);
		}
	}

	private void saveDBLink(UUID uuid, String uid) {
		Integer playerDBid = playerDatabaseIds.get(uuid);
		if (playerDBid == null) {
			addUserToDB(uuid);
			playerDBid = playerDatabaseIds.get(uuid);
		}
		if (playerDBid == null) {
			return;
		}
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("INSERT INTO " + TABLE_PREFIX + "link_uids" +
					"(`id`, `uid`) VALUES (?, ?)")) {
				SQLUtils.setInt(stmt, 1, playerDBid);
				SQLUtils.setString(stmt, 2, uid);

				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void deleteDBLink(UUID uuid, String uid) {
		Integer playerDBId = playerDatabaseIds.get(uuid);
		if (playerDBId == null) {
			MainManager.main.getPluginLogger().log(Level.WARNING,
					"Tried to delete uid from user nonexistent in database: " + uuid + " // " + uid);
			return;
		}
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("DELETE FROM " + TABLE_PREFIX + "link_uids " +
					"WHERE id = ? AND uid = ?")) {
				SQLUtils.setInt(stmt, 1, playerDBId);
				SQLUtils.setString(stmt, 2, uid);

				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public UUID getPlayerUUIDbyClientUID(String uid) {
		PlayerLink link = getLoadedLink(uid);
		if (link == null) {
			return null;
		}
		return link.getUuid();
	}

	@Override
	public Set<String> getClientUIDbyPlayerUUID(UUID uuid) {
		return getLoadedLink(uuid).getUids();
	}

	@Override
	public void linkPlayer(UUID uuid, String uid) {
		saveDBLink(uuid, uid);
		addLoadedLink(uuid, uid);
	}

	@Override
	public void unlinkPlayer(UUID uuid, String uid) {
		deleteDBLink(uuid, uid);
		removeLoadedLink(uuid, uid);
	}

	@Override
	public void unlinkClient(String uid) {
		PlayerLink link = getLoadedLink(uid);
		if (link == null) {
			return;
		}
		unlinkPlayer(link.getUuid(), uid);
	}

	@Override
	public boolean isPlayerLinked(UUID uuid) {
		return !getLoadedLink(uuid).getUids().isEmpty();
	}

	@Override
	public boolean isClientLinked(String uid) {
		return getLoadedLink(uid) != null;
	}

}
