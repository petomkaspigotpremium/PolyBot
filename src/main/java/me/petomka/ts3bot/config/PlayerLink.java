package me.petomka.ts3bot.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import java.util.Set;
import java.util.UUID;

@RequiredArgsConstructor
@Getter
public class PlayerLink {

	private final @Nonnull UUID uuid;

	private final @Nonnull Set<String> uids;

}
