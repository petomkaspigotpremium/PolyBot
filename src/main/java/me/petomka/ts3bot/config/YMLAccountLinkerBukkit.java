package me.petomka.ts3bot.config;

import me.petomka.ts3bot.main.MainManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

public class YMLAccountLinkerBukkit extends FileStorageLinker {

	private FileConfiguration links;
	private File linksFile;

	public YMLAccountLinkerBukkit() {
		super(LinkerHandler.LinkerType.YML);
		File file = new File(MainManager.main.getDataFolder(), "links.yml");
		if (!file.exists()) {
			try {
				file.createNewFile(); //ignore output
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		linksFile = file;
		links = YamlConfiguration.loadConfiguration(file);
		links.getKeys(false).forEach(uuidKey -> {
			try {
				List<String> uuidLinks = links.getStringList(uuidKey);
				if (uuidLinks == null || uuidLinks.isEmpty()) {
					return;
				}
				setLoadedLinks(UUID.fromString(uuidKey), uuidLinks);
			} catch (Exception e) {
				MainManager.main.getPluginLogger().log(Level.WARNING,
						"Invalid key in links.yml: " + uuidKey + " (" + e.getMessage() + ")");
			}
		});
	}

	@Override
	public void saveLinksFile() {
		try {
			links.save(linksFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public UUID getPlayerUUIDbyClientUID(String uid) {
		PlayerLink link = getLoadedLink(uid);
		if (link == null) {
			return null;
		}
		return link.getUuid();
	}

	@Override
	public Set<String> getClientUIDbyPlayerUUID(UUID uuid) {
		return getLoadedLink(uuid).getUids();
	}

	@Override
	public void linkPlayer(UUID uuid, String uid, boolean saveFile) {
		MainManager.main.async(() -> {
			List<String> uuidLinks = links.getStringList(uuid.toString());
			uuidLinks.add(uid);
			links.set(uuid.toString(), uuidLinks);
			if (saveFile) {
				saveLinksFile();
			}
		});
		addLoadedLink(uuid, uid);
	}

	@Override
	public void linkPlayer(UUID uuid, String uid) {
		this.linkPlayer(uuid, uid, true);
	}

	@Override
	public void unlinkPlayer(UUID uuid, String uid) {
		MainManager.main.async(() -> {
			List<String> uuidLinks = links.getStringList(uuid.toString());
			uuidLinks.remove(uid);
			links.set(uuid.toString(), uuidLinks);
			saveLinksFile();
		});
		removeLoadedLink(uuid, uid);
	}

	@Override
	public void unlinkClient(String uid) {
		PlayerLink link = getLoadedLink(uid);
		if (link == null) {
			return;
		}
		unlinkPlayer(link.getUuid(), uid);
	}

	@Override
	public boolean isPlayerLinked(UUID uuid) {
		return !getLoadedLink(uuid).getUids().isEmpty();
	}

	@Override
	public boolean isClientLinked(String uid) {
		return getLoadedLink(uid) != null;
	}
}
