package me.petomka.ts3bot.config;

import com.google.common.io.ByteStreams;
import me.petomka.ts3bot.main.BungeeMain;
import me.petomka.ts3bot.main.MainManager;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

public class YMLAccountLinkerBungee extends FileStorageLinker {

	private Configuration links;
	private File linksFile;

	public YMLAccountLinkerBungee() {
		super(LinkerType.YML);
		File configFile = new File(MainManager.main.getDataFolder(), "links.yml");
		if (!configFile.exists()) {
			try {
				configFile.createNewFile();
				try (InputStream is = ((BungeeMain) MainManager.main).getResourceAsStream("me/petomka/ts3bot/bungeefiles/links.yml");
					 OutputStream os = new FileOutputStream(configFile)) {
					ByteStreams.copy(is, os);
				}
			} catch (IOException e) {
				throw new RuntimeException("Unable to create configuration file", e);
			}
		}
		try {
			links = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
			linksFile = configFile;
			links.getKeys().forEach(uuidKey -> {
				try {
					List<String> uuidLinks = links.getStringList(uuidKey);
					if (uuidLinks.isEmpty()) {
						return;
					}
					setLoadedLinks(UUID.fromString(uuidKey), uuidLinks);
				} catch (Exception e) {
					MainManager.main.getPluginLogger().log(Level.WARNING,
							"Invalid key in links.yml: " + uuidKey + " (" + e.getMessage() + ")");
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void saveLinksFile() {
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(links, linksFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public UUID getPlayerUUIDbyClientUID(String uid) {
		PlayerLink link = getLoadedLink(uid);
		if (link == null) {
			return null;
		}
		return link.getUuid();
	}

	@Override
	public Set<String> getClientUIDbyPlayerUUID(UUID uuid) {
		return getLoadedLink(uuid).getUids();
	}

	@Override
	public void linkPlayer(UUID uuid, String uid, boolean saveFile) {
		MainManager.main.async(() -> {
			List<String> uuidLinks = links.getStringList(uuid.toString());
			uuidLinks.add(uid);
			links.set(uuid.toString(), uuidLinks);
			if (saveFile) {
				saveLinksFile();
			}
		});
		addLoadedLink(uuid, uid);
	}

	@Override
	public void linkPlayer(UUID uuid, String uid) {
		this.linkPlayer(uuid, uid, true);
	}

	@Override
	public void unlinkPlayer(UUID uuid, String uid) {
		MainManager.main.async(() -> {
			List<String> uuidLinks = links.getStringList(uuid.toString());
			uuidLinks.remove(uid);
			links.set(uuid.toString(), uuidLinks);
			saveLinksFile();
		});
		removeLoadedLink(uuid, uid);
	}

	@Override
	public void unlinkClient(String uid) {
		PlayerLink link = getLoadedLink(uid);
		if (link == null) {
			return;
		}
		unlinkPlayer(link.getUuid(), uid);
	}

	@Override
	public boolean isPlayerLinked(UUID uuid) {
		return !getLoadedLink(uuid).getUids().isEmpty();
	}

	@Override
	public boolean isClientLinked(String uid) {
		return getLoadedLink(uid) != null;
	}
}
