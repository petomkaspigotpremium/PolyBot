package me.petomka.ts3bot.config.legacy;

import com.google.common.collect.ImmutableSet;
import me.petomka.ts3bot.config.AccountLinker;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Benedikt on 07.08.2017.
 */
public abstract class LegacyAccountLinker extends AccountLinker {

	public LegacyAccountLinker(LinkerType linkerType) {
		super(linkerType);
	}

	@Override
	public Set<String> getClientUIDbyPlayerUUID(UUID uuid) {
		String uid = legacyGetClientUIDbyPlayerUUID(uuid);
		if (uuid != null) {
			return ImmutableSet.of(uid);
		}
		return ImmutableSet.of();
	}

	@Override
	public void unlinkPlayer(UUID uuid, String uid) {
		unlinkPlayer(uuid);
	}

	public abstract void wipeStorage();

	public abstract UUID getPlayerUUIDbyClientUID(String uid);

	public abstract String legacyGetClientUIDbyPlayerUUID(UUID uuid);

	public abstract void linkPlayer(UUID uuid, String uid);

	public abstract void unlinkPlayer(UUID uuid);

	public abstract void unlinkClient(String uid);

	public abstract boolean isPlayerLinked(UUID uuid);

	public abstract boolean isClientLinked(String uid);

	public abstract Map<UUID, String> getAllLinks();

}
