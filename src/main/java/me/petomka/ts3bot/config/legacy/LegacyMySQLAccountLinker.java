package me.petomka.ts3bot.config.legacy;

import com.google.common.collect.Maps;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.mysql.MySQL;
import static me.petomka.ts3bot.mysql.MySQL.TABLE_PREFIX;
import me.petomka.ts3bot.mysql.SQLUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;


/**
 * Created by Benedikt on 07.08.2017.
 */
public class LegacyMySQLAccountLinker extends LegacyAccountLinker {

	public LegacyMySQLAccountLinker() {
		super(LinkerType.MYSQL);
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("create table if not exists " + TABLE_PREFIX + " (`uuid` varchar(50) character set utf8 collate utf8_bin not null, " +
					"`client_uid` varchar(100) character set utf8 collate utf8_bin not null, primary key(`uuid`), unique(`client_uid`))")) {
				stmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Could not connect to MySQL database!");
		}
	}

	@Override
	public Map<UUID, String> getAllLinks() {
		Map<UUID, String> allLinks = Maps.newHashMap();
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM " + TABLE_PREFIX)) {
				try (ResultSet resultSet = stmt.executeQuery()) {
					while (resultSet.next()) {
						UUID uuid = SQLUtils.getUUID(resultSet, "uuid");
						String clientUid = SQLUtils.getString(resultSet, "client_uid");
						allLinks.put(uuid, clientUid);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allLinks;
	}

	@Override
	public void wipeStorage() {
		//do nothing
	}

	@Override
	public UUID getPlayerUUIDbyClientUID(String uid) {
		try (Connection con = MySQL.getConnection()) {
			try (PreparedStatement stmt = con.prepareStatement(String.format("SELECT uuid FROM %s WHERE client_uid = ?", TABLE_PREFIX))) {
				SQLUtils.setString(stmt, 1, uid);
				try (ResultSet resultSet = stmt.executeQuery()) {
					return SQLUtils.getUUID(resultSet, "uuid");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String legacyGetClientUIDbyPlayerUUID(UUID uuid) {
		try (Connection con = MySQL.getConnection()) {
			try (PreparedStatement stmt = con.prepareStatement(String.format("SELECT client_uid FROM %s WHERE uuid = ?", TABLE_PREFIX))) {
				SQLUtils.setUUID(stmt, 1, uuid);
				try (ResultSet resultSet = stmt.executeQuery()) {
					return SQLUtils.getString(resultSet, "client_uid");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void linkPlayer(UUID uuid, String uid) {
		try (Connection con = MySQL.getConnection()) {
			try (PreparedStatement stmt = con.prepareStatement(String.format("INSERT INTO %s (`uuid`, `client_uid`) values (?, ?)", TABLE_PREFIX))) {
				SQLUtils.setUUID(stmt, 1, uuid);
				SQLUtils.setString(stmt, 2, uid);

				stmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void unlinkPlayer(UUID uuid) {
		try (Connection con = MySQL.getConnection()) {
			try (PreparedStatement stmt = con.prepareStatement(String.format("DELETE FROM %s WHERE UUID = ?", TABLE_PREFIX))) {
				SQLUtils.setUUID(stmt, 1, uuid);

				stmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void unlinkClient(String uid) {
		try (Connection con = MySQL.getConnection()) {
			try (PreparedStatement stmt = con.prepareStatement(String.format("DELETE FROM %s WHERE client_uid = ?", TABLE_PREFIX))) {
				SQLUtils.setString(stmt, 1, uid);

				stmt.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isPlayerLinked(UUID uuid) {
		try (Connection con = MySQL.getConnection()) {
			try (PreparedStatement stmt = con.prepareStatement(String.format("SELECT uuid FROM %s WHERE uuid = ?", TABLE_PREFIX))) {
				SQLUtils.setUUID(stmt, 1, uuid);
				try (ResultSet resultSet = stmt.executeQuery()) {
					return resultSet.next();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean isClientLinked(String uid) {
		try (Connection con = MySQL.getConnection()) {
			try (PreparedStatement stmt = con.prepareStatement(String.format("SELECT client_uid FROM %s WHERE client_uid = ?", TABLE_PREFIX))) {
				SQLUtils.setString(stmt, 1, uid);
				try (ResultSet resultSet = stmt.executeQuery()) {
					return resultSet.next();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
