package me.petomka.ts3bot.config.legacy;

import com.google.common.collect.Maps;
import me.petomka.ts3bot.main.MainManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class LegacyYamlAccountLinkerBukkit extends LegacyAccountLinker {

	private FileConfiguration links;
	private File linksFile;

	public LegacyYamlAccountLinkerBukkit() {
		super(LinkerType.YML);
		File file = new File(MainManager.main.getDataFolder(), "links.yml");
		if (!file.exists()) {
			try {
				file.createNewFile(); //ignore output
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		linksFile = file;
		links = YamlConfiguration.loadConfiguration(file);
	}

	private void saveLinksFile() {
		try {
			links.save(linksFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<UUID, String> getAllLinks() {
		Map<UUID, String> allLinks = Maps.newHashMap();
		links.getKeys(false).forEach(uuidKey -> {
			allLinks.put(UUID.fromString(uuidKey), links.getString(uuidKey));
		});
		return allLinks;
	}

	@Override
	public void wipeStorage() {
		links.getKeys(true).forEach(key -> links.set(key, null));
		saveLinksFile();
	}

	@Override
	public UUID getPlayerUUIDbyClientUID(String uid) {
		Map<String, Object> content = links.getValues(false);
		for (String key : content.keySet()) {
			if (content.get(key).toString().equalsIgnoreCase(uid)) {
				return UUID.fromString(key);
			}
		}
		return null;
	}

	@Override
	public String legacyGetClientUIDbyPlayerUUID(UUID uuid) {
		return links.getString(uuid.toString());
	}

	@Override
	public void linkPlayer(UUID uuid, String uid) {
		links.set(uuid.toString(), uid);
		saveLinksFile();
	}

	@Override
	public void unlinkPlayer(UUID uuid) {
		links.set(uuid.toString(), null);
		saveLinksFile();
	}

	@Override
	public void unlinkClient(String uid) {
		Map<String, Object> content = links.getValues(false);
		for (String key : content.keySet()) {
			if (content.get(key).toString().equalsIgnoreCase(uid)) {
				links.set(key, null);
				saveLinksFile();
			}
		}
	}

	@Override
	public boolean isPlayerLinked(UUID uuid) {
		return links.contains(uuid.toString());
	}

	@Override
	public boolean isClientLinked(String uid) {
		Map<String, Object> content = links.getValues(false);
		for (String key : content.keySet()) {
			if (content.get(key).toString().equalsIgnoreCase(uid)) {
				return true;
			}
		}
		return false;
	}
}
