package me.petomka.ts3bot.config.legacy;

import com.google.common.collect.Maps;
import com.google.common.io.ByteStreams;
import me.petomka.ts3bot.main.BungeeMain;
import me.petomka.ts3bot.main.MainManager;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class LegacyYamlAccountLinkerBungee extends LegacyAccountLinker {

	private Configuration links;
	private File linksFile;

	public LegacyYamlAccountLinkerBungee() {
		super(LinkerType.YML);
		File configFile = new File(MainManager.main.getDataFolder(), "links.yml");
		if (!configFile.exists()) {
			try {
				configFile.createNewFile();
				try (InputStream is = ((BungeeMain) MainManager.main).getResourceAsStream("me/petomka/ts3bot/bungeefiles/links.yml");
					 OutputStream os = new FileOutputStream(configFile)) {
					ByteStreams.copy(is, os);
				}
			} catch (IOException e) {
				throw new RuntimeException("Unable to create configuration file", e);
			}
		}
		try {
			links = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
			linksFile = configFile;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void saveLinksFile() {
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(links, linksFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<UUID, String> getAllLinks() {
		Map<UUID, String> allLinks = Maps.newHashMap();
		links.getKeys().forEach(uuidKey -> {
			allLinks.put(UUID.fromString(uuidKey), links.getString(uuidKey));
		});
		return allLinks;
	}

	@Override
	public void wipeStorage() {
		links.getKeys().forEach(key -> links.set(key, null));
		saveLinksFile();
	}

	@Override
	public UUID getPlayerUUIDbyClientUID(String uid) {
		Collection<String> content = links.getKeys();
		for (String key : content) {
			if (links.getString(key).equalsIgnoreCase(uid)) {
				return UUID.fromString(key);
			}
		}
		return null;
	}

	@Override
	public String legacyGetClientUIDbyPlayerUUID(UUID uuid) {
		return links.getString(uuid.toString());
	}

	@Override
	public void linkPlayer(UUID uuid, String uid) {
		links.set(uuid.toString(), uid);
		saveLinksFile();
	}

	@Override
	public void unlinkPlayer(UUID uuid) {
		links.set(uuid.toString(), null);
		saveLinksFile();
	}

	@Override
	public void unlinkClient(String uid) {
		Collection<String> content = links.getKeys();
		for (String key : content) {
			if (links.getString(key).equalsIgnoreCase(uid)) {
				links.set(key, null);
				saveLinksFile();
			}
		}
	}

	@Override
	public boolean isPlayerLinked(UUID uuid) {
		return links.contains(uuid.toString());
	}

	@Override
	public boolean isClientLinked(String uid) {
		Collection<String> content = links.getKeys();
		for (String key : content) {
			if (links.getString(key).equalsIgnoreCase(uid)) {
				return true;
			}
		}
		return false;
	}
}
