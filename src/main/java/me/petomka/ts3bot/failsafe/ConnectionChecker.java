package me.petomka.ts3bot.failsafe;

import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.modules.PermissionsModule;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class ConnectionChecker extends Thread {

	@Override
	public void run() {
		try {
			if (!PermissionsModule.canSeeServerInfo()) {
				MainManager.main.getPluginLogger().log(Level.WARNING, "Cannot check connection to server, invalid permission setup.");
				return;
			}
			while (true) {
				if (!MainManager.isSelectedTeamspeakServerRunning()) {
					MainManager.main.getPluginLogger().log(Level.SEVERE, "Teamspeak server appears to be offline!");
					try {
						MainManager.main.forceAttemptReconnectServer();
					} catch (Exception e) {
						MainManager.main.getPluginLogger().log(Level.SEVERE, "Could not reconnect to the teamspeak server!");
					}
				}
				Thread.sleep(TimeUnit.SECONDS.toMillis(5));
			}
		} catch (InterruptedException ignored) {
		}
	}
}
