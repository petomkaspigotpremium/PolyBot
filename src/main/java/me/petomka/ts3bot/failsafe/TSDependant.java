package me.petomka.ts3bot.failsafe;

public interface TSDependant {

	void onConnectionLoss();

	void onReconnect();

}
