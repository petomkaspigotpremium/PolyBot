package me.petomka.ts3bot.failsafe;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * This class is notified when the bot lost the connection to the teamspeak server,
 * and will then notify every other registered service.
 * <p>
 * Called in Main#isSelectedTeamspeakServerRunning
 */
public class TSDependencyManager {

	private static List<TSDependant> tsDependants = Lists.newArrayList();

	public static boolean registerTSDependency(TSDependant dependant) {
		if (!tsDependants.contains(dependant)) {
			tsDependants.add(dependant);
			return true;
		}
		return false;
	}

	public static boolean removeTSDependency(TSDependant dependant) {
		return tsDependants.remove(dependant);
	}

	public static void onServerConnectionLoss() {
		tsDependants.forEach(TSDependant::onConnectionLoss);
	}

	public static void onServerReconnected() {
		tsDependants.forEach(TSDependant::onReconnect);
	}

}
