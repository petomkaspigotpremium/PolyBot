package me.petomka.ts3bot.main;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.commands.bukkit.CommandLink;
import me.petomka.ts3bot.commands.bukkit.CommandLinkconvert;
import me.petomka.ts3bot.commands.bukkit.CommandManager;
import me.petomka.ts3bot.commands.bukkit.CommandTS3Bot;
import me.petomka.ts3bot.commands.bukkit.CommandTSInit;
import me.petomka.ts3bot.commands.bukkit.CommandTSReconnect;
import me.petomka.ts3bot.commands.bukkit.CommandTSStatus;
import me.petomka.ts3bot.commands.bukkit.CommandUnlink;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.util.clientverifier.ClientVerifier;
import me.petomka.ts3bot.util.clientverifier.ClientVerifierBukkit;
import me.petomka.ts3bot.util.Pair;
import me.petomka.ts3bot.util.commandsender.UtilCommandSender;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBukkit;
import me.petomka.ts3bot.util.player.UtilPlayer;
import me.petomka.ts3bot.util.player.UtilPlayerBukkit;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by Benedikt on 06.08.2017.
 */
public class BukkitMain extends JavaPlugin implements Main {

	private static FileConfiguration config;

	private static File configFile;

	@Getter
	private static BukkitMain instance;

	private boolean isEnabled = false;

	private TS3Query ts3Query;

	private TS3Api ts3Api;

	private ServerVersion serverVersion;

	@Getter
	private Thread mainThread;

	@Override
	public void onEnable() {
		if (isEnabled) {
			return;
		}
		mainThread = Thread.currentThread();

		MainManager.setMain(this, MainManager.MainType.BUKKIT);

		isEnabled = true;
		instance = this;
		serverVersion = ServerVersion.fromVersion(Bukkit.getBukkitVersion());

		finishStartup();
	}

	@Override
	public void onDisable() {
		HandlerList.unregisterAll(this);
		this.onDisable(getLogger());
	}

	@Override
	public MainManager.MainType getMainType() {
		return MainManager.MainType.BUKKIT;
	}

	@Override
	public String getString(String path) {
		return config.getString(path);
	}

	@Override
	public boolean getBoolean(String path) {
		return config.getBoolean(path);
	}

	@Override
	public int getInt(String path) {
		return config.getInt(path);
	}

	@Override
	public double getDouble(String path) {
		return config.getDouble(path);
	}

	@Override
	public List<Integer> getIntegerList(String path) {
		return config.getIntegerList(path);
	}

	@Override
	public List<String> getStringList(String path) {
		return config.getStringList(path);
	}

	@Override
	public Map<String, Object> mapConfigSection(String path) {
		Map<String, Object> result = Maps.newHashMap();
		ConfigurationSection section = config.getConfigurationSection(path);
		if (section != null) {
			for (String key : section.getKeys(false)) {
				if (config.get(path + "." + key) instanceof ConfigurationSection) {
					result.put(key, mapConfigSection(path + "." + key));
				} else {
					result.put(key, config.get(path + "." + key));
				}
			}
		}
		return result;
	}

	@Override
	public void configSet(String path, Object value) {
		config.set(path, value);
		try {
			config.save(configFile);
		} catch (IOException e) {
			getLogger().log(Level.SEVERE, "Error saving configuration!");
		}
	}

	@Override
	public ServerVersion getCBVersion() {
		return serverVersion;
	}

	@Override
	public String getVersion() {
		return getDescription().getVersion();
	}

	@Override
	public void setQueryAndAPI(Pair<TS3Query, TS3Api> pair) {
		this.ts3Query = pair.getLeft();
		this.ts3Api = pair.getRight();
	}

	@Override
	public void async(Runnable runnable) {
		Bukkit.getScheduler().runTaskAsynchronously(this, runnable);
	}

	@Override
	public boolean isPlayerOnline(UUID uuid) {
		return Bukkit.getPlayer(uuid) != null;
	}

	@Override
	public UtilPlayer getUtilPlayer(UUID uuid) {
		if (uuid == null) {
			return null;
		}
		Player player = Bukkit.getPlayer(uuid);
		if (player == null) {
			return null;
		}
		return new UtilPlayerBukkit(player);
	}

	@Override
	public UtilPlayer getUtilPlayer(Object player) {
		if (player instanceof Player) {
			return UtilPlayerBukkit.of((Player) player);
		}
		return null;
	}

	@Override
	public UtilPlayer getUtilPlayer(String name) {
		Player player = Bukkit.getServer().getPlayer(name);
		if (player == null) {
			return null;
		}
		return new UtilPlayerBukkit(player);
	}

	@Override
	public List<UtilPlayer> getPlayers() {
		return Lists.newArrayList(Bukkit.getOnlinePlayers().stream()
				.map(UtilPlayerBukkit::new)
				.collect(Collectors.toList()));
	}

	@Override
	public ClientVerifier createVerifier(@Nonnull UUID uuid) {
		return new ClientVerifierBukkit(Bukkit.getPlayer(uuid));
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		return CommandManager.onCommand(sender, command, label, args);
	}

	@Override
	public TS3Api getTS3Api() {
		return ts3Api;
	}

	@Override
	public TS3Query getTS3Query() {
		return ts3Query;
	}

	public void initConfig() {
		config = createDefaultYamlFromJar("bungeefiles/config.yml", "config.yml");
		configFile = new File(getDataFolder(), "config.yml");
	}

	private @Nonnull
	FileConfiguration createDefaultYamlFromJar(@Nonnull String filePathInJar, @Nonnull String filePathToCreate) {
		File file = new File(getDataFolder(), "/" + filePathToCreate);
		if (file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException exc) {
				getLogger().log(Level.SEVERE, "Error creating file: " + filePathInJar, exc);
			}
		}
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		FileConfiguration template = YamlConfiguration.loadConfiguration(
				new InputStreamReader(getClass().getResourceAsStream(
						"/me/petomka/ts3bot/" + filePathInJar)));
		config.addDefaults(template);
		config.options().copyDefaults(true);

		try {
			config.save(file);
		} catch (IOException exc) {
			getLogger().log(Level.SEVERE, "Error saving file: " + filePathInJar, exc);
		}

		return config;
	}

	public Logger getPluginLogger() {
		return super.getLogger();
	}

	public void initCommands() {
		new CommandLink();
		new CommandUnlink();
		new CommandTS3Bot();
		new CommandLinkconvert();
		new CommandTSReconnect();
		new CommandTSStatus();
		new CommandTSInit();
	}

	@Override
	public UtilCommandSender getConsoleCommandSender() {
		return new UtilCommandSenderBukkit(Bukkit.getConsoleSender());
	}

	@Override
	public void dispatchCommand(UtilCommandSender sender, String commandLine) {
		if (!(sender instanceof UtilCommandSenderBukkit)) {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Tried to dispatch command with incompatible commandsender!");
			return;
		}
		CommandSender commandSender = (CommandSender) sender.unwrap();
		Bukkit.dispatchCommand(commandSender, commandLine);
	}
}
