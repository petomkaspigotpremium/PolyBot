package me.petomka.ts3bot.main;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.ByteStreams;
import lombok.Getter;
import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.commands.bungee.*;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.config.MessageColorizer;
import me.petomka.ts3bot.util.clientverifier.ClientVerifier;
import me.petomka.ts3bot.util.clientverifier.ClientVerifierBungee;
import me.petomka.ts3bot.util.Pair;
import me.petomka.ts3bot.util.commandsender.UtilCommandSender;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBungee;
import me.petomka.ts3bot.util.player.UtilPlayer;
import me.petomka.ts3bot.util.player.UtilPlayerBungee;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class BungeeMain extends Plugin implements Main {

	private final ServerVersion serverVersion = ServerVersion.BUNGEE;
	private Configuration config;
	private TS3Query ts3Query;
	private TS3Api ts3Api;

	@Getter
	private Thread mainThread;

	@Override
	public void onEnable() {
		mainThread = Thread.currentThread();

		MainManager.setMain(this, MainManager.MainType.BUNGEE);
		MessageColorizer.initMessageColorizerBungee();

		finishStartup();
	}


	@Override
	public void onDisable() {
		this.getProxy().getPluginManager().unregisterListeners(this);
		this.onDisable(getLogger());
	}

	@Override
	public MainManager.MainType getMainType() {
		return MainManager.MainType.BUNGEE;
	}

	public String getString(String path) {
		return config.getString(path);
	}

	@Override
	public boolean getBoolean(String path) {
		return config.getBoolean(path);
	}

	@Override
	public int getInt(String path) {
		return config.getInt(path);
	}

	@Override
	public double getDouble(String path) {
		return config.getDouble(path);
	}

	@Override
	public List<Integer> getIntegerList(String path) {
		return config.getIntList(path);
	}

	@Override
	public List<String> getStringList(String path) {
		return config.getStringList(path);
	}

	@Override
	public Map<String, Object> mapConfigSection(String path) {
		Map<String, Object> result = Maps.newHashMap();
		Configuration section = config.getSection(path);
		for (String key : section.getKeys()) {
			if (config.get(path + "." + key) instanceof Configuration) {
				result.put(key, mapConfigSection(path + "." + key));
			} else {
				result.put(key, config.get(path + "." + key));
			}
		}
		return result;
	}

	@Override
	public void configSet(String path, Object value) {
		config.set(path, value);
		saveConfig();
	}

	@Override
	public ServerVersion getCBVersion() {
		return serverVersion;
	}

	@Override
	public String getVersion() {
		return super.getDescription().getVersion();
	}

	@Override
	public void setQueryAndAPI(Pair<TS3Query, TS3Api> pair) {
		this.ts3Query = pair.getLeft();
		this.ts3Api = pair.getRight();
	}

	@Override
	public void async(Runnable runnable) {
		ProxyServer.getInstance().getScheduler().runAsync(this, runnable);
	}

	@Override
	public boolean isPlayerOnline(UUID uuid) {
		return ProxyServer.getInstance().getPlayer(uuid) != null;
	}

	@Override
	public UtilPlayer getUtilPlayer(UUID uuid) {
		if (uuid == null) {
			return null;
		}
		ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
		if (player == null) {
			return null;
		}
		return new UtilPlayerBungee(player);
	}

	@Override
	public UtilPlayer getUtilPlayer(Object player) {
		if (player instanceof ProxiedPlayer) {
			return UtilPlayerBungee.of((ProxiedPlayer) player);
		}
		return null;
	}

	@Override
	public UtilPlayer getUtilPlayer(String name) {
		ProxiedPlayer player = ProxyServer.getInstance().getPlayer(name);
		if (player == null) {
			return null;
		}
		return new UtilPlayerBungee(player);
	}

	@Override
	public List<UtilPlayer> getPlayers() {
		return Lists.newArrayList(ProxyServer.getInstance().getPlayers().stream()
				.map(UtilPlayerBungee::new)
				.collect(Collectors.toList()));
	}

	@Override
	public ClientVerifier createVerifier(UUID uuid) {
		return new ClientVerifierBungee(getProxy().getPlayer(uuid));
	}

	@Override
	public TS3Api getTS3Api() {
		return ts3Api;
	}

	@Override
	public TS3Query getTS3Query() {
		return ts3Query;
	}

	private File configFile;

	private void saveConfig() {
		if(configFile == null) {
			return;
		}
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, configFile);
		} catch (IOException e) {
			getLogger().log(Level.SEVERE, "Error saving config", e);
		}
	}

	public void initConfig() {
		File dataFolder = getDataFolder();
		if (!dataFolder.exists()) {
			dataFolder.mkdir();
		}
		configFile = new File(dataFolder, "config.yml");
		if (!configFile.exists()) {
			try {
				configFile.createNewFile();
				try (InputStream is = getResourceAsStream("me/petomka/ts3bot/bungeefiles/config.yml");
					 OutputStream os = new FileOutputStream(configFile)) {
					ByteStreams.copy(is, os);
				}
			} catch (IOException e) {
				throw new RuntimeException("Unable to create configuration file", e);
			}
		}
		try {
			config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
			Configuration temp = ConfigurationProvider.getProvider(YamlConfiguration.class)
					.load(getResourceAsStream("me/petomka/ts3bot/bungeefiles/config.yml"));
			boolean changed = false;
			ArrayList<ConfigEntry> configEntries = scanConfig(temp, new ArrayList<>(), "");
			for (ConfigEntry entry : configEntries) {
				if (!config.contains(entry.path)) {
					changed = true;
					config.set(entry.path, entry.value);
				}
			}
			if (changed) {
				saveConfig();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private ArrayList<ConfigEntry> scanConfig(Configuration config, ArrayList<ConfigEntry> configEntries, String path) {
		for (String k : config.getKeys()) {
			if (config.get(k) instanceof Configuration) {
				configEntries = scanConfig(config.getSection(k), configEntries, path + k + ".");
			} else {
				configEntries.add(new ConfigEntry(config.get(k), path + k));
			}
		}
		return configEntries;
	}

	public Logger getPluginLogger() {
		return super.getLogger();
	}

	public void initCommands() {
		new CommandLink();
		new CommandUnlink();
		new CommandTS3Bot();
		new CommandLinkconvert();
		new CommandTSReconnect();
		new CommandTSStatus();
		new CommandTSInit();
	}

	@Override
	public UtilCommandSender getConsoleCommandSender() {
		return new UtilCommandSenderBungee(ProxyServer.getInstance().getConsole());
	}

	@Override
	public void dispatchCommand(UtilCommandSender sender, String commandLine) {
		if (!(sender instanceof UtilCommandSenderBungee)) {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Tried to dispatch command with incompatible commandsender!");
			return;
		}
		CommandSender commandSender = (CommandSender) sender.unwrap();
		ProxyServer.getInstance().getPluginManager().dispatchCommand(commandSender, commandLine);
	}

	private class ConfigEntry {
		Object value;
		String path;

		public ConfigEntry(Object value, String path) {
			this.value = value;
			this.path = path;
		}
	}
}
