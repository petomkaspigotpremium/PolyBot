package me.petomka.ts3bot.main;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.reconnect.ConnectionHandler;
import com.github.theholywaffle.teamspeak3.api.reconnect.ReconnectStrategy;
import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.modules.ConnectionModule;
import me.petomka.ts3bot.modules.StatusModule;
import me.petomka.ts3bot.util.Pair;
import me.petomka.ts3bot.util.clientverifier.ClientVerifier;
import me.petomka.ts3bot.util.commandsender.UtilCommandSender;
import me.petomka.ts3bot.util.player.UtilPlayer;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Benedikt on 07.08.2017.
 */
public interface Main {

	MainManager.MainType getMainType();

	String getString(String path);

	boolean getBoolean(String path);

	int getInt(String path);

	double getDouble(String path);

	List<Integer> getIntegerList(String path);

	List<String> getStringList(String path);

	Map<String, Object> mapConfigSection(String path);

	void configSet(String path, Object value);

	ServerVersion getCBVersion();

	String getVersion();

	Thread getMainThread();

	default void finishStartup() {
		UniMethods.copyLegacyFolder(getDataFolder(), getLogger());

		initConfig();
		initCommands();

		MainManager.startupTSInit();

		if (ConfigManager.isLegacyLinking()) {
			MainManager.main.getPluginLogger().info("Upgrading your links...");
			UniMethods.upgradeLinksFromLegacy();
		}

		getLogger().info("TS3Bot by petomka enabled!");
	}

	Logger getLogger();

	void initConfig();

	void initCommands();

	default Pair<TS3Query, TS3Api> connectToTS3(TS3Config ts3Config) {
		ts3Config.setHost(getString("bot.host"));
		ts3Config.setQueryPort(getInt("bot.port"));

		if (ConfigManager.getBoolean("unlimited-floodrate")) {
			ts3Config.setFloodRate(TS3Query.FloodRate.UNLIMITED);
		}
		ts3Config.setConnectionHandler(new ConnectionHandler() {
			@Override
			public void onConnect(TS3Api ts3Api) {
				ConnectionModule.setConnectionStatus(StatusModule.Status.OK);
				onTSJoin(ts3Api);

			}

			@Override
			public void onDisconnect(TS3Query ts3Query) {
				ConnectionModule.setConnectionStatus(StatusModule.Status.ERROR);
			}
		});

		//ts3Config.setDebugLevel(Level.OFF);
		ts3Config.setReconnectStrategy(ReconnectStrategy.constantBackoff());

		TS3Query ts3Query = new TS3Query(ts3Config);
		ts3Query.connect();

		onTSJoin(ts3Query.getApi());

		TS3Api ts3Api = ts3Query.getApi();

		return Pair.of(ts3Query, ts3Api);
	}

	default void forceAttemptReconnectQuery() {
		if (getTS3Query().isConnected()) {
			return;
		}
		getTS3Query().connect();
	}

	default boolean forceAttemptReconnectServer() {
		if (MainManager.isSelectedTeamspeakServerRunning()) {
			return true;
		}
		try {
			getTS3Api().selectVirtualServerById(getInt("bot.virtual-server"));
			onTSJoin(getTS3Api());
			return true;
		} catch (Exception exc) {
			getPluginLogger().log(Level.SEVERE, "Could not reconnect to teamspeak server, is it offline?");
			return false;
		}
	}

	default void onTSJoin(TS3Api ts3Api) {
		ts3Api.login(getString("bot.login-name"), getString("bot.login-password"));
		ts3Api.selectVirtualServerById(getInt("bot.virtual-server"));
		try {
			ts3Api.setNickname(getString("bot.nickname"));
		} catch (Exception ignored) {
			MainManager.main.getPluginLogger().log(Level.WARNING, "The bot's nickname was already taken.");
		}
		int joinChannelId = getInt("bot.join-channel-id");
		MainManager.setWhoAmI(ts3Api.whoAmI());
		if (joinChannelId != -1) {
			ts3Api.moveClient(MainManager.getWhoAmI().getId(), joinChannelId);
		}
	}

	default void onDisable(Logger logger) {
		MainManager.getConnectionChecker().interrupt();
		if (getTS3Api() != null) {
			getTS3Api().logout();
		}
		if (getTS3Query() != null) {
			getTS3Query().exit();
		}
		logger.info("TS3Bot by petomka disabled");
	}

	File getDataFolder();

	void setQueryAndAPI(Pair<TS3Query, TS3Api> pair);

	void async(Runnable runnable);

	boolean isPlayerOnline(UUID uuid);

	UtilPlayer getUtilPlayer(UUID uuid);

	UtilPlayer getUtilPlayer(String name);

	UtilPlayer getUtilPlayer(Object player);

	UtilCommandSender getConsoleCommandSender();

	void dispatchCommand(UtilCommandSender sender, String commandLine);

	List<UtilPlayer> getPlayers();

	ClientVerifier createVerifier(UUID uuid);

	TS3Api getTS3Api();

	TS3Query getTS3Query();

	Logger getPluginLogger();


}
