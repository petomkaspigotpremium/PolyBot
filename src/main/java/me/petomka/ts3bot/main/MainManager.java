package me.petomka.ts3bot.main;

import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerQueryInfo;
import lombok.Getter;
import lombok.Setter;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.failsafe.ConnectionChecker;
import me.petomka.ts3bot.failsafe.TSDependencyManager;
import me.petomka.ts3bot.modules.ConnectionModule;
import me.petomka.ts3bot.modules.FeaturesModule;
import me.petomka.ts3bot.modules.LinkModule;
import me.petomka.ts3bot.modules.PermissionsModule;
import me.petomka.ts3bot.modules.StatusModule;
import me.petomka.ts3bot.modules.UsersModule;
import me.petomka.ts3bot.tasks.afkmover.AFKDetector;
import me.petomka.ts3bot.tasks.servergroupupdater.ServergroupUpdater;
import me.petomka.ts3bot.ts3listeners.JoinUpdateListener;
import me.petomka.ts3bot.ts3listeners.TextMessageListener;
import me.petomka.ts3bot.ts3listeners.proxychecker.JoinProxyCheckListener;
import me.petomka.ts3bot.ts3listeners.supportchannel.SupportChannelListener;
import me.petomka.ts3bot.util.Pair;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class MainManager {

	public static Main main;

	public static MainType mainType;

	@Getter
	@Setter
	private static ServerQueryInfo whoAmI;

	@Getter
	private static boolean isInitialized = false;

	@Getter
	private static ConnectionChecker connectionChecker = new ConnectionChecker();

	private static Pair<Long, Boolean> cachedStatusResult = Pair.of(0L, Boolean.FALSE);

	public static boolean isSelectedTeamspeakServerRunning() {
		if (!MainManager.main.getTS3Query().isConnected()) {
			return false;
		}
		if (cachedStatusResult.getKey() + 1000L > System.currentTimeMillis()) {
			return cachedStatusResult.getValue();
		}
		boolean result;
		try {
			if (ConnectionModule.getConnectionStatus() == StatusModule.Status.ERROR) {
				TSDependencyManager.onServerReconnected();
			}
			main.getTS3Api().getServerInfo();
			ConnectionModule.setConnectionStatus(StatusModule.Status.OK);
			result = true;
		} catch (Exception exc) {
			if (ConnectionModule.getConnectionStatus() == StatusModule.Status.OK) {
				TSDependencyManager.onServerConnectionLoss();
			}
			ConnectionModule.setConnectionStatus(StatusModule.Status.ERROR);
			result = false;
		}
		cachedStatusResult = Pair.of(System.currentTimeMillis(), result);
		return result;
	}

	public static void setMain(Main main, MainType type) {
		MainManager.main = main;
		MainManager.mainType = type;
	}

	public static void startupTSInit() {
		ConnectionModule.init(); //this one is safe
		try {
			tsInit();
		} catch (Exception exc) {
			ConnectionModule.setConnectionStatus(StatusModule.Status.ERROR);
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Could not connect to teamspeak. Once your teamspeak " +
					"server is running, use the command \"tsinit\" to initialize the connection again.");
		}
	}

	public static void tsInit() {
		main.setQueryAndAPI(
				main.connectToTS3(new TS3Config())
		);

		try {
			LinkerHandler.initAccountLinker();
		} catch (Exception e) {
			main.getPluginLogger().log(Level.WARNING, "Account linker could not be initialized: " + e.getMessage());
		}
		//fixTS3Api(main.getTS3Query());
		try {
			main.getTS3Api().registerAllEvents();
		} catch (Exception e) {
			main.getPluginLogger().log(Level.SEVERE, "Events could not be registered. Bot may not function properly. Cause: " + e.getMessage(),
					e);
		}
		try {
			TextMessageListener.registerTextMessageListener();
		} catch (Exception e) {
			main.getPluginLogger().log(Level.WARNING, "TextMessage listener could not be initialized: " + e.getMessage());
		}
		if (ConfigManager.getBoolean("supportchannel.enabled")) {
			try {
				SupportChannelListener.initSupportChannelListener();
			} catch (Exception e) {
				main.getPluginLogger().log(Level.WARNING, "Supportchannel feature could not be initialized: " + e.getMessage());
			}
		}
		if (ConfigManager.getBoolean("servergroup-updater.enabled")) {
			try {
				ServergroupUpdater.initServergroupUpdater();
			} catch (Exception e) {
				main.getPluginLogger().log(Level.WARNING, "Servergroup Updater feature could not be initialized: " + e.getMessage());
			}
		}
		if (ConfigManager.getBoolean("afkchannel.enabled")) {
			try {
				AFKDetector.init();
			} catch (Exception e) {
				main.getPluginLogger().log(Level.WARNING, "AFKChannel could not be initialized: " + e.getMessage());
			}
		}
		if (ConfigManager.getBoolean("proxycheck.enabled")) {
			try {
				JoinProxyCheckListener.initProxyCheckListener();
			} catch (Exception e) {
				main.getPluginLogger().log(Level.WARNING, "ProxyCheck and Geofence could not be initialized: " + e.getMessage());
			}
		}

		try {
			JoinUpdateListener.initJoinUpdateListener();
		} catch (Exception e) {
			main.getPluginLogger().log(Level.WARNING, "Join listener could not be initialized: " + e.getMessage());
		}

		try {
			PermissionsModule.init();
		} catch (Exception e) {
			main.getPluginLogger().log(Level.WARNING, "Permissions module could not be initialized: " + e.getMessage());
		}
		try {
			LinkModule.init();
		} catch (Exception e) {
			main.getPluginLogger().log(Level.WARNING, "Link module could not be initialized: " + e.getMessage());
		}
		try {
			FeaturesModule.init();
		} catch (Exception e) {
			main.getPluginLogger().log(Level.WARNING, "Features module could not be initialized: " + e.getMessage());
		}
		try {
			UsersModule.init();
		} catch (Exception e) {
			main.getPluginLogger().log(Level.WARNING, "Users module could not be initialized: " + e.getMessage());
		}

		//Connection checker falls die tolle API einen Disconnect nicht mitbekommt, z.B. wenn man den Server einfach ausmacht
		try {
			connectionChecker.start();
		} catch (Exception e) {
			main.getPluginLogger().log(Level.SEVERE, "Connection Checker could not be started: " + e.getMessage());
		}

		isInitialized = true;
	}

	public static ClientInfo getSingleClientInfoFromMultiple(Collection<String> clientUids, boolean nullIfMore) {
		Set<ClientInfo> clientInfos = clientUids.stream()
				.map(main.getTS3Api()::getClientByUId)
				.collect(Collectors.toSet());
		if (clientInfos.isEmpty()) {
			return null;
		}
		if (clientInfos.size() > 1 && nullIfMore) {
			return null;
		}
		return clientInfos.stream().findAny().get();
	}

	public static Set<ClientInfo> getClientInfosFromMultiple(Collection<String> clientUids) {
		return clientUids.stream()
				.map(main.getTS3Api()::getClientByUId)
				.collect(Collectors.toSet());
	}

	public static @Nullable
	<T> T getAny(Collection<T> collection) {
		return collection.stream().findAny().orElse(null);
	}

	public static Set<String> getOnlineLinkedUids(UUID uniqueId) {
		return LinkerHandler.linker.getClientUIDbyPlayerUUID(uniqueId)
				.stream()
				.filter(MainManager.main.getTS3Api()::isClientOnline)
				.collect(Collectors.toSet());
	}

	public enum MainType {
		BUKKIT,
		BUNGEE
	}

}
