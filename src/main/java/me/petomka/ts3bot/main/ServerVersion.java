package me.petomka.ts3bot.main;

import lombok.AllArgsConstructor;

import javax.annotation.Nullable;
import java.util.Arrays;


@AllArgsConstructor
public enum ServerVersion {

	BUNGEE(null),
	UNKNOWN(null),
	V1_13("1.13"),
	V1_12("1.12"),
	V1_11("1.11"),
	V1_10("1.10"),
	V1_9("1.9"),
	V1_8("1.8");

	@Nullable
	private String versionString;

	public static ServerVersion fromVersion(String versionString) {
		return Arrays.stream(ServerVersion.values())
				.filter(serverVersion -> serverVersion.versionString != null)
				.filter(serverVersion -> versionString.contains(serverVersion.versionString))
				.findFirst().orElse(ServerVersion.UNKNOWN);
	}

}
