package me.petomka.ts3bot.modules;

import lombok.Getter;
import lombok.Setter;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.UtilChatColor;

public class ConnectionModule implements StatusModule {

	@Getter
	@Setter
	/*
	 * This field is used a lot for external calculations also. Modifying this value might
	 * lead to some odd behaviour.
	 */
	private static Status connectionStatus = Status.ERROR;

	public static void init() {
		ModuleHandler.getInstance().addStatusModule(new ConnectionModule());
	}

	@Override
	public boolean requiresTeamspeakConnection() {
		return false;
	}

	@Override
	public boolean usesDatabaseConnection() {
		return false;
	}

	@Override
	public String getName() {
		return "Connection";
	}

	@Override
	public Status getStatus() {
		return connectionStatus;
	}

	@Override
	public Menu getDetailedInfo() {
		Menu menu = new Menu(UtilChatColor.GOLD + "Connection Status:");
		switch (connectionStatus) {
			case OK:
				menu.addSub(new Menu(UtilChatColor.GREEN + "Connection to TS3 was established successfully."));
				break;
			case ERROR:
				if (!MainManager.isInitialized()) {
					menu.addSub(new Menu(UtilChatColor.RED + "Teamspeak was not online when the plugin started! Initialize with /tsinit"));
				}
				menu.addSub(new Menu(UtilChatColor.RED + "Could not connect to TS3. Please reconfigure the connection settings."));
				break;
		}
		return menu;
	}

}
