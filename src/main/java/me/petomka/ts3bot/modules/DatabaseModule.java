package me.petomka.ts3bot.modules;

import lombok.Getter;
import lombok.Setter;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.UtilChatColor;

public class DatabaseModule implements StatusModule {

	@Getter
	@Setter
	private static Status databaseStatus = Status.ERROR;

	public static void init() {
		ModuleHandler.getInstance().addStatusModule(new DatabaseModule());
	}

	@Override
	public boolean requiresTeamspeakConnection() {
		return false;
	}

	@Override
	public boolean usesDatabaseConnection() {
		return false;
	}

	@Override
	public String getName() {
		return "Database";
	}

	@Override
	public Status getStatus() {
		return databaseStatus;
	}

	@Override
	public Menu getDetailedInfo() {
		Menu menu = new Menu(UtilChatColor.GOLD + "Connection Status:");
		switch (databaseStatus) {
			case OK:
				menu.addSub(new Menu(UtilChatColor.GREEN + "Connection to Database was established successfully."));
				break;
			case ERROR:
				menu.addSub(new Menu(UtilChatColor.RED + "Could not connect to Database. Please reconfigure the connection settings."));
				break;
		}
		return menu;
	}
}
