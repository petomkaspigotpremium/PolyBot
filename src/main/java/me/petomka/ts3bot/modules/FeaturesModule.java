package me.petomka.ts3bot.modules;

import com.google.common.collect.Lists;
import lombok.Getter;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.UtilChatColor;
import net.md_5.bungee.api.chat.ClickEvent;

import javax.annotation.Nullable;
import java.util.List;

public class FeaturesModule implements StatusModule {

	@Getter
	private static List<StatusModule> registeredFeatures = Lists.newArrayList();

	private static Status featuresStatus;

	public static void registerFeature(StatusModule feature) {
		if (!registeredFeatures.contains(feature)) {
			registeredFeatures.add(feature);
		}
	}

	@Nullable
	public static StatusModule getFeature(String name) {
		return registeredFeatures.stream().filter(module ->
				module.getName().equalsIgnoreCase(name))
				.findFirst().orElse(null);
	}

	public static void init() {
		ModuleHandler.getInstance().addStatusModule(new FeaturesModule());
	}

	private static void recalcStatus() {
		featuresStatus = Status.OK;
		for (StatusModule module : registeredFeatures) {
			if (module.getStatus() == Status.WARN) {
				featuresStatus = Status.WARN;
			}
			if (module.getStatus() == Status.ERROR) {
				featuresStatus = Status.ERROR;
				break;
			}
		}
	}

	@Override
	public boolean requiresTeamspeakConnection() {
		return true;
	}

	@Override
	public boolean usesDatabaseConnection() {
		return false;
	}

	@Override
	public String getName() {
		return "Features";
	}

	@Override
	public Status getStatus() {
		recalcStatus();
		return featuresStatus;
	}

	@Override
	public Menu getDetailedInfo() {
		Menu menu = new Menu(UtilChatColor.GOLD + "Active Features:");
		registeredFeatures.forEach(module -> {
			menu.addSub(
					new Menu(UtilChatColor.BLUE + module.getName() + ": " + module.getStatus(),
							ClickEvent.Action.RUN_COMMAND,
							"/tsstatus " + this.getName() + " " + module.getName())
			);
		});
		return menu;
	}
}
