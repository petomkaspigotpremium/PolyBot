package me.petomka.ts3bot.modules;

import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroup;
import lombok.Getter;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.ChatUtils;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.UtilChatColor;

import java.util.List;

public class LinkModule implements StatusModule {

	private static Status linkStatus = Status.OK;

	@Getter
	private static LinkModule instance;

	public static void init() {
		instance = new LinkModule();
		instance.getStatus();
		ModuleHandler.getInstance().addStatusModule(instance);
	}

	@Override
	public boolean requiresTeamspeakConnection() {
		return true;
	}

	@Override
	public boolean usesDatabaseConnection() {
		return false;
	}

	@Override
	public String getName() {
		return "Linking";
	}

	@Override
	public Status getStatus() {
		getDetailedInfo();
		return linkStatus;
	}

	@Override
	public Menu getDetailedInfo() {
		Menu menu = new Menu(UtilChatColor.GOLD + getName() + " Status:");

		int assign_group_id = ConfigManager.getInt("link.servergroup-id");
		ServerGroup serverGroup = MainManager.main.getTS3Api().getServerGroups().stream()
				.filter(serverGroup1 -> serverGroup1.getId() == assign_group_id)
				.findAny().orElse(null);

		if(LinkerHandler.linker == null) {
			linkStatus = Status.ERROR;
			menu.addSub(new Menu(UtilChatColor.RED + "Account linker was not initialized. Check configuration and console log!"));
			return menu;
		}

		String start = UtilChatColor.BLUE + "Link Group: ";
		if (serverGroup != null) {
			menu.addSub(
					new Menu(start + UtilChatColor.WHITE + serverGroup.getName() + " (" +
							assign_group_id + ")")
			);
			linkStatus = Status.OK;
		} else {
			menu.addSub(
					new Menu(start + UtilChatColor.RED + "invalid")
			);
			linkStatus = Status.ERROR;
		}

		boolean setIcon = ConfigManager.getBoolean("link.set-commandsender-head-icon");
		menu.addSub(
				new Menu(UtilChatColor.BLUE + "Set Head Icon: " + ChatUtils.formatBoolean(setIcon))
		);

		boolean setDescription = ConfigManager.getBoolean("link.description.set");
		menu.addSub(
				new Menu(UtilChatColor.BLUE + "Set Description: " +
						(setDescription ? UtilChatColor.GREEN : UtilChatColor.RED) + setDescription)
		);

		Menu linkDispatchedCommands = new Menu(UtilChatColor.BLUE + "Dispatched commands on link:");
		List<String> linkDispatchedCommandsList = ConfigManager.getStringList("link.dispatched-commands");
		if (linkDispatchedCommandsList.isEmpty()) {
			linkDispatchedCommandsList.add(UtilChatColor.RED + "No commands are dispatched upon link.");
		}
		linkDispatchedCommandsList.forEach(cmd -> {
			linkDispatchedCommands.addSub(new Menu(cmd));
		});
		menu.addSub(linkDispatchedCommands);

		Menu unlinkMenu = new Menu(UtilChatColor.BLUE + "Unlinking:");
		boolean removeLinkGroup = ConfigManager.getBoolean("unlink.revoke-link-group");
		unlinkMenu.addSub(
				new Menu(UtilChatColor.DARK_AQUA + "Remove Link Group: " + ChatUtils.formatBoolean(removeLinkGroup))
		);
		boolean removeDescription = ConfigManager.getBoolean("unlink.remove-description");
		unlinkMenu.addSub(
				new Menu(UtilChatColor.DARK_AQUA + "Remove Description: " + ChatUtils.formatBoolean(removeDescription))
		);
		boolean removeIcon = ConfigManager.getBoolean("unlink.remove-client-icon");
		unlinkMenu.addSub(
				new Menu(UtilChatColor.DARK_AQUA + "Remove Icon: " + ChatUtils.formatBoolean(removeIcon))
		);
		boolean removeSyncedGroups = ConfigManager.getBoolean("unlink.remove-synced-groups");
		unlinkMenu.addSub(
				new Menu(UtilChatColor.DARK_AQUA + "Remove Synced Groups: " + ChatUtils.formatBoolean(removeSyncedGroups))
		);

		Menu unlinkDispatchedCommands = new Menu(UtilChatColor.BLUE + "Dispatched commands on unlink:");
		List<String> unlinkDispatchedCommandsList = ConfigManager.getStringList("unlink.dispatched-commands");
		if (unlinkDispatchedCommandsList.isEmpty()) {
			unlinkDispatchedCommandsList.add(UtilChatColor.RED + "No commands are dispatched upon unlink.");
		}
		unlinkDispatchedCommandsList.forEach(cmd -> {
			unlinkDispatchedCommands.addSub(new Menu(cmd));
		});
		unlinkMenu.addSub(unlinkDispatchedCommands);

		menu.addSub(unlinkMenu);

		return menu;
	}
}
