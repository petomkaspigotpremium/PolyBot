package me.petomka.ts3bot.modules;

import com.google.common.collect.Lists;
import lombok.Getter;

import javax.annotation.Nullable;
import java.util.List;

public class ModuleHandler {

	@Getter
	private static ModuleHandler instance = new ModuleHandler();

	@Getter
	private List<StatusModule> statusModules = Lists.newArrayList();

	public void addStatusModule(StatusModule module) {
		if (!statusModules.contains(module)) {
			statusModules.add(module);
		}
	}

	@Nullable
	public StatusModule byName(String name) {
		return statusModules.stream()
				.filter(s -> s.getName().equalsIgnoreCase(name))
				.findFirst().orElse(null);
	}

}
