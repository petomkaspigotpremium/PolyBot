package me.petomka.ts3bot.modules;

import com.github.theholywaffle.teamspeak3.api.wrapper.PermissionAssignment;
import com.google.common.collect.Maps;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.UtilChatColor;
import net.md_5.bungee.api.chat.ClickEvent;

import java.util.Collections;
import java.util.Map;

public class PermissionsModule implements StatusModule {

	private static Map<Integer, String> permissionIdNames = Maps.newHashMap();

	private static Map<String, PermissionAssignment> effectivePermissions = Maps.newHashMap();

	private static Map<String, Integer> neededPermissions = mapOf(
			"b_client_permissionoverview_own", 1,
			"b_serverinstance_permission_list", 1,
			"b_serverinstance_permission_find", 1,
			"b_virtualserver_permission_find", 1,
			"b_virtualserver_notify_register", 1,
			"b_virtualserver_notify_unregister", 1,
			"b_client_create_modify_serverquery_login", 1,
			"b_virtualserver_client_list", 1,
			"b_virtualserver_servergroup_list", 1,
			"b_virtualserver_channel_list", 1,
			"b_virtualserver_join_ignore_password", 1,
			"b_client_info_view", 1,
			"i_group_member_add_power", 75,
			"b_virtualserver_client_dbsearch", 1,
			"b_virtualserver_client_dbinfo", 1,
			"b_channel_info_view", 1,
			"b_client_modify_dbproperties", 1,
			"b_client_remoteaddress_view", 1,
			"b_client_skip_channelgroup_permissions", 1,
			"b_channel_join_permanent", 1,
			"b_channel_join_semi_permanent", 1,
			"b_channel_modify_maxclients", 1,
			"b_channel_modify_password", 1,
			"b_channel_join_ignore_password", 1,
			"i_client_permission_modify_power", 75,
			"i_channel_permission_modify_power", 75,
			"i_permission_modify_power", 75,
			"b_client_ignore_antiflood", 1,
			"b_virtualserver_modify_antiflood", 1,
			"b_icon_manage", 1,
			"i_client_poke_power", 75,
			"i_client_private_textmessage_power", 75,
			"i_client_move_power", 75,
			"i_group_member_remove_power", 75,
			"b_client_modify_description", 1,
			"i_channel_subscribe_power", 75,
			"i_client_kick_from_server_power", 75,
			"i_needed_modify_power_icon_id", 75, //GRANT-PERMISSION
			"b_virtualserver_info_view", 1,
			"i_max_icon_filesize", 8192
	);
	private static Status permissionStatus = Status.ERROR;

	public static boolean canSeeServerInfo() {
		return MainManager.main.getTS3Api().getPermissionValue("b_virtualserver_info_view") == 1;
	}

	public static boolean canUploadIcon() {
		return MainManager.main.getTS3Api().getPermissionValue("i_max_icon_filesize") >= 8192;
	}

	private static <K, V> Map<K, V> mapOf(Object... args) {
		if (args.length % 2 != 0) {
			return Collections.emptyMap();
		}
		Map<K, V> map = Maps.newLinkedHashMap();
		for (int index = 0; index < args.length; index += 2) {
			map.put((K) args[index], (V) args[index + 1]);
		}
		return map;
	}

	public static void init() {
		MainManager.main.getTS3Api().getPermissions().forEach(permissionInfo -> {
			permissionIdNames.put(permissionInfo.getId(), permissionInfo.getName());
		});
		recalcPermissions();
		ModuleHandler.getInstance().addStatusModule(new PermissionsModule());
	}

	public static void recalcPermissions() {
		MainManager.main.getTS3Api().getPermissionOverview(
				MainManager.getWhoAmI().getChannelId(),
				MainManager.getWhoAmI().getDatabaseId()
		).stream()
				.filter(permissionAssignment -> neededPermissions.containsKey(
						permissionIdNames.get(permissionAssignment.getId())
				))
				.forEach(permissionAssignment -> effectivePermissions.put(
						permissionIdNames.get(permissionAssignment.getId()),
						permissionAssignment
				));
		permissionStatus = Status.OK;
		for (String neededPerm : neededPermissions.keySet()) {
			if (effectivePermissions.get(neededPerm) == null || effectivePermissions.get(neededPerm).getValue() == 0) {
				permissionStatus = Status.ERROR;
				break;
			}
			if (effectivePermissions.get(neededPerm).getValue() < neededPermissions.get(neededPerm)) {
				permissionStatus = Status.WARN;
			}
		}
	}

	@Override
	public boolean requiresTeamspeakConnection() {
		return true;
	}

	@Override
	public boolean usesDatabaseConnection() {
		return false;
	}

	@Override
	public String getName() {
		return "Permissions";
	}

	@Override
	public Status getStatus() {
		recalcPermissions();
		return permissionStatus;
	}

	@Override
	public Menu getDetailedInfo() {
		recalcPermissions();
		Menu menu = new Menu(UtilChatColor.GOLD + "Permissions Status:");
		neededPermissions.forEach((s, integer) -> {
			Menu toAdd;
			String grant = "i_needed_modify_power_";
			String permName;
			if (s.startsWith(grant)) {
				permName = s.substring(grant.length()) + " (GRANT)";
			} else {
				permName = s;
			}
			if (effectivePermissions.get(s) == null || effectivePermissions.get(s).getValue() == 0) {
				toAdd = new Menu(UtilChatColor.BLUE + permName + ": " + UtilChatColor.RED + "0");
			} else if (effectivePermissions.get(s).getValue() < integer) {
				toAdd = new Menu(UtilChatColor.BLUE + permName + ": " + UtilChatColor.YELLOW +
						effectivePermissions.get(s).getValue() + " < " + integer);
			} else {
				toAdd = new Menu(UtilChatColor.BLUE + permName + ": " + UtilChatColor.GREEN + effectivePermissions.get(s).getValue());
			}
			toAdd.setAction(ClickEvent.Action.SUGGEST_COMMAND);
			if (s.startsWith(grant)) {
				toAdd.setActionString(s.substring(grant.length()));
			} else {
				toAdd.setActionString(s);
			}
			menu.addSub(toAdd);
		});
		return menu;
	}
}
