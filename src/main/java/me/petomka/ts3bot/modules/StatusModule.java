package me.petomka.ts3bot.modules;

import lombok.AllArgsConstructor;
import me.petomka.ts3bot.config.AccountLinker;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.UtilChatColor;

public interface StatusModule {

	boolean requiresTeamspeakConnection();

	default boolean requiresDatabaseConnection() {
		return (AccountLinker.linker == null || AccountLinker.linker.getLinkerType() == LinkerHandler.LinkerType.MYSQL) && usesDatabaseConnection();
	}

	boolean usesDatabaseConnection();

	String getName();

	Status getStatus();

	Menu getDetailedInfo();

	@AllArgsConstructor
	enum Status {
		OK('a'),
		WARN('e'),
		ERROR('c');

		char color;

		public static Status getWorse(Status s1, Status s2) {
			if (s1.ordinal() > s2.ordinal()) {
				return s1;
			}
			return s2;
		}

		public static boolean isWorse(Status bad, Status worse) {
			return getWorse(bad, worse) == worse;
		}

		public String toString() {
			return UtilChatColor.getByChar(color) + name();
		}
	}

}
