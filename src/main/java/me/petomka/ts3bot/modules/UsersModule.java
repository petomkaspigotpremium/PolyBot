package me.petomka.ts3bot.modules;

import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.google.common.collect.Maps;
import lombok.Data;
import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.Array;
import me.petomka.ts3bot.util.ChatUtils;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.UtilChatColor;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class UsersModule implements StatusModule {

	public static void init() {
		ModuleHandler.getInstance().addStatusModule(new UsersModule());
	}

	@Override
	public boolean requiresTeamspeakConnection() {
		return true;
	}

	@Override
	public boolean usesDatabaseConnection() {
		return true;
	}

	@Override
	public String getName() {
		return "Users";
	}

	@Override
	public Status getStatus() {
		AtomicReference<Status> worst = new AtomicReference<>(Status.OK);
		calcClientStati().values().stream()
				.map(ClientStatus::getStatus)
				.forEach(s -> {
					if (StatusModule.Status.isWorse(worst.get(), s)) {
						worst.set(s);
					}
				});
		return worst.get();
	}

	private Map<Client, ClientStatus> calcClientStati() {
		Map<Client, ClientStatus> clientStati = Maps.newHashMap();
		List<Client> clients = MainManager.main.getTS3Api().getClients();
		for (Client client : clients) {
			ClientStatus status = new ClientStatus();
			if (Array.containsInt(client.getServerGroups(), UniMethods.getLinkServergroupID())) {
				status.hasLinkGroup = true;
			}
			if (LinkerHandler.linker.isClientLinked(client.getUniqueIdentifier())) {
				status.isLinkedDatabase = true;
			}
			clientStati.put(client, status);
		}
		return clientStati;
	}

	@Override
	public Menu getDetailedInfo() {
		Map<Client, ClientStatus> clients = calcClientStati();
		Menu menu = new Menu(UtilChatColor.GOLD + "Users Status:");
		for (Map.Entry<Client, ClientStatus> clientStatus : clients.entrySet()
				.stream()
				.sorted(Comparator.comparing(o -> o.getKey().getNickname()))
				.collect(Collectors.toList())) {
			ClientStatus status = clientStatus.getValue();
			menu.addSub(
					new Menu(status.getStatusColor() +
							clientStatus.getKey().getNickname(),
							UtilChatColor.GRAY + "DB-Linked: " +
									ChatUtils.formatBoolean(status.isLinkedDatabase) + "\n" +
									UtilChatColor.GRAY + "Has Link-Group: " + ChatUtils.formatBoolean(status.hasLinkGroup))
			);
		}
		return menu;
	}

	@Data
	private static class ClientStatus {

		boolean hasLinkGroup = false;
		boolean isLinkedDatabase = false;

		Status getStatus() {
			if (hasLinkGroup == isLinkedDatabase) {
				return Status.OK;
			}
			if (!isLinkedDatabase) {
				return Status.ERROR;
			}
			return Status.WARN;
		}

		UtilChatColor getStatusColor() {
			if (hasLinkGroup || isLinkedDatabase) {
				return UtilChatColor.getByChar(getStatus().color);
			}
			return UtilChatColor.GRAY;
		}
	}
}
