package me.petomka.ts3bot.mysql;

import me.petomka.ts3bot.main.MainManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created by Benedikt on 10.08.2017.
 */
class BukkitConfig extends Config {

	private FileConfiguration config;

	public void createConfig() {
		File file = new File(MainManager.main.getDataFolder(), "database.yml");
		if (!file.exists()) {
			file.mkdirs();
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		config = YamlConfiguration.loadConfiguration(file);

		String db = "database.";

		config.addDefault(db + "host", "hostname");
		config.addDefault(db + "port", 3306);
		config.addDefault(db + "user", "username");
		config.addDefault(db + "password", "password");
		config.addDefault(db + "database", "name");
		config.addDefault(db + "table-name", "ts3bot");
		config.options().copyDefaults(true);
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

		host = config.getString(db + "host");
		port = config.getInt(db + "port");
		user = config.getString(db + "user");
		password = config.getString(db + "password");
		database = config.getString(db + "database");
		table_prefix = config.getString(db + "table-prefix");
	}

}
