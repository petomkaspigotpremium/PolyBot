package me.petomka.ts3bot.mysql;

import com.google.common.io.ByteStreams;
import me.petomka.ts3bot.main.BungeeMain;
import me.petomka.ts3bot.main.MainManager;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;

/**
 * Created by Benedikt on 10.08.2017.
 */
class BungeeConfig extends Config {

	private Configuration config;

	public void createConfig() {
		File configFile = new File(MainManager.main.getDataFolder(), "database.yml");
		if (!configFile.exists()) {
			try {
				configFile.createNewFile();
				try (InputStream is = ((BungeeMain) MainManager.main).getResourceAsStream("me/petomka/ts3bot/bungeefiles/database.yml");
					 OutputStream os = new FileOutputStream(configFile)) {
					ByteStreams.copy(is, os);
				}
			} catch (IOException e) {
				throw new RuntimeException("Unable to create configuration file", e);
			}
		}
		try {
			config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
			String db = "database.";
			host = config.getString(db + "host");
			port = config.getInt(db + "port");
			user = config.getString(db + "user");
			password = config.getString(db + "password");
			database = config.getString(db + "database");
			table_prefix = config.getString(db + "table-prefix");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
