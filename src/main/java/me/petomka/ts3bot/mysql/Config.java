package me.petomka.ts3bot.mysql;

import me.petomka.ts3bot.main.MainManager;

/**
 * Created by Benedikt on 10.08.2017.
 */
abstract class Config {

	String host,
			user,
			password,
			database,
			table_prefix;

	int port;

	static Config initConfig() {
		switch (MainManager.mainType) {
			case BUNGEE:
				return new BungeeConfig();
			case BUKKIT:
				return new BukkitConfig();
		}
		return null;
	}

	abstract void createConfig();

}
