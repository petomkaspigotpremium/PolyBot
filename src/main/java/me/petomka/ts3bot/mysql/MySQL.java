package me.petomka.ts3bot.mysql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.experimental.UtilityClass;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.modules.DatabaseModule;
import me.petomka.ts3bot.modules.StatusModule;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

@UtilityClass
public class MySQL {

	public static String TABLE_PREFIX;

	private HikariConfig hikariConfig = new HikariConfig();
	private HikariDataSource dataSource;

	private Config config = Config.initConfig();

	private static boolean isInitialized = false;

	public static void initMySQL() {
		if(isInitialized) {
			return;
		}
		config = Config.initConfig();
		DatabaseModule.init();
		if (config != null) {
			config.createConfig();
			TABLE_PREFIX = config.table_prefix;

			hikariConfig.setJdbcUrl("jdbc:mysql://" + config.host + ":" + config.port + "/" + config.database + "?useSSL=false");
			hikariConfig.setUsername(config.user);
			hikariConfig.setPassword(config.password);

			hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
			hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
			hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
			hikariConfig.addDataSourceProperty("maxLifetime", "600000"); //600_000 ms -> 10 minutes

			dataSource = new HikariDataSource(hikariConfig);
			DatabaseModule.setDatabaseStatus(StatusModule.Status.OK);
			isInitialized = true;
		} else {
			throw new NullPointerException("Connection to MySQL failed");
		}
	}

	/**
	 * Gibt die Verbindung
	 *
	 * @return Verbindung
	 */
	public static Connection getConnection() throws SQLException {
		initMySQL();
		return dataSource.getConnection();
	}

	/**
	 * Testet ob eine gültige Verbindung besteht
	 *
	 * @return true wenn ja
	 */
	public boolean hasConnection() {
		return dataSource != null && dataSource.isRunning() && !dataSource.isClosed();
	}

	/**
	 * Alle Ergebnisse die deine Anfrage bringt, werden in der zurückgegebenen HashMap gespeichert.
	 * Die Key-Value ist der Name der Spalte der abgefragten Tabelle, die zurückgegebene ArrayList ist die
	 * gesamte Spalte.
	 * Gleiche Indezes bei verschiedenen ArrayLists aus dieser HashMap bilden also einen Datensatz.
	 *
	 * @param query Die Abfrage
	 * @return die resultierende Tabelle
	 */
	public HashMap<String, ArrayList<Object>> executeManyQueries(String query) {
		HashMap<String, ArrayList<Object>> columns = new HashMap<>();
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement(query)) {
				try (ResultSet resultSet = stmt.executeQuery()) {
					ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
					List<ArrayList<Object>> lists = new ArrayList<>();
					for (int i = 0; i < resultSetMetaData.getColumnCount(); i++) {
						lists.add(new ArrayList<>());
					}
					while (resultSet.next()) {
						for (int i = 0; i < resultSetMetaData.getColumnCount(); i++) {
							lists.get(i).add(resultSet.getObject(i + 1));
						}
					}
					for (int i = 0; i < resultSetMetaData.getColumnCount(); i++) {
						columns.put(resultSetMetaData.getColumnLabel(i + 1), lists.get(i));
					}
				}
			}
		} catch (SQLException exc) {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Error executing database query", exc);
		}
		return columns;
	}
}

