package me.petomka.ts3bot.mysql;

import me.petomka.ts3bot.main.MainManager;

import javax.annotation.Nullable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.logging.Level;

public class SQLUtils {

	public static void setInt(PreparedStatement stmt, int index, int value) {
		try {
			stmt.setInt(index, value);
		} catch (SQLException e) {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Error setting an integer to prepared statement", e);
		}
	}

	public static void setString(PreparedStatement stmt, int index, String value) {
		try {
			stmt.setString(index, value);
		} catch (SQLException e) {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Error setting a string to prepared statement", e);
		}
	}

	public static void setUUID(PreparedStatement stmt, int index, UUID value) {
		SQLUtils.setString(stmt, index, value.toString());
	}

	public static @Nullable
	Integer getInt(ResultSet resultSet, String columnName) {
		try {
			return resultSet.getInt(columnName);
		} catch (SQLException e) {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Error getting an integer from result set", e);
		}
		return null;
	}

	public static @Nullable
	String getString(ResultSet resultSet, String columnName) {
		try {
			return resultSet.getString(columnName);
		} catch (SQLException e) {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Error getting a string from result set", e);
		}
		return null;
	}

	public static @Nullable
	UUID getUUID(ResultSet resultSet, String columnName) {
		String uuidString = SQLUtils.getString(resultSet, columnName);
		if (uuidString == null) {
			return null;
		}
		try {
			return UUID.fromString(uuidString);
		} catch (Exception e) {
			MainManager.main.getPluginLogger().log(Level.SEVERE, "Error parsing a UUID from result set", e);
		}
		return null;
	}

}
