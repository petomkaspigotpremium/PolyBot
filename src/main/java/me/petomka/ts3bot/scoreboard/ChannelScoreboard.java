package me.petomka.ts3bot.scoreboard;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.api.event.ClientLeaveEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.failsafe.TSDependant;
import me.petomka.ts3bot.failsafe.TSDependencyManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.scoreboard.scoreboardapi.AbstractScoreboardAPI;
import me.petomka.ts3bot.util.UtilChatColor;
import me.petomka.ts3bot.util.player.UtilPlayer;
import me.petomka.ts3bot.uuidfetcher.UUIDFetcher;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by Benedikt on 09.08.2017.
 */
public abstract class ChannelScoreboard extends TS3EventAdapter implements TSDependant {

	private static final String sNormal = ConfigManager.getString("ts.scoreboard.symbols.normal");
	private static final String sActive = ConfigManager.getString("ts.scoreboard.symbols.active");
	private static final String sRecord = ConfigManager.getString("ts.scoreboard.symbols.recording");
	private static final String sInMute = ConfigManager.getString("ts.scoreboard.symbols.input-mute");
	private static final String sOutMute = ConfigManager.getString("ts.scoreboard.symbols.output-mute");
	private static final String uFormatNormal = ConfigManager.getString("ts.scoreboard.user.normal");
	private static final String uFormatNoTalkPower = ConfigManager.getString("ts.scoreboard.user.no-talk-power");
	private static final boolean showQueryClients = ConfigManager.getBoolean("ts.scoreboard.show-query-clients");
	protected static TS3Api api = MainManager.main.getTS3Api();
	static Map<UUID, ChannelScoreboard> playerChannelScoreboards = Maps.newConcurrentMap();
	private static Map<Integer, String> channelAliases = Maps.newHashMap();
	private static Map<Integer, List<Client>> clientInChannels = getClientInChannels();

	static {
		Map<String, Object> configuredAliases = MainManager.main.mapConfigSection("ts.scoreboard.channel-aliases");
		configuredAliases.entrySet().stream()
				.filter(entry -> entry.getValue() instanceof String)
				.forEach(entry -> channelAliases.put(Integer.parseInt(entry.getKey()),
						UtilChatColor.translateAlternateColorCodes('&', (String) entry.getValue())));
	}

	ChannelInfo channelInfo;
	ClientInfo clientInfo;
	int channelID = -1;
	@Getter
	private UtilPlayer utilPlayer;
	@Getter
	@Setter
	private AbstractScoreboardAPI scoreboardAPI;
	private List<Integer> clientIDsInChannel = Lists.newArrayList();

	public ChannelScoreboard(UtilPlayer player) {
		this.utilPlayer = player;
		TSDependencyManager.registerTSDependency(this);
	}

	public static ChannelScoreboard getBoard(UtilPlayer utilPlayer) {
		return playerChannelScoreboards.get(utilPlayer.getUUID());
	}

	private static Map<Integer, List<Client>> getClientInChannels() {
		if (!MainManager.isSelectedTeamspeakServerRunning()) {
			return Maps.newHashMap();
		}
		return api.getClients()
				.stream()
				.filter(client -> showQueryClients || !client.isServerQueryClient())
				.collect(Collectors.groupingBy(Client::getChannelId));
	}

	@Override
	public void onConnectionLoss() {
		dispose();
	}

	@Override
	public void onReconnect() {

	}

	@Override
	public void onClientLeave(ClientLeaveEvent e) {
		if (e.getClientId() == clientInfo.getId()) {
			dispose();
		}
	}

	@Override
	public void onClientMoved(ClientMovedEvent e) {
		if (e.getClientId() == clientInfo.getId()) {
			channelID = e.getTargetChannelId();
		}
		if (e.getClientId() != clientInfo.getId() && e.getTargetChannelId() != channelID && !clientIDsInChannel.contains(e.getClientId())) {
			return;
		}
		updateScoreboard();
	}

	final String formatChannelName(ChannelInfo channelInfo) {
		String alias = channelAliases.get(channelInfo.getId());
		if (alias != null) {
			return alias;
		}
		return channelInfo.getName().replaceFirst("(\\[([crl*])?[spacer]([0-9a-zA-Z])+?])", "");
	}

	public abstract void dispose();

	void updateScoreboard() {
		if (!MainManager.isSelectedTeamspeakServerRunning()) {
			//scoreboard are then handled via failsafe.TSDependencyManager
			return;
		}
		if (scoreboardAPI == null) {
			return;
		}
		if (channelID == -1) {
			return;
		}
		channelInfo = api.getChannelInfo(channelID);
		if (channelInfo == null) {
			return;
		}
		scoreboardAPI.updateDisplayName(formatChannelName(channelInfo));

		clientIDsInChannel.clear();

		if (clientInChannels == null || LinkerHandler.linker == null || clientInChannels.get(channelID) == null) {
			return;
		}

		HashMap<String, Integer> formats = new HashMap<>();
		for (Client client : clientInChannels.get(channelID)) {
			String name = client.getNickname();
			if (LinkerHandler.linker.isClientLinked(client.getUniqueIdentifier())) {
				name = UUIDFetcher.getName(LinkerHandler.linker.getPlayerUUIDbyClientUID(client.getUniqueIdentifier()));
			}
			if (name == null) {
				continue;
			}
			boolean isMicMuted = client.isInputMuted();
			boolean isSpeakerMuted = client.isOutputMuted();
			boolean canTalk = client.getTalkPower() >= channelInfo.getNeededTalkPower();
			int talkPower = client.getTalkPower();
			boolean isRecording = client.isRecording();
			boolean isActive = client.getIdleTime() < 800;
			String format = ConfigManager.getRawString("ts.scoreboard.format")
					.replaceAll("<symbol>", ("§" + name.charAt(0) + "§" + name.charAt(1)) + (isSpeakerMuted ? sInMute :
							(isMicMuted ? sOutMute : (isRecording ? sRecord : (isActive ? sActive : sNormal)))))
					.replaceAll("<user>", "§r" + (canTalk ? uFormatNormal : uFormatNoTalkPower))
					.replaceAll("<user>", name);
			if (format.length() > 32) {
				format = format.substring(0, 32);
			}
			formats.put(format, talkPower);
			clientIDsInChannel.add(client.getId());
		}
		scoreboardAPI.setScores(formats);
	}

	static class ScoreboardUpdaterTask implements Runnable {

		@Override
		public void run() {
			try {
				clientInChannels = getClientInChannels();
				playerChannelScoreboards.values().forEach(ChannelScoreboard::updateScoreboard);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
