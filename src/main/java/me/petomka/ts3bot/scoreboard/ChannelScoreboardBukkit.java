package me.petomka.ts3bot.scoreboard;

import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.failsafe.TSDependencyManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.scoreboard.scoreboardapi.BukkitScoreboardAPI;
import me.petomka.ts3bot.util.player.UtilPlayerBukkit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Benedikt on 08.08.2017.
 */
public class ChannelScoreboardBukkit extends ChannelScoreboard implements Listener {

	static {
		Bukkit.getScheduler().scheduleSyncRepeatingTask((JavaPlugin) MainManager.main, new ScoreboardUpdaterTask(), 60L, ConfigManager.getInt("scoreboard-update-ticks"));
	}

	private Player player;
	private BukkitScoreboardAPI scoreboardAPI;

	public ChannelScoreboardBukkit(Player player) {
		super(new UtilPlayerBukkit(player));
		Bukkit.getPluginManager().registerEvents(this, (Plugin) MainManager.main);
		if (getBoard(getUtilPlayer()) != null) {
			dispose();
			return;
		}
		player.sendMessage(ConfigManager.getString("ts.scoreboard.activated"));
		playerChannelScoreboards.put(getUtilPlayer().getUUID(), this);
		if (!LinkerHandler.linker.isPlayerLinked(player.getUniqueId())) {
			player.sendMessage(ConfigManager.getString("message.link-required"));
			return;
		}
		this.player = player;
		clientInfo = api.getClientByUId(MainManager.getAny(LinkerHandler.linker.getClientUIDbyPlayerUUID(player.getUniqueId())));
		if (clientInfo == null) {
			throw new IllegalStateException("TS3 Client cannot be null");
		}
		channelID = clientInfo.getChannelId();
		channelInfo = api.getChannelInfo(channelID);
		scoreboardAPI = new BukkitScoreboardAPI(player, formatChannelName(channelInfo));
		this.setScoreboardAPI(scoreboardAPI);

		updateScoreboard();

		scoreboardAPI.displayToPlayerInSidebar();
	}

	public void dispose() {
		TSDependencyManager.removeTSDependency(this);
		HandlerList.unregisterAll(this);
		scoreboardAPI.removeScoreboard();
		playerChannelScoreboards.remove(getUtilPlayer().getUUID());
		api.removeTS3Listeners(this);
		player.sendMessage(ConfigManager.getString("ts.scoreboard.deactivated"));
		this.player = null;
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent event) {
		if (event.getPlayer().equals(player)) {
			playerChannelScoreboards.remove(getUtilPlayer().getUUID());
			HandlerList.unregisterAll(this);
		}
	}

}
