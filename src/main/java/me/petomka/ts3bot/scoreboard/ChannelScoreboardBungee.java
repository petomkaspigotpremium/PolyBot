package me.petomka.ts3bot.scoreboard;

import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.failsafe.TSDependencyManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.scoreboard.scoreboardapi.BungeeScoreboardAPI;
import me.petomka.ts3bot.util.player.UtilPlayerBungee;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import java.util.concurrent.TimeUnit;

/**
 * Created by Benedikt on 08.08.2017.
 */
@SuppressWarnings("deprecation")
public class ChannelScoreboardBungee extends ChannelScoreboard implements Listener {

	static {
		ProxyServer.getInstance().getScheduler().schedule((Plugin) MainManager.main, new ScoreboardUpdaterTask(), 3,
				ConfigManager.getInt("scoreboard-update-ticks") * 50, TimeUnit.MILLISECONDS);
	}

	private ProxiedPlayer player;
	private BungeeScoreboardAPI scoreboardAPI;

	public ChannelScoreboardBungee(ProxiedPlayer player) {
		super(new UtilPlayerBungee(player));
		this.player = player;
		ProxyServer.getInstance().getPluginManager().registerListener((Plugin) MainManager.main, this);
		if (getBoard(getUtilPlayer()) != null) {
			dispose();
			return;
		}
		player.sendMessage(ConfigManager.getString("ts.scoreboard.activated"));
		playerChannelScoreboards.put(getUtilPlayer().getUUID(), this);
		if (!LinkerHandler.linker.isPlayerLinked(player.getUniqueId())) {
			player.sendMessage(ConfigManager.getString("message.link-required"));
			return;
		}
		clientInfo = api.getClientByUId(MainManager.getAny(LinkerHandler.linker.getClientUIDbyPlayerUUID(player.getUniqueId())));
		if (clientInfo == null) {
			playerChannelScoreboards.remove(getUtilPlayer().getUUID());
			throw new IllegalStateException("TS3 Client cannot be null");
		}
		channelID = clientInfo.getChannelId();
		channelInfo = api.getChannelInfo(channelID);
		scoreboardAPI = new BungeeScoreboardAPI(player, formatChannelName(channelInfo));
		this.setScoreboardAPI(scoreboardAPI);

		updateScoreboard();

		scoreboardAPI.displayToPlayerInSidebar();
	}

	public void dispose() {
		TSDependencyManager.removeTSDependency(this);
		if (player.isConnected()) {
			scoreboardAPI.removeScoreboard();
			player.sendMessage(ConfigManager.getString("ts.scoreboard.deactivated"));
		}
		api.removeTS3Listeners(this);
		ProxyServer.getInstance().getPluginManager().unregisterListener(this);
		playerChannelScoreboards.remove(getUtilPlayer().getUUID());
		this.player = null;
	}

	@EventHandler
	public void onLeave(PlayerDisconnectEvent event) {
		if (event.getPlayer().equals(this.player)) {
			playerChannelScoreboards.remove(getUtilPlayer().getUUID());
			ProxyServer.getInstance().getPluginManager().unregisterListener(this);
		}
	}

}
