package me.petomka.ts3bot.scoreboard.scoreboardapi;

import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.petomka.ts3bot.util.player.UtilPlayer;

import java.util.Map;
import java.util.UUID;

@RequiredArgsConstructor
public abstract class AbstractScoreboardAPI {

	static final Map<UtilPlayer, AbstractScoreboardAPI> playerAPIs = Maps.newHashMap();
	@Getter
	private final UtilPlayer utilPlayer;
	;
	@Getter
	private final String uniqueName = UUID.randomUUID().toString().substring(0, 16);

	public static AbstractScoreboardAPI getPlayerScoreboard(UtilPlayer utilPlayer) {
		return playerAPIs.get(utilPlayer);
	}

	public static boolean hasPlayerScoreboard(UtilPlayer utilPlayer) {
		return playerAPIs.containsKey(utilPlayer);
	}

	public static void removeAllScoreboards() {
		playerAPIs.values().forEach(AbstractScoreboardAPI::removeScoreboard);
	}

	public abstract void removeScore(String scoreName);

	public abstract void removeScoreboard();

	public abstract void setScore(String scoreName, int value);

	public abstract void setScores(Map<String, Integer> scores);

	public abstract void updateDisplayName(String displayName);

	public abstract void displayToPlayerInSidebar();

}
