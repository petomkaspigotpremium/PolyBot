package me.petomka.ts3bot.scoreboard.scoreboardapi;

import me.petomka.ts3bot.util.player.UtilPlayerBukkit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class BukkitScoreboardAPI extends AbstractScoreboardAPI {

	private final String uniqueName;
	private Player player;
	private Scoreboard board;

	private Objective objective;

	private List<String> scores = new ArrayList<>();

	public BukkitScoreboardAPI(Player player, String title) {
		super(new UtilPlayerBukkit(player));
		if (playerAPIs.get(getUtilPlayer()) != null) {
			uniqueName = "";
			return;
		}
		this.player = player;
		uniqueName = UUID.randomUUID().toString().substring(0, 16);
		board = Bukkit.getScoreboardManager().getNewScoreboard();
		objective = board.registerNewObjective(uniqueName, "dummy");
		objective.setDisplayName(title);

		displayToPlayerInSidebar();

		playerAPIs.put(getUtilPlayer(), this);
	}

	public void updateDisplayName(String newTitle) {
		objective.setDisplayName(newTitle);
	}

	@Override
	public void displayToPlayerInSidebar() {
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		player.setScoreboard(board);
	}

	public void removeScoreboard() {
		player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		playerAPIs.remove(getUtilPlayer());
		player = null;
		board = null;
		objective = null;
	}

	public void setScore(String stringDisplay, int scoreValue) {
		Score score = objective.getScore(stringDisplay);
		score.setScore(scoreValue);
		if (scores.contains(stringDisplay)) {
			scores.add(stringDisplay);
		}
	}

	public void removeScore(String stringDisplay) {
		board.resetScores(stringDisplay);
		scores.remove(stringDisplay);
	}

	public void setScores(Map<String, Integer> nuScores) {
		List<String> contained = new ArrayList<>();
		for (String key : nuScores.keySet()) {
			contained.add(key);
			setScore(key, nuScores.get(key));
		}
		scores.removeAll(contained);
		for (String score : scores) {
			board.resetScores(score);
		}
		scores.clear();
		scores.addAll(contained);
	}

	public Player getPlayer() {
		return player;
	}

	public String getUniqueName() {
		return uniqueName;
	}
}
