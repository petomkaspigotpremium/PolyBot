package me.petomka.ts3bot.scoreboard.scoreboardapi;

import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.util.player.UtilPlayerBungee;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.chat.ComponentSerializer;
import net.md_5.bungee.protocol.packet.ScoreboardDisplay;
import net.md_5.bungee.protocol.packet.ScoreboardObjective;
import net.md_5.bungee.protocol.packet.ScoreboardScore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class BungeeScoreboardAPI extends AbstractScoreboardAPI {

	private final String uniqueName;

	private ScoreboardObjective objective;
	private ProxiedPlayer player;

	private ArrayList<String> containedScoreNames = new ArrayList<>();

	private List<String> scores = new ArrayList<>();

	public BungeeScoreboardAPI(ProxiedPlayer player, String title) {
		super(new UtilPlayerBungee(player));
		if (playerAPIs.get(getUtilPlayer()) != null) {
			uniqueName = "";
			return;
		}

		playerAPIs.put(getUtilPlayer(), this);
		uniqueName = UUID.randomUUID().toString().substring(0, 16);
		if (title.length() > 30) {
			title = title.substring(0, 30).trim();
		}
		objective = new ScoreboardObjective(uniqueName, ComponentSerializer.toString(new ComponentBuilder(title).create()), ScoreboardObjective.HealthDisplay.INTEGER, (byte) 0);
		player.unsafe().sendPacket(objective);
		this.player = player;
	}

	@Override
	public void updateDisplayName(String newTitle) {
		if (newTitle.length() > 30) {
			newTitle = newTitle.substring(0, 30).trim();
		}
		objective = new ScoreboardObjective(uniqueName, getTitle(newTitle), ScoreboardObjective.HealthDisplay.INTEGER, (byte) 2);
		player.unsafe().sendPacket(objective);
	}

	private String getTitle(String title) {
		if(ConfigManager.getBoolean("ts.scoreboard.json")) {
			return ComponentSerializer.toString(new ComponentBuilder(title).create());
		}
		return title;
	}

	public void removeScoreboard() {
		objective = new ScoreboardObjective(uniqueName, "remove", ScoreboardObjective.HealthDisplay.INTEGER, (byte) 1);
		player.unsafe().sendPacket(objective);
		playerAPIs.remove(getUtilPlayer());
		objective = null;
		player = null;
	}

	public void setScore(String stringDisplay, int scoreDisplay) {
		if (stringDisplay.length() > 30) {
			stringDisplay = stringDisplay.substring(0, 30);
		}
		ScoreboardScore score = new ScoreboardScore(stringDisplay, (byte) 0, uniqueName, scoreDisplay);
		player.unsafe().sendPacket(score);
	}

	public void removeScore(String stringDisplay) {
		if (stringDisplay.length() > 30) {
			stringDisplay = stringDisplay.substring(0, 30);
		}
		ScoreboardScore score = new ScoreboardScore(stringDisplay, (byte) 1, uniqueName, 0);
		player.unsafe().sendPacket(score);
	}

	public void displayToPlayerInSidebar() {
		ScoreboardDisplay display = new ScoreboardDisplay((byte) 1, uniqueName);
		player.unsafe().sendPacket(display);
	}

	public String getUniqueName() {
		return uniqueName;
	}

	public void setScores(Map<String, Integer> nuScores) {
		List<String> contained = new ArrayList<>();
		for (String key : nuScores.keySet()) {
			contained.add(key);
			setScore(key, nuScores.get(key));
		}
		scores.removeAll(contained);
		for (String score : scores) {
			removeScore(score);
		}
		scores.clear();
		scores.addAll(contained);
	}

	public ProxiedPlayer getPlayer() {
		return player;
	}
}
