package me.petomka.ts3bot.tasks.afkmover;

import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.modules.FeaturesModule;
import me.petomka.ts3bot.modules.StatusModule;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.Pair;
import me.petomka.ts3bot.util.UtilChatColor;

import java.util.List;
import java.util.Map;

/**
 * Created by Benedikt on 13.10.2017.
 */
public abstract class AFKDetector implements Runnable, StatusModule {

	private static AFKDetector afkDetector;
	private Map<String, Integer> afkUserIDs = Maps.newHashMap();
	private List<Integer> ignoreChannels = Lists.newArrayList();
	private int idleMaxNormal, idleMaxMuted, idleMaxAway, afkChannelID;
	private String notifyMessage = "";
	private Status afkChannelStatus = Status.OK;

	AFKDetector() {
		FeaturesModule.registerFeature(this);
		idleMaxNormal = MainManager.main.getInt("afkchannel.afk-time-seconds");
		idleMaxMuted = MainManager.main.getInt("afkchannel.afk-time-seconds-muted");
		idleMaxAway = MainManager.main.getInt("afkchannel.afk-time-seconds-away");
		afkChannelID = MainManager.main.getInt("afkchannel.channel-id");
		notifyMessage = ConfigManager.getRawString("afkchannel.notify-message");
		ignoreChannels = ConfigManager.getIntegerList("afkchannel.ignore-channels");
		if (notifyMessage == null) {
			notifyMessage = "";
		}
		int intervalSeconds = MainManager.main.getInt("afkchannel.interval-seconds");
		scheduleTask(intervalSeconds);
	}

	public static void init() {
		switch (MainManager.mainType) {
			case BUKKIT:
				afkDetector = new AFKDetectorBukkit();
				break;
			case BUNGEE:
				afkDetector = new AFKDetectorBungee();
				break;
		}
	}

	@Override
	public boolean requiresTeamspeakConnection() {
		return true;
	}

	@Override
	public String getName() {
		return "AFKChannel";
	}

	@Override
	public Status getStatus() {
		getDetailedInfo();
		return afkChannelStatus;
	}

	@Override
	public boolean usesDatabaseConnection() {
		return false;
	}

	@Override
	public Menu getDetailedInfo() {
		Menu menu = new Menu(UtilChatColor.GOLD + getName() + " Status:");
		Menu data = new Menu(UtilChatColor.BLUE + "Idle configuration:");
		data.addSub(new Menu(UtilChatColor.DARK_AQUA + "Max Idle Time: " + UtilChatColor.WHITE + idleMaxNormal));
		data.addSub(new Menu(UtilChatColor.DARK_AQUA + "Max Idle Time with Input Mute: " + UtilChatColor.WHITE + idleMaxMuted));
		data.addSub(new Menu(UtilChatColor.DARK_AQUA + "Max Idle Time with Away: " + UtilChatColor.WHITE + idleMaxAway));
		menu.addSub(data);

		Channel channel = MainManager.main.getTS3Api().getChannels().stream()
				.filter(channel1 -> channel1.getId() == afkChannelID)
				.findAny().orElse(null);
		String start = UtilChatColor.BLUE + "AFK Channel: ";
		if (channel != null) {
			menu.addSub(new Menu(start + UtilChatColor.WHITE + channel.getName() + " (" + afkChannelID + ")"));
			afkChannelStatus = Status.OK;
		} else {
			menu.addSub(new Menu(start + UtilChatColor.RED + "invalid"));
			afkChannelStatus = Status.ERROR;
		}

		Map<Integer, Channel> channels = MainManager.main.getTS3Api().getChannels().stream()
				.map(channel1 -> Pair.of(channel1.getId(), channel))
				.collect(Pair.pairToMap());
		Menu ignoreChannelsMenu = new Menu(UtilChatColor.BLUE + "Ignoring channels:");
		ignoreChannels.forEach(channelId -> {
			Channel ignored = channels.get(channelId);
			if (ignored == null) {
				menu.addSub(
						new Menu("" + UtilChatColor.DARK_AQUA + channelId + ": " + UtilChatColor.RED + "invalid")
				);
			} else {
				menu.addSub(
						new Menu("" + UtilChatColor.DARK_AQUA + channelId + ": " + UtilChatColor.WHITE + ignored.getName())
				);
			}
		});

		return menu;
	}

	public abstract void scheduleTask(int intervalSeconds);

	public Map<String, Integer> getAfkUserIDs() {
		return afkUserIDs;
	}

	@Override
	public void run() {
		MainManager.main.getTS3Api().getClients().forEach(c -> {
			if (!afkUserIDs.containsKey(c.getUniqueIdentifier())) {
				if (c.getChannelId() == afkChannelID || ignoreChannels.contains(c.getChannelId())) {
					//users already in afk channel themselves are not further processed
					return;
				}
				/*Process Users who are not marked AFK*/
				if (idleMaxNormal > 0 && c.getIdleTime() > idleMaxNormal * 1000 && !c.isAway() && !c.isOutputMuted()) {
					//headphones not muted, and not away
					seeYouSoon(c);
				} else if (idleMaxMuted > 0 && c.getIdleTime() > idleMaxMuted * 1000 && !c.isAway() && c.isOutputMuted()) {
					//headphones MUTED, and NOT away
					seeYouSoon(c);
				} else if (idleMaxAway > 0 && c.getIdleTime() > idleMaxAway * 1000 && c.isAway()) {
					//away
					seeYouSoon(c);
				}
			} else {
				/*Process users who are marked AFK*/
				if (!c.isAway() && !c.isOutputMuted() && (c.getIdleTime() < idleMaxNormal * 1000 || c.getIdleTime() < idleMaxAway * 1000 || c.getIdleTime() < idleMaxAway * 1000)) {
					welcomeBack(c.getUniqueIdentifier());
				}
			}
		});
	}

	public void seeYouSoon(Client client) {
		afkUserIDs.put(client.getUniqueIdentifier(), client.getChannelId());
		MainManager.main.getTS3Api().moveClient(client.getId(), afkChannelID);
		if (!notifyMessage.equals("")) {
			MainManager.main.getTS3Api().sendPrivateMessage(client.getId(), notifyMessage);
		}
	}

	public void welcomeBack(String uid) {
		ClientInfo info = MainManager.main.getTS3Api().getClientByUId(uid);
		afkUserIDs.remove(uid);
		if (info.getChannelId() != afkChannelID) {
			return;
		}
		try {
			MainManager.main.getTS3Api().moveClient(info.getId(), afkUserIDs.get(uid));
		} catch (Exception ignored) {
			//Channel was probably deleted
		}
	}
}
