package me.petomka.ts3bot.tasks.afkmover;

import me.petomka.ts3bot.main.MainManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Interval seconds is multiplied internally by 20, to convert to seconds
 */
public class AFKDetectorBukkit extends AFKDetector {
	@Override
	public void scheduleTask(int intervalSeconds) {
		Bukkit.getScheduler().scheduleSyncRepeatingTask((JavaPlugin) MainManager.main, this, 0, intervalSeconds * 20);
	}
}
