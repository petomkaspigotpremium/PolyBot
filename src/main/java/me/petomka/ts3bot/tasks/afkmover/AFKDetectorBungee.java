package me.petomka.ts3bot.tasks.afkmover;

import me.petomka.ts3bot.main.MainManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.concurrent.TimeUnit;

/**
 * Created by Benedikt on 13.10.2017.
 */
public class AFKDetectorBungee extends AFKDetector {
	@Override
	public void scheduleTask(int intervalSeconds) {
		ProxyServer.getInstance().getScheduler().schedule((Plugin) MainManager.main, this, 0L, intervalSeconds, TimeUnit.SECONDS);
	}
}
