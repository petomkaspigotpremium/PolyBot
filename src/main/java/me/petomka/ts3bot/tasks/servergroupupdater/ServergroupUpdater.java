package me.petomka.ts3bot.tasks.servergroupupdater;

import com.github.theholywaffle.teamspeak3.api.wrapper.DatabaseClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroup;
import com.google.common.collect.Maps;
import lombok.Getter;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.modules.FeaturesModule;
import me.petomka.ts3bot.modules.StatusModule;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.Pair;
import me.petomka.ts3bot.util.UtilChatColor;
import me.petomka.ts3bot.util.player.UtilPlayer;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Benedikt on 10.08.2017.
 */
public abstract class ServergroupUpdater implements StatusModule {

	@Getter
	private static ServergroupUpdater instance;

	@Getter
	protected static Map<String, Integer> permissionToGroup = Maps.newHashMap();

	private boolean updateMessagesEnabled = ConfigManager.getBoolean("servergroup-updater.messages-enabled");
	private Status serverGroupUpdaterStatus = Status.OK;

	ServergroupUpdater() {
		FeaturesModule.registerFeature(this);
		List<String> assignGroups = ConfigManager.getStringList("servergroup-updater.permissions");
		Pattern integer = Pattern.compile("[0-9]\\b|[1-9]+?[0-9]+");
		for (String group : assignGroups) {
			String[] split = group.split(":");
			if (split.length != 2) {
				throw new IllegalArgumentException("Invalid configuration! Permissions in servergroup-updater must follow this pattern: \"permissionnode : servergoupID\"");
			}
			split[0] = split[0].replaceAll(" ", "");
			split[1] = split[1].replaceAll(" ", "");
			if (!integer.matcher(split[1]).matches()) {
				throw new IllegalArgumentException("Invalid configuration! ServergroupIDs in servergroup-updater have to be an integer!");
			}
			permissionToGroup.put(split[0], Integer.parseInt(split[1]));
		}
		instance = this;
	}

	public static void initServergroupUpdater() {
		switch (MainManager.mainType) {
			case BUNGEE:
				new ServergroupUpdaterBungee();
				break;
			case BUKKIT:
				new ServergroupUpdaterBukkit();
				break;
		}
	}

	@Override
	public boolean requiresTeamspeakConnection() {
		return true;
	}

	@Override
	public String getName() {
		return "Servergroup Updater";
	}

	@Override
	public Status getStatus() {
		getDetailedInfo();
		return serverGroupUpdaterStatus;
	}

	@Override
	public Menu getDetailedInfo() {
		Menu menu = new Menu(UtilChatColor.GOLD + getName() + " Status:");
		menu.addSub(new Menu(UtilChatColor.BLUE + "Interval (seconds): " + UtilChatColor.WHITE + getIntervalSeconds()));

		Menu permissionsMenu = new Menu(UtilChatColor.BLUE + "Permissions to Servergroup:");
		menu.addSub(permissionsMenu);

		Map<Integer, ServerGroup> serverGroups = MainManager.main.getTS3Api().getServerGroups().stream()
				.map(serverGroup -> Pair.of(serverGroup.getId(), serverGroup))
				.collect(Pair.pairToMap());

		permissionToGroup.forEach((permName, groupID) -> {
			ServerGroup group = serverGroups.get(groupID);
			String start = UtilChatColor.DARK_AQUA + permName + ": ";
			if (group == null) {
				permissionsMenu.addSub(
						new Menu(start + UtilChatColor.RED + "invalid")
				);
				serverGroupUpdaterStatus = Status.ERROR;
			} else {
				permissionsMenu.addSub(
						new Menu(start + UtilChatColor.GREEN + group.getName() + " (" + groupID + ")")
				);
			}
		});

		return menu;
	}

	public void performUpdate(UtilPlayer player, String permission, DatabaseClientInfo info) {
		if (info == null) {
			return;
		}
		/*what a line huh? if(commandsender does not have permission AND is NOT in servergroup currently being looped) -> continue*/
		boolean hasPerm = player.hasPermission(permission);
		boolean hasGroup;
		try {
			hasGroup = MainManager.main.getTS3Api().getServerGroupsByClientId(info.getDatabaseId())
					.stream().collect(Collectors.groupingBy(ServerGroup::getId)).containsKey(permissionToGroup.get(permission));
		} catch (NullPointerException e) {
			return;
		}
		if (hasPerm == hasGroup) {
			return;
		}
		String servergroupname = "ERROR";
		for (ServerGroup serverGroup : MainManager.main.getTS3Api().getServerGroups()) {
			if (serverGroup.getId() == permissionToGroup.get(permission)) {
				servergroupname = serverGroup.getName();
				break;
			}
		}
		if (hasPerm) {
			MainManager.main.getTS3Api().addClientToServerGroup(permissionToGroup.get(permission), info.getDatabaseId());
			if (updateMessagesEnabled) {
				player.sendMessage(ConfigManager.getString("servergroup-updater.added-to-group").replaceAll("<groupname>", servergroupname));
			}
		} else {
			MainManager.main.getTS3Api().removeClientFromServerGroup(permissionToGroup.get(permission), info.getDatabaseId());
			if (updateMessagesEnabled) {
				player.sendMessage(ConfigManager.getString("servergroup-updater.removed-from-group").replaceAll("<groupname>", servergroupname));
			}
		}
	}

	int getIntervalSeconds() {
		return ConfigManager.getInt("servergroup-updater.interval-seconds");
	}
}
