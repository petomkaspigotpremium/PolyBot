package me.petomka.ts3bot.tasks.servergroupupdater;

import com.github.theholywaffle.teamspeak3.api.wrapper.DatabaseClientInfo;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.player.UtilPlayerBukkit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Benedikt on 10.08.2017.
 */
public class ServergroupUpdaterBukkit extends ServergroupUpdater implements Runnable {

	@SuppressWarnings("deprecation")
	ServergroupUpdaterBukkit() {
		Bukkit.getScheduler().runTaskLaterAsynchronously((Plugin) MainManager.main, this,
				getIntervalSeconds() * 20L);
	}

	@Override
	public void run() {
		if (!MainManager.isSelectedTeamspeakServerRunning()) {
			return;
		}
		for (Player player : Bukkit.getOnlinePlayers()) {
			for (String uid : LinkerHandler.linker.getClientUIDbyPlayerUUID(player.getUniqueId())) {
				if (!player.isOnline()) {
					continue;
				}
				DatabaseClientInfo info = MainManager.main.getTS3Api().getDatabaseClientByUId(uid);
				for (String permission : permissionToGroup.keySet()) {
					performUpdate(new UtilPlayerBukkit(player), permission, info);
				}
			}
		}
		Bukkit.getScheduler().runTaskLaterAsynchronously((Plugin) MainManager.main, this,
				getIntervalSeconds() * 20L);
	}

	@Override
	public boolean usesDatabaseConnection() {
		return true;
	}
}
