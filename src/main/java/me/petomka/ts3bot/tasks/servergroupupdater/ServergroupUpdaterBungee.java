package me.petomka.ts3bot.tasks.servergroupupdater;

import com.github.theholywaffle.teamspeak3.api.wrapper.DatabaseClientInfo;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.player.UtilPlayerBungee;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.concurrent.TimeUnit;

/**
 * Created by Benedikt on 10.08.2017.
 */
public class ServergroupUpdaterBungee extends ServergroupUpdater implements Runnable {

	public ServergroupUpdaterBungee() {
		ProxyServer.getInstance().getScheduler().schedule((Plugin) MainManager.main, this,
				getIntervalSeconds(), TimeUnit.SECONDS);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		if (!MainManager.isSelectedTeamspeakServerRunning()) {
			return;
		}
		for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
			for (String uid : LinkerHandler.linker.getClientUIDbyPlayerUUID(player.getUniqueId())) {
				if (!player.isConnected()) {
					continue;
				}
				DatabaseClientInfo info = MainManager.main.getTS3Api().getDatabaseClientByUId(uid);
				for (String permission : permissionToGroup.keySet()) {
					performUpdate(new UtilPlayerBungee(player), permission, info);
				}
			}
		}
		ProxyServer.getInstance().getScheduler().schedule((Plugin) MainManager.main, this,
				getIntervalSeconds(), TimeUnit.SECONDS);
	}

	@Override
	public boolean usesDatabaseConnection() {
		return true;
	}
}
