package me.petomka.ts3bot.ts3listeners;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import me.petomka.ts3bot.config.AccountLinker;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.IconSetter;
import me.petomka.ts3bot.uuidfetcher.UUIDFetcher;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Benedikt on 25.09.2017.
 */
public class JoinUpdateListener extends TS3EventAdapter {

	public static void initJoinUpdateListener() {
		MainManager.main.getTS3Api().addTS3Listeners(new JoinUpdateListener());
	}

	@Override
	public void onClientJoin(ClientJoinEvent e) {
		greet(e);
		if (!LinkerHandler.linker.isClientLinked(e.getUniqueClientIdentifier())) {
			return;
		}
		UUID uuid = LinkerHandler.linker.getPlayerUUIDbyClientUID(e.getUniqueClientIdentifier());
		TS3Api ts3 = MainManager.main.getTS3Api();
		String name = UUIDFetcher.getName(uuid);
		if (name != null) {
			if (ConfigManager.getBoolean("link.description.set")) {
				ts3.editClient(e.getClientId(), Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION,
						ConfigManager.getRawString("link.description.format").replaceAll("<player>", name)
								.replaceAll("<uuid>", uuid.toString())));
			}
		}
		if (ConfigManager.getBoolean("link.set-commandsender-head-icon")) {
			try {
				IconSetter.setIcon(e.getUniqueClientIdentifier(), uuid, MainManager.main.getTS3Api());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private void greet(ClientJoinEvent event) {
		boolean unlinkedEnabled = ConfigManager.getBoolean("greeting.enabled-unlinked");
		boolean linkedEnabled = ConfigManager.getBoolean("greeting.enabled-linked");
		boolean linked = AccountLinker.linker.isClientLinked(event.getUniqueClientIdentifier());
		int clid = event.getClientId();
		String message = null;
		if (unlinkedEnabled && !linked) {
			message = ConfigManager.getRawString("greeting.message-unlinked");
		}
		if (linkedEnabled && linked) {
			message = ConfigManager.getRawString("greeting.message-linked");
		}
		if (message == null) {
			return;
		}
		int visits = 0;
		if (message.contains("<visits>")) {
			ClientInfo client = MainManager.main.getTS3Api().getClientInfo(event.getClientId());
			visits = client.getTotalConnections();
		}
		message = message.replace("<visits>", String.valueOf(visits));
		MainManager.main.getTS3Api().sendPrivateMessage(clid, message);
	}
}
