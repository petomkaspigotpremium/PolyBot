package me.petomka.ts3bot.ts3listeners;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class TS3MessageReceivedEventBukkit extends Event {

	private static HandlerList handlerList = new HandlerList();
	private final String message;
	private final String invokerNameExact;

	public TS3MessageReceivedEventBukkit(String message, String invokerNameExact) {
		this.message = message;
		this.invokerNameExact = invokerNameExact;
	}

	public static HandlerList getHandlerList() {
		return handlerList;
	}

	public String getMessage() {
		return message;
	}

	public String getInvokerNameExact() {
		return invokerNameExact;
	}

	public HandlerList getHandlers() {
		return handlerList;
	}

}
