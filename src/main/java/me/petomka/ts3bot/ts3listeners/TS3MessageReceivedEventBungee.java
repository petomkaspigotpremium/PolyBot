package me.petomka.ts3bot.ts3listeners;

import net.md_5.bungee.api.plugin.Event;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class TS3MessageReceivedEventBungee extends Event {

	private final String message;

	private final String invokerNameExact;

	public TS3MessageReceivedEventBungee(String message, String invokerNameExact) {
		this.message = message;
		this.invokerNameExact = invokerNameExact;
	}

	public String getInvokerNameExact() {
		return invokerNameExact;
	}

	public String getMessage() {
		return message;
	}
}
