package me.petomka.ts3bot.ts3listeners;

import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.google.common.collect.Lists;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.ts3listeners.botcommands.commandutils.CommandManager;

import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Benedikt on 07.08.2017.
 */
public abstract class TextMessageListener extends TS3EventAdapter {

	public static final Pattern URLClientPattern = Pattern.compile("\\[URL=client://\\d/(?<uid>(.+?))[~].+?](?<name>.+?)\\[/URL]");
	private static final Pattern TAG_PATTERN = Pattern.compile("(\\[(?<tag>.+?).+?/\\k<tag>])|(\".+?\")|([^ ]+(\\b|\\B))|(?:[ ])");
	private final int clientID;

	public TextMessageListener() {
		clientID = MainManager.getWhoAmI().getId();
	}

	public static void registerTextMessageListener() {
		if (MainManager.mainType == MainManager.MainType.BUKKIT) {
			MainManager.main.getTS3Api().addTS3Listeners(new TextMessageListenerBukkit());
		} else if (MainManager.mainType == MainManager.MainType.BUNGEE) {
			MainManager.main.getTS3Api().addTS3Listeners(new TextMessageListenerBungee());
		}
	}

	@Override
	public void onTextMessage(TextMessageEvent e) {
		if (!(e.getTargetMode() == TextMessageTargetMode.CLIENT && e.getInvokerId() != clientID)) {
			return;
		}
		if (e.getMessage().startsWith("!")) {

			List<String> cmdArgs = Lists.newArrayList();
			String message = e.getMessage().substring(1);
			Matcher matcher = TAG_PATTERN.matcher(message);

			matcherWhile:
			while (matcher.find()) {
				MatchResult matchResult = matcher.toMatchResult();

				for (int groupCount = 1; groupCount <= matchResult.groupCount(); groupCount++) {
					String group = matcher.group(groupCount);
					if (group != null) {
						cmdArgs.add(matcher.group(groupCount));
						continue matcherWhile;
					}
				}
			}
			String cmd = cmdArgs.get(0);
			cmdArgs.remove(0);

			CommandManager.executeCommand(cmd, e.getInvokerUniqueId(), cmdArgs);
		} else {
			callEvent(e);
		}
	}

	public abstract void callEvent(TextMessageEvent event);
}
