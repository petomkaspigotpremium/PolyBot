package me.petomka.ts3bot.ts3listeners;

import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class TextMessageListenerBukkit extends TextMessageListener {
	@Override
	public void callEvent(TextMessageEvent e) {
		org.bukkit.Bukkit.getPluginManager().callEvent(new TS3MessageReceivedEventBukkit(e.getMessage(), e.getInvokerName()));
	}
}
