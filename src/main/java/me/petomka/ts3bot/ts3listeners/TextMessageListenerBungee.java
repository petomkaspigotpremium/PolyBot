package me.petomka.ts3bot.ts3listeners;

import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import net.md_5.bungee.api.ProxyServer;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class TextMessageListenerBungee extends TextMessageListener {
	@Override
	public void callEvent(TextMessageEvent e) {
		ProxyServer.getInstance().getPluginManager().callEvent(new TS3MessageReceivedEventBungee(e.getMessage(), e.getInvokerName()));
	}
}
