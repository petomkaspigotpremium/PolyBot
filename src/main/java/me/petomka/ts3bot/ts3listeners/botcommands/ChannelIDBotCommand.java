package me.petomka.ts3bot.ts3listeners.botcommands;

import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.ts3listeners.botcommands.commandutils.BotCommand;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Benedikt on 26.09.2017.
 */
public class ChannelIDBotCommand extends BotCommand {


	public ChannelIDBotCommand() {
		super("channelid",
				ConfigManager.getRawString("tscommands.channelid.description"),
				ConfigManager.getRawString("tscommands.channelid.permission"));
	}

	@Override
	public void execute(String invokerUID, List<String> args) {
		ClientInfo info = MainManager.main.getTS3Api().getClientByUId(invokerUID);
		int cID = info.getChannelId();
		String chN = MainManager.main.getTS3Api().getChannels().stream().collect(Collectors.groupingBy(Channel::getId)).get(cID).get(0).getName();
		MainManager.main.getTS3Api().sendPrivateMessage(info.getId(), ConfigManager.getRawString("tscommands.channelid.answer")
				.replace("<channel>", chN).replace("<id>", String.valueOf(cID)));
	}
}
