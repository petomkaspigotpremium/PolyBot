package me.petomka.ts3bot.ts3listeners.botcommands;

import com.github.theholywaffle.teamspeak3.api.wrapper.DatabaseClientInfo;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.ts3listeners.TextMessageListener;
import me.petomka.ts3bot.ts3listeners.botcommands.commandutils.BotCommand;

import java.util.List;
import java.util.regex.Matcher;

public class CldbidBotCommand extends BotCommand {

	public CldbidBotCommand() {
		super("cldbid",
				ConfigManager.getRawString("tscommands.cldbid.description"),
				ConfigManager.getRawString("tscommands.cldbid.permission"));
	}

	@Override
	public void execute(String invokerUID, List<String> args) {
		int clientID = MainManager.main.getTS3Api().getClientByUId(invokerUID).getId();
		if (args.size() != 1) {
			MainManager.main.getTS3Api().sendPrivateMessage(clientID, ConfigManager.getRawString("tscommands.cldbid.syntax"));
			return;
		}

		Matcher m = TextMessageListener.URLClientPattern.matcher(args.get(0));

		if (!m.matches()) {
			MainManager.main.getTS3Api().sendPrivateMessage(clientID, ConfigManager.getRawString("tscommands.cldbid.invalid-client-url"));
			return;
		}

		String clientUID = m.group("uid");

		DatabaseClientInfo databaseClientInfo = MainManager.main.getTS3Api().getDatabaseClientByUId(clientUID);

		if (databaseClientInfo == null) {
			MainManager.main.getTS3Api().sendPrivateMessage(clientID, ConfigManager.getRawString("tscommands.cldbid.client-not-found"));
			return;
		}

		MainManager.main.getTS3Api().sendPrivateMessage(clientID, ConfigManager.getRawString("tscommands.cldbid.answer")
				.replace("<cldbid>", String.valueOf(databaseClientInfo.getDatabaseId()))
				.replace("<client>", args.get(0)));
	}
}
