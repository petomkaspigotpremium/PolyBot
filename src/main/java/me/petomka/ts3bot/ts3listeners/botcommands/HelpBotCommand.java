package me.petomka.ts3bot.ts3listeners.botcommands;

import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.ts3listeners.botcommands.commandutils.BotCommand;
import me.petomka.ts3bot.ts3listeners.botcommands.commandutils.CommandManager;
import me.petomka.ts3bot.util.commandsender.UtilCommandSender;

import java.util.List;

/**
 * Created by Benedikt on 26.09.2017.
 */
public class HelpBotCommand extends BotCommand {

	private boolean showOnlyPermitted = ConfigManager.getBoolean("tscommands.help.only-permitted");

	public HelpBotCommand() {
		super("help",
				ConfigManager.getRawString("tscommands.help.description"),
				null);
	}

	@Override
	public void execute(String invokerUID, List<String> args) {
		int clientID = MainManager.main.getTS3Api().getClientByUId(invokerUID).getId();
		UtilCommandSender player = MainManager.main.getUtilPlayer(LinkerHandler.linker.getPlayerUUIDbyClientUID(invokerUID));
		MainManager.main.getTS3Api().sendPrivateMessage(clientID, "[s]                  [/s]");
		for (BotCommand cmd : CommandManager.getBotCommands()) {
			if (cmd.hasPermission() && showOnlyPermitted) {
				if (player == null) {
					continue;
				}
				if (!player.hasPermission(cmd.getPermission())) {
					continue;
				}
			}
			String description = "[color=#ff3a3a][b]missing description![/b][/color]";
			if (cmd.getDescription() != null && !cmd.getDescription().isEmpty()) {
				description = cmd.getDescription();
			}
			MainManager.main.getTS3Api().sendPrivateMessage(clientID, "!" + cmd.getName() + ": " + description);
		}
		MainManager.main.getTS3Api().sendPrivateMessage(clientID, "[s]                  [/s]");
	}
}
