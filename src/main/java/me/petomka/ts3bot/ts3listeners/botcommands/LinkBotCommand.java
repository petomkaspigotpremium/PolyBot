package me.petomka.ts3bot.ts3listeners.botcommands;

import com.google.common.collect.Maps;
import me.petomka.ts3bot.config.AccountLinker;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.ts3listeners.botcommands.commandutils.BotCommand;
import me.petomka.ts3bot.util.clientverifier.ClientVerifier;
import me.petomka.ts3bot.util.player.UtilPlayer;
import me.petomka.ts3bot.util.playerverifier.PlayerVerifier;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class LinkBotCommand extends BotCommand {

	private Map<String, PlayerVerifier> triedLinks = Maps.newHashMap();

	public LinkBotCommand() {
		super("link", ConfigManager.getRawString("tscommands.link.description"), null);
	}

	@Override
	public void execute(final String invokerUID, List<String> args) {
		int clientID = MainManager.main.getTS3Api().getClientByUId(invokerUID).getId();
		if (AccountLinker.linker.isClientLinked(invokerUID)) {
			MainManager.main.getTS3Api().sendPrivateMessage(clientID,
					ConfigManager.getRawString("tscommands.link.already-linked"));
			return;
		}
		if (args.size() != 1) {
			MainManager.main.getTS3Api().sendPrivateMessage(clientID,
					ConfigManager.getRawString("tscommands.link.syntax"));
			return;
		}
		UtilPlayer player = MainManager.main.getUtilPlayer(args.get(0));
		if (player == null) {
			MainManager.main.getTS3Api().sendPrivateMessage(clientID,
					ConfigManager.getRawString("tscommands.link.not-online"));
			return;
		}
		AtomicBoolean isAllowed = new AtomicBoolean(false);
		PlayerVerifier verifier = triedLinks.compute(invokerUID, (s, playerVerifier) -> {
			if (playerVerifier == null) {
				playerVerifier = new PlayerVerifier(player, invokerUID);
				playerVerifier.setOnFinished(() -> triedLinks.remove(invokerUID));
				isAllowed.set(true);
			}
			return playerVerifier;
		});
		if (!isAllowed.get()) {
			MainManager.main.getTS3Api().sendPrivateMessage(clientID,
					ConfigManager.getRawString("tscommands.link.not-allowed")
							.replace("<code>", verifier.getCode()));
			return;
		}
	}
}
