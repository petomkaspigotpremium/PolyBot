package me.petomka.ts3bot.ts3listeners.botcommands;

import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.ts3listeners.botcommands.commandutils.BotCommand;

import java.util.List;

public class NoteBotCommand extends BotCommand {

	public NoteBotCommand() {
		super("note",
				ConfigManager.getRawString("tscommands.note.description"),
				ConfigManager.getRawString("tscommands.note.permission"));
	}


	@Override
	public void execute(String invokerUID, List<String> args) {
		int clientID = MainManager.main.getTS3Api().getClientByUId(invokerUID).getId();
		args.forEach(arg -> MainManager.main.getTS3Api().sendPrivateMessage(clientID, arg));
	}
}
