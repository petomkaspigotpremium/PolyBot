package me.petomka.ts3bot.ts3listeners.botcommands;

import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.google.common.collect.Lists;
import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.ts3listeners.botcommands.commandutils.BotCommand;
import me.petomka.ts3bot.util.commandsender.UtilCommandSender;
import me.petomka.ts3bot.util.commandsender.UtilCommandSenderTSClient;

import java.util.List;

public class SupportchannelBotCommand extends BotCommand {

	public SupportchannelBotCommand() {
		super("supportchannel", ConfigManager.getRawString("tscommands.supportchannel.description"),
				ConfigManager.getRawString("tscommands.supportchannel.permission"));
	}

	@Override
	public void execute(String invokerUID, List<String> args) {
		ClientInfo executor = MainManager.main.getTS3Api().getClientByUId(invokerUID);
		List<String> allArgs = Lists.newArrayList();
		allArgs.add("supportchannel");
		allArgs.addAll(args);
		UtilCommandSender sender = new UtilCommandSenderTSClient(executor);
		if (args.isEmpty()) {
			sender.sendMessage(ConfigManager.getRawString("tscommands.supportchannel.syntax"));
		}
		UniMethods.ts3botSubCommand(sender, allArgs.toArray(new String[0]));
	}
}
