package me.petomka.ts3bot.ts3listeners.botcommands;

import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.AccountLinker;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.ts3listeners.botcommands.commandutils.BotCommand;

import java.util.List;
import java.util.UUID;

public class UnlinkBotCommand extends BotCommand {

	public UnlinkBotCommand() {
		super("unlink", ConfigManager.getRawString("tscommands.unlink.description"), null);
	}

	@Override
	public void execute(String invokerUID, List<String> args) {
		int clientID = MainManager.main.getTS3Api().getClientByUId(invokerUID).getId();
		if (!args.isEmpty()) {
			MainManager.main.getTS3Api().sendPrivateMessage(clientID,
					ConfigManager.getRawString("tscommands.unlink.syntax"));
			return;
		}
		UUID linkedUUID = AccountLinker.linker.getPlayerUUIDbyClientUID(invokerUID);
		if (linkedUUID == null) {
			MainManager.main.getTS3Api().sendPrivateMessage(clientID,
					ConfigManager.getRawString("tscommands.unlink.not-linked"));
			return;
		}
		UniMethods.unlinkPlayer(null, linkedUUID, invokerUID, null, null,
				null, null);
	}
}
