package me.petomka.ts3bot.ts3listeners.botcommands.commandutils;

import me.petomka.ts3bot.config.ConfigManager;

import java.util.List;

/**
 * Created by Benedikt on 26.09.2017.
 */
public abstract class BotCommand {

	private String name;

	private String description;

	private String permission;

	public BotCommand(String name, String description, String permission) {
		this.name = name;
		this.description = description;
		this.permission = permission;
		CommandManager.registerCommand(this);
	}

	public abstract void execute(String invokerUID, List<String> args);

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean hasPermission() {
		if (permission == null || permission.isEmpty()) {
			return false;
		}
		return ConfigManager.getBoolean("tscommands.permissions-enabled");
	}

	public String getPermission() {
		return permission;
	}
}
