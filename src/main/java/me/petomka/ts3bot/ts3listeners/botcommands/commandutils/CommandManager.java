package me.petomka.ts3bot.ts3listeners.botcommands.commandutils;

import com.google.common.collect.Lists;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.ts3listeners.botcommands.ChannelIDBotCommand;
import me.petomka.ts3bot.ts3listeners.botcommands.CldbidBotCommand;
import me.petomka.ts3bot.ts3listeners.botcommands.HelpBotCommand;
import me.petomka.ts3bot.ts3listeners.botcommands.LinkBotCommand;
import me.petomka.ts3bot.ts3listeners.botcommands.SupportchannelBotCommand;
import me.petomka.ts3bot.ts3listeners.botcommands.UnlinkBotCommand;
import me.petomka.ts3bot.util.commandsender.UtilCommandSender;

import java.util.List;
import java.util.UUID;

/**
 * Created by Benedikt on 26.09.2017.
 */
public class CommandManager {

	private static List<BotCommand> botCommands = Lists.newArrayList();

	private static boolean permissionsEnabled = ConfigManager.getBoolean("tscommands.permissions-enabled");

	static {
		new HelpBotCommand();
		new ChannelIDBotCommand();
		new CldbidBotCommand();
		new SupportchannelBotCommand();
		//new NoteBotCommand();
		new LinkBotCommand();
		new UnlinkBotCommand();
	}

	public static void registerCommand(BotCommand cmd) {
		botCommands.add(cmd);
	}

	public static void executeCommand(String name, String invokerUID, List<String> args) {
		for (BotCommand cmd : botCommands) {
			if (cmd.getName().equalsIgnoreCase(name)) {
				if (permissionsEnabled && cmd.getPermission() != null) {
					UUID uuid = LinkerHandler.linker.getPlayerUUIDbyClientUID(invokerUID);
					if (uuid == null || !MainManager.main.isPlayerOnline(uuid)) {
						int clid = MainManager.main.getTS3Api().getClientByUId(invokerUID).getId();
						MainManager.main.getTS3Api().sendPrivateMessage(clid, ConfigManager.getRawString("tscommands.in-game-connection-required"));
						return;
					}
					UtilCommandSender player = MainManager.main.getUtilPlayer(uuid);
					if (cmd.hasPermission() && !player.hasPermission(cmd.getPermission())) {
						int clid = MainManager.main.getTS3Api().getClientByUId(invokerUID).getId();
						MainManager.main.getTS3Api().sendPrivateMessage(clid, ConfigManager.getRawString("tscommands.no-permission"));
						return;
					}
				}
				cmd.execute(invokerUID, args);
				return;
			}
		}
		int clid = MainManager.main.getTS3Api().getClientByUId(invokerUID).getId();
		MainManager.main.getTS3Api().sendPrivateMessage(clid, ConfigManager.getRawString("tscommands.unknown-command"));
	}

	public static List<BotCommand> getBotCommands() {
		return botCommands;
	}
}
