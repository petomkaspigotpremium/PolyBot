package me.petomka.ts3bot.ts3listeners.proxychecker;

import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.google.common.collect.Maps;
import lombok.AllArgsConstructor;
import lombok.Data;
import me.petomka.ts3bot.antiproxy.Geofence;
import me.petomka.ts3bot.antiproxy.ProxyChecker;
import me.petomka.ts3bot.antiproxy.ProxyQueryResult;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.modules.FeaturesModule;
import me.petomka.ts3bot.modules.StatusModule;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.UtilChatColor;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.stream.Collectors;

public abstract class JoinProxyCheckListener extends TS3EventAdapter implements StatusModule {

	final String platformMC = ConfigManager.getRawString("proxycheck.platform.mc");
	final String kickMessageProxy = ConfigManager.getRawString("proxycheck.kick-message");
	final boolean disallowProxyMCConnections = ConfigManager.getBoolean("proxycheck.disallow-mc-connection");
	final boolean geofenceEnabled = ConfigManager.getBoolean("proxycheck.geofence.enabled");
	final String kickMessageGeofence = ConfigManager.getRawString("proxycheck.geofence.kick-message");
	final boolean disallowGeofenceMCConnections = ConfigManager.getBoolean("proxycheck.geofence.disallow-mc-connection");
	private final String platformTS = ConfigManager.getRawString("proxycheck.platform.ts");
	private final String ingamePermission = ConfigManager.getRawString("proxycheck.notify-permission");
	private final Map<String, JoinData> cachedIPs = Maps.newHashMap();
	private final boolean disallowProxyTSConnections = ConfigManager.getBoolean("proxycheck.disallow-ts-connection");
	private final boolean disallowGeofenceTSConnections = ConfigManager.getBoolean("proxycheck.geofence.disallow-ts-connection");
	private Status proxyCheckStatus = Status.OK;

	JoinProxyCheckListener() {
		MainManager.main.getTS3Api().addTS3Listeners(this);
		FeaturesModule.registerFeature(this);
	}

	public static void initProxyCheckListener() {
		switch (MainManager.mainType) {
			case BUNGEE:
				new JoinProxyCheckListenerBungee();
				break;
			case BUKKIT:
				new JoinProxyCheckListenerBukkit();
				break;
		}
	}

	@Override
	public boolean requiresTeamspeakConnection() {
		return true;
	}

	@Override
	public String getName() {
		return "ProxyCheck";
	}

	@Override
	public Status getStatus() {
		return proxyCheckStatus;
	}

	@Override
	public boolean usesDatabaseConnection() {
		return false;
	}

	@Override
	public Menu getDetailedInfo() {
		Menu menu = new Menu(UtilChatColor.GOLD + getName() + " Status:");

		Menu proxyMenu = new Menu(UtilChatColor.BLUE + "Proxy Blocking:");
		proxyMenu.addSub(
				new Menu(UtilChatColor.DARK_AQUA + "Disallow Proxy MC Connections: " +
						(disallowProxyMCConnections ? UtilChatColor.GREEN : UtilChatColor.RED) + disallowProxyMCConnections)
		);
		proxyMenu.addSub(
				new Menu(UtilChatColor.DARK_AQUA + "Disallow Proxy TS Connections: " +
						(disallowProxyTSConnections ? UtilChatColor.GREEN : UtilChatColor.RED) + disallowProxyTSConnections)
		);
		menu.addSub(proxyMenu);

		Menu geofenceMenu = new Menu(UtilChatColor.BLUE + "Geofencing: ");
		if (geofenceEnabled) {
			geofenceMenu.addSub(
					new Menu(UtilChatColor.DARK_AQUA + "Disallow Geofenced MC Connections: " +
							(disallowGeofenceMCConnections ? UtilChatColor.GREEN : UtilChatColor.RED) +
							disallowGeofenceMCConnections)
			);
			geofenceMenu.addSub(
					new Menu(UtilChatColor.DARK_AQUA + "Disallow Geofenced TS Connections: " +
							(disallowGeofenceTSConnections ? UtilChatColor.GREEN : UtilChatColor.RED) +
							disallowGeofenceTSConnections)
			);
			Menu geofenceListMenu = new Menu(UtilChatColor.DARK_AQUA + "Geofence Country List:");
			geofenceListMenu.addSub(
					new Menu(UtilChatColor.LIGHT_PURPLE + "Acts as whitelist: " +
							(Geofence.isFenceIsWhitelist() ? UtilChatColor.GREEN : UtilChatColor.RED) +
							Geofence.isFenceIsWhitelist())
			);
			Menu countryList = new Menu(UtilChatColor.LIGHT_PURPLE + "Countries:");
			Geofence.getFencedCountries().forEach(s ->
					countryList.addSub(
							new Menu(UtilChatColor.YELLOW + s)
					));
			geofenceListMenu.addSub(countryList);
			geofenceMenu.addSub(geofenceListMenu);
		} else {
			geofenceMenu.addSub(new Menu(UtilChatColor.RED + "Geofencing is disabled."));
		}
		menu.addSub(geofenceMenu);

		return menu;
	}

	@Override
	public void onClientJoin(ClientJoinEvent event) {
		Client client = MainManager.main.getTS3Api().getClientByUId(event.getUniqueClientIdentifier());
		new Thread(() -> {
			try {
				JoinData joinData = checkIPAddress(client.getIp(), client.getNickname(), platformTS).get();
				if (joinData.wasProxy && disallowProxyTSConnections) {
					MainManager.main.getTS3Api().kickClientFromServer(kickMessageProxy, client);
					return;
				}
				if (joinData.wasGeofenced && disallowGeofenceTSConnections) {
					MainManager.main.getTS3Api().kickClientFromServer(kickMessageGeofence, client);
				}
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}).start();
	}

	Future<JoinData> checkIPAddress(String ip, String username, String platform) {
		if (cachedIPs.getOrDefault(ip, new JoinData(false, false, "",
				0L)).timestamp + 1000 * 60 * 60 * 6 > System.currentTimeMillis()) {
			return new FutureTask<>(() -> cachedIPs.getOrDefault(ip, null));
		}
		cachedIPs.put(ip, new JoinData(false, false, "", System.currentTimeMillis()));
		FutureTask<JoinData> futureTask = new FutureTask<>(
				() -> {
					Future<Optional<ProxyQueryResult>> maybeLaterResult = ProxyChecker.queryIPAddress(
							ip
					);
					JoinData joinData = new JoinData(false, false, "", System.currentTimeMillis());
					try {
						maybeLaterResult.get().filter(ProxyChecker::isProxyIP).ifPresent(
								queryResult -> {
									joinData.wasProxy = true;
									alertTSAdmins(ip, username, platform, "", "proxycheck.notify-message-teamspeak");
									alertMCAdmins(ip, username, platform, "", "proxycheck.notify-message-ingame");
									cachedIPs.put(ip, joinData);
								}
						);
						maybeLaterResult.get().filter(Geofence::isFenced).ifPresent(
								queryResult -> {
									joinData.country = queryResult.getCountry();
									joinData.wasGeofenced = true;
									alertTSAdmins(ip, username, platform, joinData.country, "proxycheck.geofence.notify-message.teamspeak");
									alertMCAdmins(ip, username, platform, joinData.country, "proxycheck.geofence.notify-message-ingame");
									cachedIPs.put(ip, joinData);
								}
						);
					} catch (InterruptedException | ExecutionException e) {
						e.printStackTrace();
					}
					return joinData;
				}
		);
		futureTask.run();
		return futureTask;
	}

	private void alertTSAdmins(String ipAddress, String username, String platform, String country, String baseMessagePath) {
		List<Integer> adminGroups = ConfigManager.getIntegerList("proxycheck.notify-groups");
		List<Client> adminsToAlert = MainManager.main.getTS3Api().getClients().stream()
				.filter(client -> adminGroups.stream()
						.anyMatch(sgid -> containsInt(sgid, client.getServerGroups())))
				.collect(Collectors.toList());
		String message = ConfigManager.getRawString(baseMessagePath)
				.replace("<user>", username)
				.replace("<address>", ipAddress)
				.replace("<platform>", platform)
				.replace("<location>", country);
		adminsToAlert.forEach(admin ->
				MainManager.main.getTS3Api().sendPrivateMessage(admin.getId(), message));
	}

	private void alertMCAdmins(String username, String ipAddress, String platform, String country, String baseMessagePath) {
		String message = ConfigManager.getString(baseMessagePath)
				.replace("<user>", username)
				.replace("<address>", ipAddress)
				.replace("<platform>", platform)
				.replace("<location>", country);
		MainManager.main.getPlayers().stream()
				.filter(player -> player.hasPermission(ingamePermission))
				.forEach(player -> player.sendMessage(message));
	}

	private boolean containsInt(int toCheck, int[] array) {
		for (int intVal : array) {
			if (toCheck == intVal) {
				return true;
			}
		}
		return false;
	}

	@Data
	@AllArgsConstructor
	class JoinData {
		boolean wasProxy;
		boolean wasGeofenced;
		String country;
		long timestamp;
	}

}
