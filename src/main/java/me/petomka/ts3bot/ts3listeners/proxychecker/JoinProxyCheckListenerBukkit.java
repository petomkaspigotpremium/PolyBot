package me.petomka.ts3bot.ts3listeners.proxychecker;

import me.petomka.ts3bot.main.MainManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.Plugin;

import java.util.concurrent.ExecutionException;

public class JoinProxyCheckListenerBukkit extends JoinProxyCheckListener implements Listener {

	public JoinProxyCheckListenerBukkit() {
		Bukkit.getPluginManager().registerEvents(this, (Plugin) MainManager.main);
	}

	@EventHandler
	public void onJoin(PlayerLoginEvent event) {
		String ip = event.getPlayer().getAddress().getAddress().getHostAddress();
		new Thread(() -> {
			try {
				JoinData joinData = checkIPAddress(ip, event.getPlayer().getName(), platformMC).get();
				if (joinData.wasProxy && disallowProxyMCConnections) {
					event.getPlayer().kickPlayer(kickMessageProxy);
					return;
				}
				if (joinData.wasGeofenced && disallowGeofenceMCConnections) {
					event.getPlayer().kickPlayer(kickMessageGeofence);
				}
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}).start();
	}

}
