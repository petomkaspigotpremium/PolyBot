package me.petomka.ts3bot.ts3listeners.proxychecker;

import me.petomka.ts3bot.main.MainManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import java.util.concurrent.ExecutionException;

public class JoinProxyCheckListenerBungee extends JoinProxyCheckListener implements Listener {

	public JoinProxyCheckListenerBungee() {
		ProxyServer.getInstance().getPluginManager().registerListener((Plugin) MainManager.main, this);
	}

	@EventHandler
	public void onJoin(PostLoginEvent event) {
		String ip = event.getPlayer().getAddress().getAddress().getHostAddress();
		new Thread(() -> {
			try {
				JoinData joinData = checkIPAddress(ip, event.getPlayer().getName(), platformMC).get();
				if (joinData.wasProxy && disallowProxyMCConnections) {
					event.getPlayer().disconnect(TextComponent.fromLegacyText(kickMessageProxy));
					return;
				}
				if (joinData.wasGeofenced && disallowGeofenceMCConnections) {
					event.getPlayer().disconnect(TextComponent.fromLegacyText(kickMessageGeofence));
				}
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}).start();
	}

}
