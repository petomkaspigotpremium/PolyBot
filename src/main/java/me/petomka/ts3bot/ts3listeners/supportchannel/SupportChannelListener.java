package me.petomka.ts3bot.ts3listeners.supportchannel;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientLeaveEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroup;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import lombok.Getter;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.modules.FeaturesModule;
import me.petomka.ts3bot.modules.StatusModule;
import me.petomka.ts3bot.util.ListFunctions;
import me.petomka.ts3bot.util.Menu;
import me.petomka.ts3bot.util.Pair;
import me.petomka.ts3bot.util.UtilChatColor;
import me.petomka.ts3bot.uuidfetcher.UUIDFetcher;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Benedikt on 10.08.2017.
 */
public abstract class SupportChannelListener extends TS3EventAdapter implements StatusModule {

	@Getter
	private static SupportChannelListener instance;
	@Getter
	private List<Integer> supportChannelIDs;
	@Getter
	private List<Integer> serverGroupNotifyIDs;
	private Status supportChannelStatus = Status.OK;
	private Status supportChannelIDstatus = Status.OK;
	private Status serverGroupIDstatus = Status.OK;
	private Menu detailedStatus = new Menu(UtilChatColor.GOLD + "Supportchannels Status:");
	private Menu supportChannelMenu = new Menu(UtilChatColor.BLUE + "Channel IDs:");
	private Menu serverGroupMenu = new Menu(UtilChatColor.BLUE + "Notify Group IDs:");
	private boolean pokeInstead = ConfigManager.getBoolean("supportchannel.poke-instead");
	SupportChannelListener() {
		FeaturesModule.registerFeature(this);
		supportChannelIDs = Lists.newArrayList(ConfigManager.getIntegerList("supportchannel.channel-ids"));
		if (supportChannelIDs.isEmpty()) {
			supportChannelStatus = Status.ERROR;
			throw new IllegalArgumentException("Invalid configuration: Support Channel ID cannot be empty");
		}
		serverGroupNotifyIDs = Lists.newArrayList(ConfigManager.getIntegerList("supportchannel.notify-groups"));
		if (serverGroupNotifyIDs.isEmpty()) {
			supportChannelStatus = Status.ERROR;
			throw new IllegalArgumentException("Invalid configuration: Servergroups to notify cannot be empty");
		}
		MainManager.main.getTS3Api().addTS3Listeners(this);
		instance = this;
	}

	public static void initSupportChannelListener() {
		switch (MainManager.mainType) {
			case BUKKIT:
				new SupportChannelListenerBukkit();
				break;
			case BUNGEE:
				new SupportChannelListenerBungee();
				break;
		}
	}

	@Override
	public boolean requiresTeamspeakConnection() {
		return true;
	}

	private void recalcInternalStatus() {
		detailedStatus.clearSubs();
		supportChannelMenu.clearSubs();
		serverGroupMenu.clearSubs();

		detailedStatus.addSub(new Menu(UtilChatColor.BLUE + "Poke instead: " + (pokeInstead ? UtilChatColor.GREEN : UtilChatColor.RED) + pokeInstead));

		detailedStatus.addSub(supportChannelMenu);
		detailedStatus.addSub(serverGroupMenu);

		Map<Integer, Channel> channels = MainManager.main.getTS3Api().getChannels().stream()
				.map(channel -> Pair.of(channel.getId(), channel))
				.collect(Pair.pairToMap());
		supportChannelIDstatus = Status.OK;
		supportChannelIDs.forEach(channelID -> {
			Channel channel = channels.get(channelID);
			if (channel == null) {
				supportChannelMenu.addSub(
						new Menu("" + UtilChatColor.DARK_AQUA + channelID + ": " + UtilChatColor.RED + "invalid")
				);
				supportChannelIDstatus = Status.ERROR;
			} else {
				supportChannelMenu.addSub(
						new Menu("" + UtilChatColor.DARK_AQUA + channel.getName() +
								" (" + channelID + "): " + UtilChatColor.GREEN + "OK")
				);
			}
		});
		supportChannelMenu.setMessage(UtilChatColor.BLUE + "Channel IDs: " + supportChannelIDstatus);

		Map<Integer, ServerGroup> serverGroups = MainManager.main.getTS3Api().getServerGroups().stream()
				.map(serverGroup -> Pair.of(serverGroup.getId(), serverGroup))
				.collect(Pair.pairToMap());
		serverGroupIDstatus = Status.OK;
		serverGroupNotifyIDs.forEach(groupID -> {
			ServerGroup serverGroup = serverGroups.get(groupID);
			if (serverGroup == null) {
				serverGroupMenu.addSub(
						new Menu("" + UtilChatColor.DARK_AQUA + groupID + ": " + UtilChatColor.RED + "invalid")
				);
				serverGroupIDstatus = Status.ERROR;
			} else {
				serverGroupMenu.addSub(
						new Menu(UtilChatColor.DARK_AQUA + serverGroup.getName() +
								" (" + groupID + "): " + UtilChatColor.GREEN + "OK")
				);
			}
		});
		serverGroupMenu.setMessage(UtilChatColor.BLUE + "Notify Group IDs: " + serverGroupIDstatus);

		if (supportChannelIDstatus != Status.OK || serverGroupIDstatus != Status.OK) {
			supportChannelStatus = Status.ERROR;
		}
	}

	@Override
	public String getName() {
		return "Supportchannels";
	}

	@Override
	public Status getStatus() {
		recalcInternalStatus();
		return supportChannelStatus;
	}

	@Override
	public Menu getDetailedInfo() {
		recalcInternalStatus();
		return detailedStatus;
	}

	@Override
	public boolean usesDatabaseConnection() {
		return true;
	}

	@Override
	public void onClientMoved(ClientMovedEvent e) {
		if (!supportChannelIDs.contains(e.getTargetChannelId())) {
			return;
		}
		int notified = notifyServerGroup(MainManager.main.getTS3Api().getClientInfo(e.getClientId()));
		if (notified >= 0) {
			notified = Math.max(notified, supportChannelJoined(e.getClientId()));
			String message = ConfigManager.getRawString("supportchannel.user-notify-message");
			if (message.equals("")) {
				return;
			}
			message = message.replace("<team_online>", String.valueOf(notified));
			notifyUser(e.getClientId(), message);
		}
	}

	@Override
	public void onClientJoin(ClientJoinEvent e) {
		recalcInternalStatus();
	}

	@Override
	public void onClientLeave(ClientLeaveEvent e) {
		recalcInternalStatus();
	}

	private int notifyServerGroup(ClientInfo info) {
		String userName = info.getNickname();
		if (LinkerHandler.linker.isClientLinked(info.getUniqueIdentifier())) {
			userName = UUIDFetcher.getName(LinkerHandler.linker.getPlayerUUIDbyClientUID(info.getUniqueIdentifier()));
		}
		if (userName == null) {
			userName = "ERROR";
		}

		List<Client> allClients = MainManager.main.getTS3Api().getClients();

		String notifyMessage = ConfigManager.getRawString("supportchannel.ts-notify-message").replaceAll("<user>", userName);

		List<Client> clientsToNotify = allClients.stream()
				.filter(client -> IntStream.of(client.getServerGroups()).anyMatch(serverGroupNotifyIDs::contains))
				.filter(ListFunctions.distinctByKey(Client::getUniqueIdentifier))
				.collect(Collectors.toList());

		/*Check if the user who joined the support channel is from staff*/
		Optional<Client> user = clientsToNotify.stream()
				.filter(client -> client.getUniqueIdentifier().equals(info.getUniqueIdentifier()))
				.findFirst();

		if (user.isPresent()) {
			return -1;
		}

		final int[] notified = {0};

		clientsToNotify.forEach(client -> {
					try {
						if (pokeInstead) {
							MainManager.main.getTS3Api().pokeClient(client.getId(), notifyMessage);
						} else {
							MainManager.main.getTS3Api().sendPrivateMessage(client.getId(), notifyMessage);
						}
						++notified[0];
					} catch (Exception e) {
						MainManager.main.getPluginLogger().log(Level.WARNING, "Error while notifying a client about support: " + e.getMessage());
					}
				}
		);
		return notified[0];
	}

	public void closeSupportChannel(int channelID) {
		if (!supportChannelIDs.contains(channelID)) {
			return;
		}
		if (ConfigManager.getBoolean("supportchannel.close-with-password")) {
			MainManager.main.getTS3Api().editChannel(channelID,
					ImmutableMap.of(ChannelProperty.CHANNEL_PASSWORD, UUID.randomUUID().toString()));
		} else {
			MainManager.main.getTS3Api().editChannel(channelID,
					ImmutableMap.of(ChannelProperty.CHANNEL_MAXCLIENTS, "0",
							ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED, "0"));
		}
	}

	public void openSupportChannel(int channelID) {
		if (!supportChannelIDs.contains(channelID)) {
			return;
		}
		MainManager.main.getTS3Api().editChannel(channelID,
				ImmutableMap.of(ChannelProperty.CHANNEL_PASSWORD, "",
						ChannelProperty.CHANNEL_MAXCLIENTS, "1",
						ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED, "1"));
	}

	private void notifyUser(int invokerID, String message) {
		MainManager.main.getTS3Api().sendPrivateMessage(invokerID, message);
	}

	abstract int supportChannelJoined(int clientID);
}
