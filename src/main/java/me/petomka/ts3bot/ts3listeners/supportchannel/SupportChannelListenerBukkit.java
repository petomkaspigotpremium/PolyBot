package me.petomka.ts3bot.ts3listeners.supportchannel;

import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Benedikt on 10.08.2017.
 */
public class SupportChannelListenerBukkit extends SupportChannelListener {

	@Override
	int supportChannelJoined(int clientID) {
		ClientInfo info = MainManager.main.getTS3Api().getClientInfo(clientID);
		String name = info.getNickname();
		if (LinkerHandler.linker.isClientLinked(info.getUniqueIdentifier())) {
			name = Bukkit.getOfflinePlayer(LinkerHandler.linker.getPlayerUUIDbyClientUID(info.getUniqueIdentifier())).getName();
		}
		int notified = 0;
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (player.hasPermission("ts3bot.supportnotify")) {
				player.sendMessage(ConfigManager.getString("supportchannel.game-notify-message").replaceAll("<user>", name));
				++notified;
			}
		}
		return notified;
	}

	@Override
	public boolean usesDatabaseConnection() {
		return true;
	}
}
