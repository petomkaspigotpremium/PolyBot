package me.petomka.ts3bot.ts3listeners.supportchannel;

import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.uuidfetcher.UUIDFetcher;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created by Benedikt on 10.08.2017.
 */
public class SupportChannelListenerBungee extends SupportChannelListener {

	@SuppressWarnings("deprecation")
	@Override
	int supportChannelJoined(int clientID) {
		ClientInfo info = MainManager.main.getTS3Api().getClientInfo(clientID);
		String name = info.getNickname();
		if (LinkerHandler.linker.isClientLinked(info.getUniqueIdentifier())) {
			name = UUIDFetcher.getName(LinkerHandler.linker.getPlayerUUIDbyClientUID(info.getUniqueIdentifier()));
		}
		int notified = 0;
		for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
			if (player.hasPermission("ts3bot.supportnotify")) {
				player.sendMessage(ConfigManager.getString("supportchannel.game-notify-message").replaceAll("<user>", name));
				++notified;
			}
		}
		return notified;
	}

}
