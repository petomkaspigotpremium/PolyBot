package me.petomka.ts3bot.util;

import java.util.Collection;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class Array {

	public static boolean containsInt(int[] array, int value) {
		for (Integer i : array) {
			if (i == value) {
				return true;
			}
		}
		return false;
	}

	public static <T> boolean contains(Collection<T> array, T value) {
		for (T arrayValue : array) {
			if (arrayValue.equals(value)) {
				return true;
			}
		}
		return false;
	}

}
