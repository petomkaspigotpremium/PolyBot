package me.petomka.ts3bot.util;

public class ChatUtils {

	public static String formatBoolean(boolean b) {
		return b ? UtilChatColor.GREEN + "true" : UtilChatColor.RED + "false";
	}

}
