package me.petomka.ts3bot.util;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.DatabaseClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.IconFile;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.modules.PermissionsModule;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.UUID;
import java.util.logging.Level;
import java.util.zip.CRC32;

/**
 * Created by Benedikt on 09.08.2017.
 */
public class IconSetter {

	private static String HEAD_ICON_URL = ConfigManager.getRawString("link.head-icon-url");

	public static void setIcon(String uid, UUID playerUUID, TS3Api api) throws Exception {
		if (!PermissionsModule.canUploadIcon()) {
			MainManager.main.getPluginLogger().log(Level.WARNING, "Cannot upload icons. Check permission configuration!");
			return;
		}

		BufferedImage img;

		URL url = new URL(HEAD_ICON_URL.replace("<uuid>", playerUUID.toString()));
		//URL url = new URL("https://crafatar.com/avatars/" + playerUUID.toString() + ".png?size=16&overlay");

		img = ImageIO.read(url);

		ByteArrayOutputStream iconBytes = new ByteArrayOutputStream(128);

		ImageIO.write(img, "PNG", iconBytes);

		CRC32 crc32 = new CRC32();
		crc32.update(iconBytes.toByteArray());
		final long iconID = crc32.getValue();

		boolean isAlreadyUploaded = api.getIconList().stream()
				.map(IconFile::getIconId)
				.anyMatch(aLong -> aLong == iconID);

		if (!isAlreadyUploaded) {
			api.uploadIconDirect(iconBytes.toByteArray());
		}

		DatabaseClientInfo info = api.getDatabaseClientByUId(uid);
		if (info == null) {
			MainManager.main.getPluginLogger().log(Level.WARNING, "Could not set Icon for client with UID " + uid);
			return;
		}
		Client client = api.getClientByNameExact(info.getNickname(), true);
		if (client == null) {
			MainManager.main.getPluginLogger().log(Level.WARNING, "Could not set Icon for client " + info.getNickname());
			return;
		}
		api.addClientPermission(client.getDatabaseId(), "i_icon_id", (int) iconID, false);
	}

	public static void unsetIcon(String uid, TS3Api api) {
		DatabaseClientInfo info = api.getDatabaseClientByUId(uid);
		api.deleteClientPermission(info.getDatabaseId(), "i_icon_id");
	}

}
