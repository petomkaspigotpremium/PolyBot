package me.petomka.ts3bot.util;

import com.google.common.collect.Lists;
import com.google.gson.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import me.petomka.ts3bot.main.BukkitMain;
import me.petomka.ts3bot.main.MainManager;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

@Data
@ToString
@NoArgsConstructor
public class JSONComponent {

	private String text;
	private UtilChatColor color;
	private Boolean bold;
	private Boolean italic;
	private Boolean underlined;
	private Boolean strikethrough;
	private Boolean obfuscated;
	private ClickEvent clickEvent;
	private HoverEvent hoverEvent;
	private List<JSONComponent> extra = Lists.newArrayList();

	private JSONComponent(BaseComponent parent) {
		if (parent instanceof TextComponent) {
			this.text = ((TextComponent) parent).getText();
		}
		this.color = UtilChatColor.getByChar(parent.getColor().toString().charAt(1));
		this.bold = parent.isBoldRaw();
		this.italic = parent.isItalicRaw();
		this.underlined = parent.isUnderlinedRaw();
		this.strikethrough = parent.isStrikethroughRaw();
		this.obfuscated = parent.isObfuscatedRaw();
		this.clickEvent = parent.getClickEvent();
		this.hoverEvent = parent.getHoverEvent();
		if (parent.getExtra() != null) {
			parent.getExtra().stream().map(JSONComponent::new).forEach(extra::add);
		}
	}

	public static void sendMessage(CommandSender sender, BaseComponent[] components) {
		if (Bukkit.getConsoleSender().equals(sender)) {
			sender.sendMessage(TextComponent.toLegacyText(components));
			return;
		}

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(JSONComponent.class, new JSONComponent.Serializer());

		Gson gson = gsonBuilder.create();

		JSONComponent component = fromBaseComponent(components);
		String tellraw = gson.toJson(component);

		boolean sync = MainManager.main.getMainThread() == Thread.currentThread();

		Runnable runnable = () ->
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tellraw " + sender.getName() + " " + tellraw);

		if (sync) {
			runnable.run();
		} else {
			Bukkit.getScheduler().runTask((BukkitMain) MainManager.main, runnable);
		}
	}

	public static JSONComponent fromBaseComponent(BaseComponent[] components) {
		assert components.length > 0;

		JSONComponent first = new JSONComponent(components[0]);
		Arrays.stream(components).skip(1).forEach(baseComponent ->
				first.extra.add(new JSONComponent(baseComponent))
		);
		return first;
	}

	public static class Serializer implements JsonSerializer<JSONComponent> {
		@Override
		public JsonElement serialize(JSONComponent component, Type type, JsonSerializationContext context) {
			JsonObject json = new JsonObject();
			json.addProperty("text", component.text);
			if (component.color != null) {
				json.addProperty("color", component.color.getName());
			}
			if (component.bold != null) {
				json.addProperty("bold", component.bold);
			}
			if (component.italic != null) {
				json.addProperty("italic", component.italic);
			}
			if (component.underlined != null) {
				json.addProperty("underlined", component.underlined);
			}
			if (component.strikethrough != null) {
				json.addProperty("strikethrough", component.strikethrough);
			}
			if (component.obfuscated != null) {
				json.addProperty("obfuscated", component.obfuscated);
			}
			if (component.clickEvent != null) {
				JsonObject click = new JsonObject();
				click.addProperty("action", component.clickEvent.getAction().name().toLowerCase());
				click.addProperty("value", component.clickEvent.getValue());
				json.add("clickEvent", click);
			}
			if (component.hoverEvent != null) {
				JsonObject hover = new JsonObject();
				hover.addProperty("action", component.hoverEvent.getAction().name().toLowerCase());
				hover.add("value", serialize(fromBaseComponent(component.hoverEvent.getValue()), type, context));
				json.add("hoverEvent", hover);
			}
			if (!component.extra.isEmpty()) {
				JsonArray array = new JsonArray();
				component.extra.forEach(jc -> array.add(serialize(jc, type, context)));
				json.add("extra", array);
			}
			return json;
		}
	}
}
