package me.petomka.ts3bot.util;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public class ListFunctions {

	public static <A, B, C> Iterable<C> zipWith(Iterable<A> iterableA, Iterable<B> iterableB, BiFunction<A, B, C> fn) {
		return () -> new Iterator<C>() {
			private Iterator<A> itA = iterableA.iterator();
			private Iterator<B> itB = iterableB.iterator();

			public boolean hasNext() {
				return itA.hasNext() && itB.hasNext();
			}

			public C next() {
				return fn.apply(itA.next(), itB.next());
			}
		};
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> seen.add(keyExtractor.apply(t));
	}

}
