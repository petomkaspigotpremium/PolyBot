package me.petomka.ts3bot.util.clientverifier;

import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import lombok.Getter;
import lombok.Setter;
import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.player.UtilPlayer;
import me.petomka.ts3bot.util.playerverifier.PlayerVerifier;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Benedikt on 07.08.2017.
 */
public abstract class ClientVerifier {

	private static HashMap<UUID, ClientVerifier> verifierHashMap = new HashMap<>();
	protected final String code;

	@Getter
	@Setter
	private Runnable onFinished = null;

	private UtilPlayer player;

	public ClientVerifier(UtilPlayer player) {
		code = UUID.randomUUID().toString().substring(0, 5);
		this.player = player;
		setVerifying(player.getUUID(), this);
	}

	public static boolean isVerifying(UUID playerUUID) {
		return verifierHashMap.containsKey(playerUUID);
	}

	public static @Nullable String getCode(UUID playerUUID) {
		ClientVerifier verifier = verifierHashMap.get(playerUUID);
		if(verifier != null) {
			return verifier.code;
		}
		return null;
	}

	static void setVerifying(UUID playerUUID, ClientVerifier verifier) {
		PlayerVerifier.cancelVerification(playerUUID);
		verifierHashMap.put(playerUUID, verifier);
	}

	public static void finishedVerifying(UUID playerUUID) {
		if (verifierHashMap.get(playerUUID) == null) {
			return;
		}
		//verifierHashMap.get(player).finished();
		ClientVerifier verifier = verifierHashMap.remove(playerUUID);
		if (verifier != null && verifier.onFinished != null) {
			verifier.onFinished.run();
		}
	}

	Client checkCode(String code, String invokerNameExact) {
		if (!this.code.equals(code)) {
			return null;
		}
		Client client = MainManager.main.getTS3Api().getClientByNameExact(invokerNameExact, true);
		if (client == null) {
			throw new RuntimeException("Error: No client found to send text message");
		}
		return client;
	}

	public abstract void finished();

	public String getCode() {
		return code;
	}
}
