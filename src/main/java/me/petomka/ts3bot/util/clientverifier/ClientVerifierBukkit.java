package me.petomka.ts3bot.util.clientverifier;

import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.ts3listeners.TS3MessageReceivedEventBukkit;
import me.petomka.ts3bot.util.player.UtilPlayer;
import me.petomka.ts3bot.util.player.UtilPlayerBukkit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class ClientVerifierBukkit extends ClientVerifier implements Listener {

	private final Player player;

	public ClientVerifierBukkit(Player player) {
		super(new UtilPlayerBukkit(player));
		this.player = player;
		Bukkit.getPluginManager().registerEvents(this, (JavaPlugin) MainManager.main);
	}

	@EventHandler
	public void onTS3MessageReceived(TS3MessageReceivedEventBukkit event) {
		Client client = this.checkCode(event.getMessage(), event.getInvokerNameExact());
		if (client == null) {
			return;
		}
		UniMethods.linkPlayer(new UtilPlayerBukkit(player), client.getUniqueIdentifier(), null, null, null);
		finished();
	}

	public void finished() {
		HandlerList.unregisterAll(this);
		ClientVerifier.finishedVerifying(player.getUniqueId());
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		finished();
	}
}
