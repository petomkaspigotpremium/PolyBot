package me.petomka.ts3bot.util.clientverifier;

import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.ts3listeners.TS3MessageReceivedEventBungee;
import me.petomka.ts3bot.util.player.UtilPlayerBungee;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Benedikt on 07.08.2017.
 */
public class ClientVerifierBungee extends ClientVerifier implements Listener {

	private ProxiedPlayer player;

	public ClientVerifierBungee(ProxiedPlayer player) {
		super(new UtilPlayerBungee(player));
		this.player = player;
		ProxyServer.getInstance().getPluginManager().registerListener((Plugin) MainManager.main, this);
	}

	@EventHandler
	public void onTextMessageReceived(TS3MessageReceivedEventBungee event) {
		Client client = this.checkCode(event.getMessage(), event.getInvokerNameExact());
		if (client == null) {
			return;
		}
		UniMethods.linkPlayer(new UtilPlayerBungee(player), client.getUniqueIdentifier(), null, null, null);
		finished();
	}

	public void finished() {
		ProxyServer.getInstance().getPluginManager().unregisterListener(this);
		ClientVerifier.finishedVerifying(player.getUniqueId());
	}

	@EventHandler
	public void onQuit(PlayerDisconnectEvent event) {
		finished();
	}

}
