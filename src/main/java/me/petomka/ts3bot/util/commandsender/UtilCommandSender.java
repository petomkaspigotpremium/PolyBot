package me.petomka.ts3bot.util.commandsender;

import net.md_5.bungee.api.chat.BaseComponent;

/**
 * Created by Benedikt on 18.10.2017.
 */
public interface UtilCommandSender<T> {

	boolean hasPermission(String permission);

	void sendMessage(String message);

	void sendMessage(BaseComponent[] components);

	String getName();

	T unwrap();

}
