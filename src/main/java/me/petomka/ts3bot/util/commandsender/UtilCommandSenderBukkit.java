package me.petomka.ts3bot.util.commandsender;

import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.JSONComponent;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;

/**
 * Created by Benedikt on 18.10.2017.
 */
public class UtilCommandSenderBukkit implements UtilCommandSender<CommandSender> {

	private final CommandSender commandSender;

	public UtilCommandSenderBukkit(CommandSender commandSender) {
		this.commandSender = commandSender;
	}


	@Override
	public boolean hasPermission(String permission) {
		return commandSender.hasPermission(permission);
	}

	@Override
	public void sendMessage(String message) {
		sendMessage(TextComponent.fromLegacyText(message));
	}

	@Override
	public void sendMessage(BaseComponent[] components) {
		switch (MainManager.main.getCBVersion()) {
			case V1_10:
			case V1_11:
			case V1_12:
			case V1_13:
				commandSender.spigot().sendMessage(components);
				break;
			default:
				JSONComponent.sendMessage(commandSender, components);
//				commandSender.sendMessage(TextComponent.toLegacyText(components));
		}
	}

	@Override
	public String getName() {
		return commandSender.getName();
	}

	@Override
	public CommandSender unwrap() {
		return commandSender;
	}
}
