package me.petomka.ts3bot.util.commandsender;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * Created by Benedikt on 18.10.2017.
 */
public class UtilCommandSenderBungee implements UtilCommandSender<CommandSender> {

	private final CommandSender commandSender;

	public UtilCommandSenderBungee(CommandSender commandSender) {
		this.commandSender = commandSender;
	}

	@Override
	public boolean hasPermission(String permission) {
		return commandSender.hasPermission(permission);
	}

	@Override
	public void sendMessage(String message) {
		commandSender.sendMessage(TextComponent.fromLegacyText(message));
	}

	@Override
	public void sendMessage(BaseComponent[] components) {
		commandSender.sendMessage(components);
	}

	@Override
	public String getName() {
		return commandSender.getName();
	}

	@Override
	public CommandSender unwrap() {
		return commandSender;
	}
}
