package me.petomka.ts3bot.util.commandsender;

import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import me.petomka.ts3bot.config.LinkerHandler;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.UtilChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

public class UtilCommandSenderTSClient implements UtilCommandSender<ClientInfo> {

	private ClientInfo clientInfo;

	public UtilCommandSenderTSClient(ClientInfo clientInfo) {
		this.clientInfo = clientInfo;
	}

	@Override
	public boolean hasPermission(String permission) {
		try {
			return MainManager.main.getUtilPlayer(
					LinkerHandler.linker.getPlayerUUIDbyClientUID(clientInfo.getUniqueIdentifier()))
					.hasPermission(permission);
		} catch (Exception ignored) {
		}
		return false;
	}

	@Override
	public void sendMessage(String message) {
		message = UtilChatColor.stripColor(message);
		MainManager.main.getTS3Api().sendPrivateMessage(clientInfo.getId(), message);
	}

	@Override
	public void sendMessage(BaseComponent[] components) {
		this.sendMessage(TextComponent.toPlainText(components));
	}

	@Override
	public String getName() {
		return clientInfo.getNickname();
	}

	@Override
	public ClientInfo unwrap() {
		return clientInfo;
	}
}
