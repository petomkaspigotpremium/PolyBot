package me.petomka.ts3bot.util.player;

import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.commandsender.UtilCommandSender;

import java.net.InetAddress;
import java.util.UUID;

public interface UtilPlayer<T> extends UtilCommandSender<T> {

	static UtilPlayer of(Object player) {
		return MainManager.main.getUtilPlayer(player);
	}

	UUID getUUID();

	InetAddress getIPAddress();

}
