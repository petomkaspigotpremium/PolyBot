package me.petomka.ts3bot.util.player;

import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.net.InetAddress;
import java.util.UUID;

public class UtilPlayerBukkit extends UtilCommandSenderBukkit implements UtilPlayer<CommandSender> {

	private final Player player;

	public UtilPlayerBukkit(Player player) {
		super(player);
		this.player = player;
	}

	public static UtilPlayer of(Player player) {
		return new UtilPlayerBukkit(player);
	}

	@Override
	public InetAddress getIPAddress() {
		return player.getAddress().getAddress();
	}

	@Override
	public UUID getUUID() {
		return player.getUniqueId();
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof UtilPlayer) {
			return ((UtilPlayer) other).getUUID().equals(getUUID());
		}
		return false;
	}
}
