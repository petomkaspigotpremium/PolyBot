package me.petomka.ts3bot.util.player;

import me.petomka.ts3bot.util.commandsender.UtilCommandSenderBungee;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.net.InetAddress;
import java.util.UUID;

public class UtilPlayerBungee extends UtilCommandSenderBungee implements UtilPlayer<CommandSender> {

	private ProxiedPlayer player;

	public UtilPlayerBungee(ProxiedPlayer player) {
		super(player);
		this.player = player;
	}

	public static UtilPlayer of(ProxiedPlayer player) {
		return new UtilPlayerBungee(player);
	}

	@Override
	public UUID getUUID() {
		return player.getUniqueId();
	}

	@Override
	public InetAddress getIPAddress() {
		return player.getPendingConnection().getAddress().getAddress();
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof UtilPlayer) {
			return ((UtilPlayer) other).getUUID().equals(getUUID());
		}
		return false;
	}

}
