package me.petomka.ts3bot.util.playerverifier;

import lombok.Getter;
import lombok.Setter;
import me.petomka.ts3bot.commands.UniMethods;
import me.petomka.ts3bot.config.ConfigManager;
import me.petomka.ts3bot.main.MainManager;
import me.petomka.ts3bot.util.clientverifier.ClientVerifier;
import me.petomka.ts3bot.util.player.UtilPlayer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.UUID;

public class PlayerVerifier {

	private static HashMap<UUID, PlayerVerifier> verifierHashMap = new HashMap<>();
	private final String code;

	@Getter
	@Setter
	private Runnable onFinished = null;

	@Getter
	private final String clientUID;

	@Getter
	private final UtilPlayer player;

	public PlayerVerifier(UtilPlayer player, String clientUID) {
		code = UUID.randomUUID().toString().substring(0, 5);
		this.clientUID = clientUID;
		this.player = player;
		int clid = MainManager.main.getTS3Api().getClientByUId(clientUID).getId();
		if (!setVerifying(this.player.getUUID(), this)) {
			MainManager.main.getTS3Api().sendPrivateMessage(clid,
					ConfigManager.getString("tscommands.link.already-linking"));
			return;
		}
		String clientName = MainManager.main.getTS3Api().getClientByUId(clientUID).getNickname();
		BaseComponent[] components = new ComponentBuilder("")
				.append(TextComponent.fromLegacyText(ConfigManager.getString("message.linking-requested")
						.replace("<tsname>", clientName))).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,
						"/link " + code))
				.create();
		player.sendMessage(components);
		MainManager.main.getTS3Api().sendPrivateMessage(clid,
				ConfigManager.getRawString("tscommands.link.requested")
						.replace("<code>", code));
	}

	public static boolean isVerifying(UUID playerUUID) {
		return verifierHashMap.containsKey(playerUUID);
	}

	public static void cancelVerification(UUID playerUUID) {
		verifierHashMap.remove(playerUUID);
	}

	public static PlayerVerifier getVerifier(UUID playerUUID) {
		return verifierHashMap.get(playerUUID);
	}

	public static @Nullable
	String getCode(UUID playerUUID) {
		PlayerVerifier verifier = verifierHashMap.get(playerUUID);
		if (verifier != null) {
			return verifier.code;
		}
		return null;
	}

	public static @Nullable
	String getClientUniqueIdentifier(UUID playerUUID) {
		PlayerVerifier verifier = verifierHashMap.get(playerUUID);
		if (verifier != null) {
			return verifier.clientUID;
		}
		return null;
	}

	private static boolean setVerifying(UUID player, PlayerVerifier verifier) {
		if (ClientVerifier.isVerifying(player)) {
			return false;
		}
		verifierHashMap.put(player, verifier);
		return true;
	}

	private static void finishedVerifying(UUID playerUUID) {
		if (verifierHashMap.get(playerUUID) == null) {
			return;
		}
		//verifierHashMap.get(clientUID).finished();
		PlayerVerifier verifier = verifierHashMap.remove(playerUUID);
		if (verifier != null && verifier.onFinished != null) {
			verifier.onFinished.run();
		}
	}

	public void finished() {
		UniMethods.linkPlayer(player, clientUID, null, null, null);
		finishedVerifying(this.player.getUUID());
	}

	public String getCode() {
		return code;
	}

}
